package model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LevelEvaluationsTest {

	private LevelEvaluations levelEval;

	@Before
	public void setUp() {
		levelEval = new LevelEvaluations();
	}

	@After
	public void tearDown() {
		levelEval = null;
	}

	@Test
	public void testEvaluateLevelOneWithZero() {
		levelEval.evaluateLevelOne();
		Assert.assertThat(levelEval.getLevelOneEvaluation(), is(1));
	}

	@Test
	public void testEvaluateLevelOneWithTwo() {
		levelEval.setLevelOneEvaluation(2);
		levelEval.evaluateLevelOne();
		Assert.assertThat(levelEval.getLevelOneEvaluation(), is(2));
	}

	@Test
	public void testEvaluateLevelOneWithOne() {
		levelEval.setLevelOneEvaluation(1);
		levelEval.evaluateLevelOne();
		assertThat(levelEval.getStartNewLevel(), is(true));
		assertThat(levelEval.getLevel(), is(2));
	}

	@Test
	public void testEvaluateLevelTwoWithZero() {
		levelEval.setLevelTwoEvaluation(0);
		levelEval.evaluateLevelTwo();
		assertThat(levelEval.getLevelTwoEvaluation(), is(1));
	}

	@Test
	public void testEvaluateLevelTwoWithOne() {
		levelEval.setLevelTwoEvaluation(1);
		levelEval.evaluateLevelTwo();
		assertThat(levelEval.getLevelTwoEvaluation(), is(2));
	}

	@Test
	public void testEvaluateLevelTwoWithTwo() {
		levelEval.setLevelTwoEvaluation(2);
		levelEval.evaluateLevelTwo();
		assertThat(levelEval.getLevelTwoEvaluation(), is(3));
		assertThat(levelEval.getLevelTwoInRow(), is(1));
	}

	@Test
	public void testEvaluateLevelTwoWithThree() {
		levelEval.setLevelTwoEvaluation(3);
		levelEval.evaluateLevelTwo();
		assertThat(levelEval.getLevelTwoEvaluation(), is(4));
	}

	@Test
	public void testEvaluateLevelTwoWithFour() {
		levelEval.setLevelTwoEvaluation(4);
		levelEval.evaluateLevelTwo();
		assertThat(levelEval.getLevelTwoEvaluation(), is(5));
	}

	@Test
	public void testEvaluateLevelTwoWithFive() {
		levelEval.setLevelTwoEvaluation(5);
		levelEval.evaluateLevelTwo();
		assertThat(levelEval.getLevelTwoEvaluation(), is(5));
	}

	@Test
	public void testEvaluateLevelTwoWithOneInRow() {
		levelEval.setLevelTwoEvaluation(2);
		levelEval.setLevelTwoInRow(1);
		levelEval.evaluateLevelTwo();
		assertThat(levelEval.getLevelTwoInRow(), is(2));
	}

	@Test
	public void testEvaluateLevelTwoWithTwoInRow() {
		levelEval.setLevelTwoEvaluation(2);
		levelEval.setLevelTwoInRow(2);
		levelEval.evaluateLevelTwo();
		assertThat(levelEval.getLevelTwoInRow(), is(3));
	}

	@Test
	public void testEvaluateLevelTwoWithThreeInRow() {
		levelEval.setLevelTwoEvaluation(2);
		levelEval.setLevelTwoInRow(3);
		levelEval.evaluateLevelTwo();
		assertThat(levelEval.getLevelTwoInRow(), is(3));
	}
	
	@Test
	public void testEvaluateLevelTwoComplete() {
		levelEval.setLevelTwoEvaluation(5);
		levelEval.setLevelTwoInRow(3);
		levelEval.evaluateLevelTwo();
		assertThat(levelEval.getStartNewLevel(), is(true));
		assertThat(levelEval.getLevel(), is(3));
	}

	@Test
	public void testEvaluateLevelThreeWithZero() {
		levelEval.evaluateLevelThree();
		assertThat(levelEval.getLevelThreeInRow(), is(1));
	}

	@Test
	public void testEvaluateLevelThreeWithOne() {
		levelEval.setLevelThreeInRow(1);
		levelEval.evaluateLevelThree();
		assertThat(levelEval.getLevelThreeInRow(), is(2));
	}

	@Test
	public void testEvaluateLevelThreeWithTwo() {
		levelEval.setLevelThreeInRow(2);
		levelEval.evaluateLevelThree();
		assertThat(levelEval.getStartNewLevel(), is(true));
		assertThat(levelEval.getLevel(), is(4));
	}

	@Test
	public void testEvaluateLevelThreeWithThree() {
		levelEval.setLevelThreeInRow(3);
		levelEval.evaluateLevelThree();
		assertThat(levelEval.getLevelThreeInRow(), is(3));
	}

	@Test
	public void testEvaluateLevelFourWithZero() {
		levelEval.evaluateLevelFour();
		assertThat(levelEval.getLevelFourInRow(), is(1));
	}

	@Test
	public void testEvaluateLevelFourWithOne() {
		levelEval.setLevelFourInRow(1);
		levelEval.evaluateLevelFour();
		assertThat(levelEval.getLevelFourInRow(), is(2));
	}

	@Test
	public void testEvaluateLevelFourWithTwo() {
		levelEval.setLevelFourInRow(2);
		levelEval.evaluateLevelFour();
		assertThat(levelEval.getStartNewLevel(), is(true));
		assertThat(levelEval.getLevel(), is(5));
	}

	@Test
	public void testEvaluateLevelFourWithFour() {
		levelEval.setLevelFourInRow(3);
		levelEval.evaluateLevelFour();
		assertThat(levelEval.getLevelFourInRow(), is(3));
	}

	@Test
	public void testEvaluateLevelFiveWithZero() {
		levelEval.evaluateLevelFive();
		Assert.assertThat(levelEval.getLevelFiveEvaluation(), is(1));
	}

	@Test
	public void testEvaluateLevelFiveWithFive() {
		levelEval.setLevelFiveEvaluation(1);
		levelEval.evaluateLevelFive();
		Assert.assertThat(levelEval.getLevelFiveEvaluation(), is(2));
	}

	@Test
	public void testEvaluateLevelFiveWithTwo() {
		levelEval.setLevelFiveEvaluation(2);
		levelEval.evaluateLevelFive();
		assertThat(levelEval.getStartNewLevel(), is(true));
		assertThat(levelEval.getLevel(), is(6));
	}

	@Test
	public void testEvaluateLevelFiveWithThree() {
		levelEval.setLevelFiveEvaluation(3);
		levelEval.evaluateLevelFive();
		Assert.assertThat(levelEval.getLevelFiveEvaluation(), is(3));
	}

	@Test
	public void testZuruecksetzen() {
		levelEval.setLevel(5);
		levelEval.setLevelOneEvaluation(3);
		levelEval.setLevelTwoEvaluation(3);
		levelEval.setLevelTwoInRow(3);
		levelEval.setLevelThreeInRow(3);
		levelEval.setLevelFourInRow(3);
		levelEval.setLevelFiveEvaluation(2);
		levelEval.zuruecksetzen();
		assertThat(levelEval.getLevel(), is(1));
		assertThat(levelEval.getLevelOneEvaluation(), is(0));
		assertThat(levelEval.getLevelTwoEvaluation(), is(0));
		assertThat(levelEval.getLevelTwoInRow(), is(0));
		assertThat(levelEval.getLevelThreeInRow(), is(0));
		assertThat(levelEval.getLevelFourInRow(), is(0));
		assertThat(levelEval.getLevelFiveEvaluation(), is(0));
	}

}
