package view;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
//TODO Verena: Umstellung auf CellEditor
/**
 * Klasse fuer die Tabelle fuer Level 5
 * 
 * @author Verena Stech
 *
 */
public class TableLevelFive extends JTable {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * Konstruktor.
	 */
	public TableLevelFive(int rows, int column) {
		super(rows, column);
		((DefaultTableCellRenderer) this.getDefaultRenderer(Object.class)).setHorizontalAlignment(SwingConstants.CENTER);

		DefaultCellEditor defaultCellEditor = new DefaultCellEditor(new JTextField());
		this.getColumnModel().getColumn(0).setCellEditor(defaultCellEditor);
	}

	/**
	 * Tabelle kann erst ab der dritten Zeile beschriftet werden
	 */
	@Override
	public boolean isCellEditable(int row, int column) {
		if (row == 0 || row == 1) {
			return false;
		}
		return super.isCellEditable(row, column);
	}

}
