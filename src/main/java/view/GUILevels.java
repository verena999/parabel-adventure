package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Oberflaeche fuer die verschiedenen Level
 * 
 * @author Verena Stech, Henning Bergmann, Marcel Lievre,
 *
 */
public class GUILevels extends JPanel {
	private static final long serialVersionUID = 1L;
	private int level;
	private int quantity;
	private JPanel southBottomPanel = new JPanel();
	private JPanel southPanel = new JPanel();
	private JPanel southTopLeftPanel = new JPanel();
	private JPanel southTopPanel = new JPanel();
	private JPanel southTopRightPanel = new JPanel();
	private JPanel westPanel = new JPanel();
	private JPanel eastPanel = new JPanel();
	private JPanel northLeftPanel = new JPanel();
	private JPanel northPanel = new JPanel();
	private JPanel northRightPanel = new JPanel();
	private JLabel scoreLabel = new JLabel("Punkte:");
	private JLabel scoreOutputLabel = new JLabel("50");
	private JLabel turnLabel = new JLabel("Spielz\u00fcge:");
	private JLabel turnOutputLabel = new JLabel("10");
	private JButton checkButton = new JButton("Pr\u00fcfen");
	private JButton mainMenuButton = new JButton("Zur\u00fcck zum Hauptmen\u00fc");
	private JButton helpButton = new JButton("?");
	private JButton statisticsButton = new JButton("Statistik");
	private JLabel equationLabel = new JLabel("Gleichung: ");
	private JLabel equation = new JLabel();
	private JLabel valuetableLabel = new JLabel("Wertetabelle: ");
	private TablesLevelOne tableLevelOne = new TablesLevelOne(2, 5, this);
	private TableLevelTwo tableLevelTwo = new TableLevelTwo(3, 4, this);
	private JLabel quantityXValueLabel = new JLabel("Anzahl X-Werte: ");
	private JTextField quantityXValueTextField = new JTextField(2);
	private JButton readyButton = new JButton("Los");
	private JButton changeQuantityButton = new JButton("Anzahl \u00E4ndern");
	private TableLevelThree tableLevelThree;
	private JLabel equationsLabel = new JLabel("Gleichungen: ");
	private TableLevelFour tableLevelFour;
	private JLabel secondTermLabel = new JLabel("Zweite Gleichung");
	private TableLevelFive tableLevelFive = new TableLevelFive(2, 5);
	private JTextField koefficientOne = new JTextField(1);
	private JTextField koefficientTwo = new JTextField(1);
	private JTextField koefficientThree = new JTextField(1);
	private JButton goOnButton = new JButton("Weiter zum n\u00E4chsten Level");
	private JButton goAheadButton = new JButton("Weiter");

	private JTextField koefficientFour = new JTextField(1);
	private JLabel exponentOne = new JLabel("x\u00B3 + ");
	private JLabel exponentTwo = new JLabel("x\u00B2 + ");
	private JLabel exponentThree = new JLabel("x + ");
	double[] mistake = new double[200];

	/**
	 * 
	 * Konstruktor.
	 */
	public GUILevels() {
		initialize();
	}

	/**
	 * setzt das Layout und ruft alle Methoden zum Stylen der Komponenten auf
	 * 
	 * fuegt alle notwendigen Komponenten zur Oberflaeche hinzu, die nicht
	 * Levelspezifisch sind
	 * 
	 * setzt die Groesse der einzelnen Panels
	 * 
	 * setzt die Layouts der Komponenten
	 */
	public void initialize() {
		setFontOfAllComponents();
		setAllComponentsOpaque();
		setForeAndBackgroundOfAllComponents();
		setLayout(new BorderLayout());
		northPanel.setLayout(new GridLayout(1, 2));
		northPanel.setPreferredSize(new Dimension(1500, 100));
		createNorthPanel();
		scoreLabel.setAlignmentX(LEFT_ALIGNMENT);
		scoreOutputLabel.setAlignmentX(LEFT_ALIGNMENT);
		turnOutputLabel.setAlignmentX(RIGHT_ALIGNMENT);
		turnLabel.setAlignmentX(RIGHT_ALIGNMENT);
		northLeftPanel.add(scoreLabel);
		northLeftPanel.add(scoreOutputLabel);
		northRightPanel.add(turnOutputLabel);
		northRightPanel.add(turnLabel);
		add(northPanel, BorderLayout.NORTH);
		northPanel.add(northLeftPanel);
		northPanel.add(northRightPanel);
		add(eastPanel, BorderLayout.EAST);
		add(westPanel, BorderLayout.WEST);
		add(southPanel, BorderLayout.SOUTH);
		westPanel.setPreferredSize(new Dimension(100, 1000));
		eastPanel.setPreferredSize(new Dimension(100, 1000));
		southPanel.add(southTopPanel);
		southPanel.add(southBottomPanel);
		southTopPanel.add(southTopLeftPanel);
		southTopPanel.add(southTopRightPanel);
		southPanel.setPreferredSize(new Dimension(1500, 200));
		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.Y_AXIS));
		southPanel.add(Box.createVerticalStrut(4));
		southTopPanel.setLayout(new GridLayout(1, 2, 20, 20));
		southTopLeftPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		southTopRightPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
	}

	/**
	 * je nach Level wird die richtige Methode ausgewaehlt
	 * 
	 * @param level das aktuelle Level
	 */
	public void initLevel(@SuppressWarnings("hiding") int level) {
		if (this.level != level) {
			this.setLevel(level);
			removeContent();
			createNorthPanel();
			initialize();
			if (level == 1) {
				levelOne();
			} else if (level == 2) {
				levelTwo();
			} else if (level == 3) {
				levelThree();
			} else if (level == 4) {
				levelFour();
			} else if (level == 5) {
				levelFive();
			}
		} else {
			return;
		}
	}

	/**
	 * erstellt die Oberflaeche fuer Level 1
	 * 
	 * fuegt alle Komponenten hinzu
	 * 
	 * erstellt die Tabelle fuer das erste Level
	 * 
	 */
	public void levelOne() {
		southTopRightPanel.add(valuetableLabel);
		southTopRightPanel.add(tableLevelOne);
		southTopRightPanel.add(Box.createHorizontalStrut(20));
		southTopLeftPanel.add(Box.createHorizontalStrut(20));
		southTopLeftPanel.add(equationLabel);
		southTopLeftPanel.add(equation);
		tableLevelOne.getColumnModel().getColumn(0).setPreferredWidth(90);
		tableLevelOne.getColumnModel().getColumn(1).setPreferredWidth(90);
		tableLevelOne.getColumnModel().getColumn(2).setPreferredWidth(90);
		tableLevelOne.getColumnModel().getColumn(3).setPreferredWidth(90);
		tableLevelOne.getColumnModel().getColumn(4).setPreferredWidth(90);
		tableLevelOne.setRowHeight(40);
		tableLevelOne.setValueAt("x", 0, 0);
		tableLevelOne.setValueAt("y", 1, 0);
		tableLevelOne.setValueAt(1, 0, 1);
		tableLevelOne.setValueAt(2, 0, 2);
		tableLevelOne.setValueAt(3, 0, 3);
		tableLevelOne.setValueAt(4, 0, 4);
		tableLevelOne.setRowSelectionAllowed(false);
		tableLevelOne.setRowSelectionAllowed(false);
		tableLevelOne.setBorder(BorderFactory.createLineBorder(Color.white, 2));
		// TODO Verena: Selektiertes Feld darf nicht mehr schwarz werden beim anklicken
		((DefaultTableCellRenderer) tableLevelOne.getDefaultRenderer(Object.class)).setOpaque(false);
		((DefaultTableCellRenderer) tableLevelOne.getDefaultRenderer(Object.class)).setForeground(Color.WHITE);
		southBottomPanel.setLayout(new BoxLayout(southBottomPanel, BoxLayout.LINE_AXIS));
		southBottomPanel.setPreferredSize(new Dimension(0, 60));
		southBottomPanel.add(checkButton);
		southBottomPanel.add(mainMenuButton);
		southBottomPanel.add(statisticsButton);
		southBottomPanel.add(helpButton);
	}

	/**
	 * erstellt die Oberflaeche fuer Level 2
	 * 
	 * fuegt alle Komponenten hinzu
	 * 
	 * erstellt die Tabelle fuer das zweite Level
	 */
	public void levelTwo() {
		quantity = 1;
		southTopRightPanel.add(valuetableLabel);
		southTopRightPanel.add(tableLevelTwo);
		southTopRightPanel.add(Box.createHorizontalStrut(20));
		southTopLeftPanel.add(Box.createHorizontalStrut(20));
		southTopLeftPanel.add(equationLabel);
		southTopLeftPanel.add(equation);
		southBottomPanel.setLayout(new BoxLayout(southBottomPanel, BoxLayout.LINE_AXIS));
		southBottomPanel.setPreferredSize(new Dimension(0, 60));
		southBottomPanel.add(checkButton);
		southBottomPanel.add(mainMenuButton);
		southBottomPanel.add(statisticsButton);
		southBottomPanel.add(helpButton);
		if (quantity == 2) {
			tableLevelTwo.removeColumn(tableLevelTwo.getColumnModel().getColumn(1));
			tableLevelTwo.getColumnModel().getColumn(1).setPreferredWidth(90);
		} else if (quantity == 1) {
			tableLevelTwo.removeColumn(tableLevelTwo.getColumnModel().getColumn(1));
			tableLevelTwo.removeColumn(tableLevelTwo.getColumnModel().getColumn(0));
			tableLevelTwo.getColumnModel().getColumn(0).setPreferredWidth(90);
			tableLevelTwo.getColumnModel().getColumn(1).setPreferredWidth(90);
		}
		tableLevelTwo.setRowHeight(40);
		tableLevelTwo.setValueAt("x", 0, 0);
		tableLevelTwo.setValueAt("y", 1, 0);
		tableLevelTwo.setValueAt("H/T", 2, 0);
		tableLevelTwo.setRowSelectionAllowed(false);
		tableLevelTwo.setRowSelectionAllowed(false);
		tableLevelTwo.setBorder(BorderFactory.createLineBorder(Color.white, 2));
		((DefaultTableCellRenderer) tableLevelTwo.getDefaultRenderer(Object.class)).setOpaque(false);
		((DefaultTableCellRenderer) tableLevelTwo.getDefaultRenderer(Object.class)).setForeground(Color.WHITE);
		};
	

	/**
	 * erstellt die Oberflaeche fuer Level 3
	 * 
	 * fuegt alle Komponenten hinzu
	 * 
	 */
	public void levelThree() {
		southTopLeftPanel.add(Box.createHorizontalStrut(20));
		southTopLeftPanel.add(equationLabel);
		southTopLeftPanel.add(equation);
		quantityXValueTextField.setText("");
		southTopRightPanel.add(quantityXValueLabel);
		southTopRightPanel.add(quantityXValueTextField);
		southTopRightPanel.add(readyButton);
		southTopRightPanel.add(Box.createHorizontalStrut(20));
		
		quantityXValueTextField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
//					columnEditors.forEach(editor -> editor.stopCellEditing());
					readyButton.doClick();
				}
			}
		});

		southBottomPanel.setLayout(new BoxLayout(southBottomPanel, BoxLayout.LINE_AXIS));
		southBottomPanel.setPreferredSize(new Dimension(0, 60));
		southBottomPanel.add(checkButton);
		southBottomPanel.add(mainMenuButton);
		southBottomPanel.add(statisticsButton);
		southBottomPanel.add(helpButton);
		southTopRightPanel.add(Box.createHorizontalStrut(20));
	}

	/**
	 * erstellt die Oberflaeche fuer Level 4
	 * 
	 * fuegt alle Komponenten hinzu
	 */
	public void levelFour() {
		southTopLeftPanel.setLayout(new GridLayout(2, 2));
		JPanel panel = new JPanel(new GridLayout(1, 1));
		panel.setOpaque(false);
		southTopLeftPanel.add(equationsLabel);
		southTopLeftPanel.add(equation);
		southTopLeftPanel.add(panel);
		southTopLeftPanel.add(secondTermLabel);
		changeQuantityLevelThreeFour();

		southBottomPanel.setLayout(new BoxLayout(southBottomPanel, BoxLayout.LINE_AXIS));
		southBottomPanel.setPreferredSize(new Dimension(0, 60));
		southBottomPanel.add(changeQuantityButton);
		southBottomPanel.add(checkButton);
		southBottomPanel.add(mainMenuButton);
		southBottomPanel.add(statisticsButton);
		southBottomPanel.add(helpButton);

	}

	/**
	 * erstellt die Oberflaeche fuer Level 5
	 * 
	 * fuegt alle Komponenten hinzu
	 */
	public void levelFive() {
		quantity = 3;
		southTopLeftPanel.add(Box.createHorizontalStrut(20));
		southTopLeftPanel.add(equationLabel);
		southTopRightPanel.add(valuetableLabel);
		tableLevelFive.setRowHeight(40);
		tableLevelFive.getColumnModel().getColumn(0).setPreferredWidth(90);
		tableLevelFive.getColumnModel().getColumn(1).setPreferredWidth(90);
		tableLevelFive.getColumnModel().getColumn(2).setPreferredWidth(90);
		tableLevelFive.getColumnModel().getColumn(3).setPreferredWidth(90);
		tableLevelFive.getColumnModel().getColumn(4).setPreferredWidth(90);
		tableLevelFive.setValueAt("x", 0, 0);
		tableLevelFive.setValueAt("y", 1, 0);
		tableLevelFive.setRowSelectionAllowed(false);
		tableLevelFive.setBorder(BorderFactory.createLineBorder(Color.white, 2));
		((DefaultTableCellRenderer) tableLevelFive.getDefaultRenderer(Object.class)).setOpaque(false);
		((DefaultTableCellRenderer) tableLevelFive.getDefaultRenderer(Object.class)).setForeground(Color.WHITE);
		southTopRightPanel.add(tableLevelFive);
		koefficientOne.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					
					getCheckButton().doClick();
				}
			}
		});
		koefficientTwo.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					
					getCheckButton().doClick();
				}
			}
		});
		koefficientThree.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					
					getCheckButton().doClick();
				}
			}
		});
		koefficientFour.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					
					getCheckButton().doClick();
				}
			}
		});
		if (quantity == 4) {
			southTopLeftPanel.add(koefficientOne);
			southTopLeftPanel.add(exponentOne);
			southTopLeftPanel.add(koefficientTwo);
			southTopLeftPanel.add(exponentTwo);
			southTopLeftPanel.add(koefficientThree);
			southTopLeftPanel.add(exponentThree);
			southTopLeftPanel.add(koefficientFour);
		} else if (quantity == 3) {
			southTopLeftPanel.add(koefficientTwo);
			southTopLeftPanel.add(exponentTwo);
			southTopLeftPanel.add(koefficientThree);
			southTopLeftPanel.add(exponentThree);
			southTopLeftPanel.add(koefficientFour);
			tableLevelFive.removeColumn(tableLevelFive.getColumnModel().getColumn(4));
		}
		southBottomPanel.setLayout(new BoxLayout(southBottomPanel, BoxLayout.LINE_AXIS));
		southBottomPanel.setPreferredSize(new Dimension(0, 60));
		southBottomPanel.add(checkButton);
		southBottomPanel.add(mainMenuButton);
		southBottomPanel.add(statisticsButton);
		southBottomPanel.add(helpButton);
		southTopRightPanel.add(Box.createHorizontalStrut(20));
	}

	/**
	 * loescht den Inhalt der Tabelle von Level 1
	 */
	public void deleteLevelOneTableContent() {
		tableLevelOne.setValueAt("", 1, 1);
		tableLevelOne.setValueAt("", 1, 2);
		tableLevelOne.setValueAt("", 1, 3);
		tableLevelOne.setValueAt("", 1, 4);
	}

	/**
	 * loescht den Inhalt der Tabelle von Level 2
	 */
	public void deleteLevelTwoTableContent() {
		tableLevelTwo.setValueAt("", 0, 1);
		tableLevelTwo.setValueAt("", 1, 1);
		tableLevelTwo.setValueAt("", 2, 1);
	}

	/**
	 * loescht den Inhalt der Tabelle aus Level 3
	 */
	public void deleteLevelThreeTableContent() {
		if (quantity == 1) {
			tableLevelThree.setValueAt("", 0, 1);
		} else if (quantity == 2) {
			tableLevelThree.setValueAt("", 0, 1);
			tableLevelThree.setValueAt("", 0, 2);
		} else if (quantity == 3) {
			tableLevelThree.setValueAt("", 0, 1);
			tableLevelThree.setValueAt("", 0, 2);
			tableLevelThree.setValueAt("", 0, 3);
		} else if (quantity == 4) {
			tableLevelThree.setValueAt("", 0, 1);
			tableLevelThree.setValueAt("", 0, 2);
			tableLevelThree.setValueAt("", 0, 3);
			tableLevelThree.setValueAt("", 0, 4);
		} else {
			tableLevelThree.setValueAt("", 0, 1);
			tableLevelThree.setValueAt("", 0, 2);
			tableLevelThree.setValueAt("", 0, 3);
			tableLevelThree.setValueAt("", 0, 4);
			tableLevelThree.setValueAt("", 0, 5);
		}
	}

	/**
	 * loescht den Inhalt aus der Tabelle von Level 4
	 */
	public void deleteLevelFourTableContent() {
		if (quantity == 1) {
			tableLevelFour.setValueAt("", 0, 1);
			tableLevelFour.setValueAt("", 1, 1);
		} else if (quantity == 2) {
			tableLevelFour.setValueAt("", 0, 1);
			tableLevelFour.setValueAt("", 0, 2);
			tableLevelFour.setValueAt("", 1, 1);
			tableLevelFour.setValueAt("", 1, 2);
		} else if (quantity == 3) {
			tableLevelFour.setValueAt("", 0, 1);
			tableLevelFour.setValueAt("", 0, 2);
			tableLevelFour.setValueAt("", 0, 3);
			tableLevelFour.setValueAt("", 1, 1);
			tableLevelFour.setValueAt("", 1, 2);
			tableLevelFour.setValueAt("", 1, 3);
		} else if (quantity == 4) {
			tableLevelFour.setValueAt("", 0, 1);
			tableLevelFour.setValueAt("", 0, 2);
			tableLevelFour.setValueAt("", 0, 3);
			tableLevelFour.setValueAt("", 0, 4);
			tableLevelFour.setValueAt("", 1, 1);
			tableLevelFour.setValueAt("", 1, 2);
			tableLevelFour.setValueAt("", 1, 3);
			tableLevelFour.setValueAt("", 1, 4);
		} else {
			tableLevelFour.setValueAt("", 0, 1);
			tableLevelFour.setValueAt("", 0, 2);
			tableLevelFour.setValueAt("", 0, 3);
			tableLevelFour.setValueAt("", 0, 4);
			tableLevelFour.setValueAt("", 0, 5);
			tableLevelFour.setValueAt("", 1, 1);
			tableLevelFour.setValueAt("", 1, 2);
			tableLevelFour.setValueAt("", 1, 3);
			tableLevelFour.setValueAt("", 1, 4);
			tableLevelFour.setValueAt("", 1, 5);
		}
	}

	/**
	 * loscht den Inhalt der Textfelder fuer die Koeffizienten
	 */
	public void deleteLevelFiveKoefficientContent() {
		koefficientOne.setText("");
		koefficientTwo.setText("");
		koefficientThree.setText("");
		koefficientFour.setText("");
	}

	/**
	 * 
	 * /** setzt nach dem Aendern der Anzahl die Tabelle neu in die Oberflaeche
	 */
	public void showTableLevelThree() {
		southTopRightPanel.removeAll();
		tableLevelThree = new TableLevelThree(1, 6, this);
		tableLevelThree.setFont(new Font("Colonna MT", Font.BOLD, 25));
		tableLevelThree.setSelectionForeground(Color.WHITE);
		tableLevelThree.setOpaque(false);
		((DefaultTableCellRenderer) tableLevelThree.getDefaultRenderer(Object.class))
				.setHorizontalAlignment(SwingConstants.CENTER);
		southTopRightPanel.add(valuetableLabel);
		southBottomPanel.setLayout(new BoxLayout(southBottomPanel, BoxLayout.LINE_AXIS));
		southBottomPanel.setPreferredSize(new Dimension(0, 60));
		southBottomPanel.add(changeQuantityButton);
		southBottomPanel.add(checkButton);
		southBottomPanel.add(mainMenuButton);
		southBottomPanel.add(statisticsButton);
		southBottomPanel.add(helpButton);
		tableLevelThree.setRowHeight(40);
		tableLevelThree.getColumnModel().getColumn(0).setPreferredWidth(90);
		tableLevelThree.getColumnModel().getColumn(1).setPreferredWidth(90);
		tableLevelThree.getColumnModel().getColumn(2).setPreferredWidth(90);
		tableLevelThree.getColumnModel().getColumn(3).setPreferredWidth(90);
		tableLevelThree.getColumnModel().getColumn(4).setPreferredWidth(90);
		tableLevelThree.getColumnModel().getColumn(5).setPreferredWidth(90);
		tableLevelThree.setRowSelectionAllowed(false);
		tableLevelThree.setBorder(BorderFactory.createLineBorder(Color.white, 2));
		((DefaultTableCellRenderer) tableLevelThree.getDefaultRenderer(Object.class)).setOpaque(false);
		((DefaultTableCellRenderer) tableLevelThree.getDefaultRenderer(Object.class)).setForeground(Color.WHITE);
		southTopRightPanel.add(tableLevelThree);
		tableLevelThree.setValueAt("X", 0, 0);
		if (quantity == 1) {
			tableLevelThree.removeColumn(tableLevelThree.getColumnModel().getColumn(4));
			tableLevelThree.removeColumn(tableLevelThree.getColumnModel().getColumn(3));
			tableLevelThree.removeColumn(tableLevelThree.getColumnModel().getColumn(2));
			tableLevelThree.removeColumn(tableLevelThree.getColumnModel().getColumn(1));
		} else if (quantity == 2) {
			tableLevelThree.removeColumn(tableLevelThree.getColumnModel().getColumn(3));
			tableLevelThree.removeColumn(tableLevelThree.getColumnModel().getColumn(2));
			tableLevelThree.removeColumn(tableLevelThree.getColumnModel().getColumn(1));
		} else if (quantity == 3) {
			tableLevelThree.removeColumn(tableLevelThree.getColumnModel().getColumn(2));
			tableLevelThree.removeColumn(tableLevelThree.getColumnModel().getColumn(1));
		} else if (quantity == 4) {
			tableLevelThree.removeColumn(tableLevelThree.getColumnModel().getColumn(1));
		}
		southTopRightPanel.add(Box.createHorizontalStrut(20));
		tableLevelThree.validate();
		southTopRightPanel.repaint();
	}

	/**
	 * setzt nach dem Aendern der Anzahl die Tabelle neu in die Oberflaeche
	 */
	public void showTableLevelFour() {
		tableLevelFour = new TableLevelFour(2, 6, this);
		tableLevelFour.setFont(new Font("Colonna MT", Font.BOLD, 25));
		tableLevelFour.setSelectionForeground(Color.WHITE);
		tableLevelFour.setOpaque(false);
		southTopRightPanel.removeAll();
		southTopRightPanel.add(valuetableLabel);
		southBottomPanel.setLayout(new BoxLayout(southBottomPanel, BoxLayout.LINE_AXIS));
		southBottomPanel.setPreferredSize(new Dimension(0, 60));
		southBottomPanel.add(changeQuantityButton);
		southBottomPanel.add(checkButton);
		southBottomPanel.add(mainMenuButton);
		southBottomPanel.add(statisticsButton);
		southBottomPanel.add(helpButton);
		tableLevelFour.setRowHeight(40);
		tableLevelFour.getColumnModel().getColumn(0).setPreferredWidth(90);
		tableLevelFour.getColumnModel().getColumn(1).setPreferredWidth(90);
		tableLevelFour.getColumnModel().getColumn(2).setPreferredWidth(90);
		tableLevelFour.getColumnModel().getColumn(3).setPreferredWidth(90);
		tableLevelFour.getColumnModel().getColumn(4).setPreferredWidth(90);
		tableLevelFour.getColumnModel().getColumn(5).setPreferredWidth(90);
		tableLevelFour.setRowSelectionAllowed(false);
		tableLevelFour.setBorder(BorderFactory.createLineBorder(Color.white, 2));
		((DefaultTableCellRenderer) tableLevelFour.getDefaultRenderer(Object.class)).setOpaque(false);
		((DefaultTableCellRenderer) tableLevelFour.getDefaultRenderer(Object.class)).setForeground(Color.WHITE);
		southTopRightPanel.add(tableLevelFour);
		tableLevelFour.setValueAt("x", 0, 0);
		tableLevelFour.setValueAt("y", 1, 0);
		if (quantity == 1) {
			tableLevelFour.removeColumn(tableLevelFour.getColumnModel().getColumn(4));
			tableLevelFour.removeColumn(tableLevelFour.getColumnModel().getColumn(3));
			tableLevelFour.removeColumn(tableLevelFour.getColumnModel().getColumn(2));
			tableLevelFour.removeColumn(tableLevelFour.getColumnModel().getColumn(1));
		} else if (quantity == 2) {
			tableLevelFour.removeColumn(tableLevelFour.getColumnModel().getColumn(3));
			tableLevelFour.removeColumn(tableLevelFour.getColumnModel().getColumn(2));
			tableLevelFour.removeColumn(tableLevelFour.getColumnModel().getColumn(1));
		} else if (quantity == 3) {
			tableLevelFour.removeColumn(tableLevelFour.getColumnModel().getColumn(2));
			tableLevelFour.removeColumn(tableLevelFour.getColumnModel().getColumn(1));
		} else if (quantity == 4) {
			tableLevelFour.removeColumn(tableLevelFour.getColumnModel().getColumn(1));
		}
		southTopRightPanel.add(Box.createHorizontalStrut(20));
		tableLevelFour.validate();
		southTopRightPanel.repaint();
	}

	/**
	 * Methode zum Aendern der Anzahl in der Oberflaeche
	 */
	public void changeQuantityLevelThreeFour() {
		southTopRightPanel.removeAll();
		quantityXValueTextField.setText("");
		southTopRightPanel.add(quantityXValueLabel);
		southTopRightPanel.add(quantityXValueTextField);
		southTopRightPanel.add(readyButton);
		southTopRightPanel.add(Box.createHorizontalStrut(20));
		southBottomPanel.remove(changeQuantityButton);
		southTopRightPanel.revalidate();
		southBottomPanel.revalidate();
		southTopRightPanel.repaint();
		southBottomPanel.repaint();
	}

	/**
	 * das Northpanel wird erstellt und grundsaetzlich gefuellt
	 */
	public void createNorthPanel() {
		northRightPanel.setLayout(new BoxLayout(northRightPanel, BoxLayout.LINE_AXIS));
		northLeftPanel.setLayout(new BoxLayout(northLeftPanel, BoxLayout.LINE_AXIS));
		northLeftPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		northRightPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		northRightPanel.add(Box.createHorizontalStrut(20));
		northLeftPanel.add(Box.createHorizontalStrut(20));
	}

	/**
	 * loescht den Inhalt aus dem Northpanel und Southpanel
	 */
	public void removeContent() {
		northLeftPanel.removeAll();
		northRightPanel.removeAll();
		northPanel.removeAll();
		southPanel.removeAll();
		southTopRightPanel.removeAll();
		southTopLeftPanel.removeAll();
		southBottomPanel.removeAll();
		southPanel.revalidate();
		southPanel.repaint();
	}

	/**
	 * Aendert das Southpanel so, dass nur noch ein Weiterbutton sichtbar ist fuer
	 * den Einstieg ins naechste Level
	 */
	public void goToNextLevel() {
		southBottomPanel.removeAll();
		southTopLeftPanel.removeAll();
		southTopRightPanel.removeAll();
		southTopPanel.removeAll();
		southPanel.removeAll();
		southPanel.setLayout(new GridBagLayout());
		goOnButton.setVerticalTextPosition(SwingConstants.CENTER);
		goOnButton.setHorizontalTextPosition(SwingConstants.CENTER);
		southPanel.add(goOnButton);
		southPanel.revalidate();
		southPanel.repaint();
		goOnButton.requestFocus();
		goOnButton.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					getGoOnButton().doClick();
				}
			}
		});
	}
	
	/**
	 * Aendert das Southpanel so, dass nur noch ein Weiterbutton sichtbar ist fuer
	 * den Einstieg ins naechste Level
	 */
	public void goToNextCheck() {
		southBottomPanel.removeAll();
		southTopLeftPanel.removeAll();
		southTopRightPanel.removeAll();
		southTopPanel.removeAll();
		southPanel.removeAll();
		southPanel.setLayout(new GridBagLayout());
		goAheadButton.setVerticalTextPosition(SwingConstants.CENTER);
		goAheadButton.setHorizontalTextPosition(SwingConstants.CENTER);
		southPanel.add(goAheadButton);
		southPanel.revalidate();
		southPanel.repaint();
		goAheadButton.requestFocus();
		goAheadButton.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					goAheadButton.doClick();
				}
			}
		});
	}

	// TODO Henning: Bitte JavaDoc machen.
	public void displayResults(boolean success, boolean[] result) {
		if (success == true) {
			System.out.println("Alles korrekt!");
			JOptionPane.showMessageDialog(null, "Alles war korrekt!");
		} else {
			System.out.println("Einzelne Booleans werden ueberprueft");
			int i = 1;
			int count = 0;
			String msg = "";
			for (boolean versuch : result) {
				if (versuch == true) {

					System.out.println("Versuch" + i + "richtig");

				} else {
					msg += i + ",";
					count += 1;
					System.out.println("Versuch" + i + "gescheitert");
				}
				i++;
			}
			JOptionPane.showMessageDialog(null, count + " Eingaben falsch: " + msg);
		}
	}

	/**
	 * setzt die Farben aller Komponenten
	 */
	public void setForeAndBackgroundOfAllComponents() {
		scoreLabel.setForeground(Color.WHITE);
		equationLabel.setForeground(Color.WHITE);
		equation.setForeground(Color.WHITE);
		valuetableLabel.setForeground(Color.WHITE);
		tableLevelOne.setSelectionForeground(Color.WHITE);
		valuetableLabel.setForeground(Color.WHITE);
		tableLevelTwo.setSelectionForeground(Color.WHITE);
		quantityXValueLabel.setForeground(Color.WHITE);
		quantityXValueTextField.setForeground(Color.WHITE);
		scoreOutputLabel.setForeground(Color.WHITE);
		turnLabel.setForeground(Color.WHITE);
		turnOutputLabel.setForeground(Color.WHITE);
		readyButton.setForeground(Color.WHITE);
		readyButton.setBackground(Color.BLACK);
		helpButton.setBackground(Color.BLACK);
		statisticsButton.setForeground(Color.WHITE);
		statisticsButton.setBackground(Color.BLACK);
		helpButton.setForeground(Color.WHITE);
		mainMenuButton.setForeground(Color.WHITE);
		mainMenuButton.setBackground(Color.BLACK);
		checkButton.setForeground(Color.WHITE);
		checkButton.setBackground(Color.BLACK);
		changeQuantityButton.setBackground(Color.BLACK);
		changeQuantityButton.setForeground(Color.WHITE);
		equation.setForeground(Color.WHITE);
		equationsLabel.setForeground(Color.WHITE);
		quantityXValueLabel.setForeground(Color.WHITE);
		quantityXValueTextField.setForeground(Color.WHITE);
		readyButton.setForeground(Color.WHITE);
		readyButton.setBackground(Color.BLACK);
		secondTermLabel.setForeground(Color.WHITE);
		koefficientOne.setForeground(Color.WHITE);
		koefficientTwo.setForeground(Color.WHITE);
		koefficientThree.setForeground(Color.WHITE);
		koefficientFour.setForeground(Color.WHITE);
		exponentOne.setForeground(Color.WHITE);
		exponentTwo.setForeground(Color.WHITE);
		exponentThree.setForeground(Color.WHITE);
		tableLevelFive.setForeground(Color.WHITE);
		goOnButton.setBackground(Color.BLACK);
		goOnButton.setForeground(Color.white);
		goAheadButton.setForeground(Color.WHITE);
		goAheadButton.setBackground(Color.black);

	}

	/**
	 * setzt alle Komponenten auf durchsichtig
	 */
	public void setAllComponentsOpaque() {
		westPanel.setOpaque(false);
		eastPanel.setOpaque(false);
		northPanel.setOpaque(false);
		northRightPanel.setOpaque(false);
		northLeftPanel.setOpaque(false);
		southPanel.setOpaque(false);
		southTopPanel.setOpaque(false);
		southTopLeftPanel.setOpaque(false);
		southTopRightPanel.setOpaque(false);
		southBottomPanel.setOpaque(false);
		goOnButton.setOpaque(false);
		valuetableLabel.setOpaque(false);
		tableLevelTwo.setOpaque(false);
		quantityXValueTextField.setOpaque(false);
		readyButton.setOpaque(false);
		valuetableLabel.setOpaque(false);
		tableLevelOne.setOpaque(false);
		quantityXValueTextField.setOpaque(false);
		readyButton.setOpaque(false);
		changeQuantityButton.setOpaque(false);
		mainMenuButton.setOpaque(false);
		checkButton.setOpaque(false);
		helpButton.setOpaque(false);
		statisticsButton.setOpaque(false);
		koefficientOne.setOpaque(false);
		koefficientTwo.setOpaque(false);
		koefficientThree.setOpaque(false);
		koefficientFour.setOpaque(false);
		exponentOne.setOpaque(false);
		exponentTwo.setOpaque(false);
		exponentThree.setOpaque(false);
		goAheadButton.setOpaque(false);
		tableLevelFive.setOpaque(false);

	}

	/**
	 * setzt die Schrift aller Komponenten
	 */
	public void setFontOfAllComponents() {
		equationLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		equation.setFont(new Font("Colonna MT", Font.BOLD, 35));
		scoreLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		scoreOutputLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		turnOutputLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		turnLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		readyButton.setFont(new Font("Colonna MT", Font.BOLD, 35));
		quantityXValueLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		quantityXValueTextField.setFont(new Font("Colonna MT", Font.BOLD, 35));
		valuetableLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		tableLevelOne.setFont(new Font("Colonna MT", Font.BOLD, 35));
		tableLevelTwo.setFont(new Font("Colonna MT", Font.BOLD, 35));
		equation.setFont(new Font("Colonna MT", Font.BOLD, 35));
		valuetableLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		equationLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		readyButton.setFont(new Font("Colonna MT", Font.BOLD, 35));
		changeQuantityButton.setFont(new Font("Colonna MT", Font.BOLD, 30));
		checkButton.setFont(new Font("Colonna MT", Font.BOLD, 30));
		statisticsButton.setFont(new Font("Colonna MT", Font.BOLD, 30));
		mainMenuButton.setFont(new Font("Colonna MT", Font.BOLD, 30));
		helpButton.setFont(new Font("Colonna MT", Font.BOLD, 30));
		quantityXValueTextField.setFont(new Font("Colonna MT", Font.BOLD, 35));
		quantityXValueLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		equationsLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		equation.setFont(new Font("Colonna MT", Font.BOLD, 35));
		secondTermLabel.setFont(new Font("Colonna MT", Font.BOLD, 35));
		koefficientOne.setFont(new Font("Colonna MT", Font.BOLD, 35));
		koefficientTwo.setFont(new Font("Colonna MT", Font.BOLD, 35));
		koefficientThree.setFont(new Font("Colonna MT", Font.BOLD, 35));
		koefficientFour.setFont(new Font("Colonna MT", Font.BOLD, 35));
		exponentOne.setFont(new Font("Colonna MT", Font.BOLD, 35));
		exponentTwo.setFont(new Font("Colonna MT", Font.BOLD, 35));
		exponentThree.setFont(new Font("Colonna MT", Font.BOLD, 35));
		tableLevelFive.setFont(new Font("Colonna MT", Font.BOLD, 35));
		goOnButton.setFont(new Font("Colonna MT", Font.BOLD, 40));
		goAheadButton.setFont(new Font("Colonna MT", Font.BOLD, 40));

	}

	/**
	 * fuegt das Baeumebild im Hintergrund ein
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.BAEUME, 0, 0, null);

	}

	/**
	 * 
	 * @return goOnButton der Button zur Weiterleitung ans naechste Level, wenn die
	 *         Regeln zum WEiterkommen ins naechste Level erfuellt sind
	 */
	public JButton getGoOnButton() {
		return goOnButton;
	}
	
	public JButton getGoAheadButton() {
		return goAheadButton;
	}

	/**
	 * 
	 * @return quantity die Anzahl der Spalten, die benoetigt werden
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * 
	 * @return level das akutelle Level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * 
	 * @return checkButton prueft das Ergebnis auf Korrektheit
	 */
	public JButton getCheckButton() {
		return checkButton;
	}

	/**
	 * 
	 * @return mainMenuButton Button zum Zurueckgehen ins Hauptmenue
	 */
	public JButton getMainMenuButton() {
		return mainMenuButton;
	}

	/**
	 * 
	 * @return helpButton Button zum Anzeigen des Hilfemenues
	 */
	public JButton getHelpButton() {
		return helpButton;
	}

	/**
	 * 
	 * @return statisticsButton Button zum Anzeigen der Level-Spielerstatistik
	 */
	public JButton getStatisticsButton() {
		return statisticsButton;
	}

	/**
	 * 
	 * @return equation das Label mit dem Text 'Gleichung'
	 */
	public JLabel getEquation() {
		return equation;
	}

	/**
	 * 
	 * @return quantityXValueTextField das Eingabefeld fuer die Anzahl der Spalten
	 */
	public JTextField getQuantityXValueTextField() {
		return quantityXValueTextField;
	}

	/**
	 * 
	 * @return readyButton Button zum Anzeigen der Tabelle nach Eingabe der Spalten
	 *         der Anzahl
	 */
	public JButton getReadyButton() {
		return readyButton;
	}

	/**
	 * 
	 * @return changeQuantityButton Button zum Aendern der Anzahl der Spalten
	 */
	public JButton getChangeQuantityButton() {
		return changeQuantityButton;
	}

	/**
	 * 
	 * @return secondTermLabel das Label fuer die zweite Gleichung
	 */
	public JLabel getSecondTermLabel() {
		return secondTermLabel;

	}

	// TODO Henning: Bitte JavaDoc machen.
	public double[] getXValues() {
		double[] xValues;
		switch (this.getLevel()) {
		case 1:
			xValues = new double[4];
			for (int i = 0; i < xValues.length; i++) {
				try {
					String text = "" + this.tableLevelOne.getValueAt(0, i+1);
					text = text.replace(',', '.');
					xValues[i] = Double.parseDouble(text);
				} catch (NumberFormatException | NullPointerException e) {
					return mistake;
				}
			}

			return xValues;
		case 2:
			xValues = new double[quantity];
			for (int i = 0; i < xValues.length; i++) {
				try {
					String text = "" + this.tableLevelTwo.getValueAt(0, i+1);
					text = text.replace(',', '.');
					xValues[i] = Double.parseDouble(text);
				} catch (NumberFormatException | NullPointerException e) {
					return mistake;
				}
			}
			return xValues;
		case 3:
			xValues = new double[quantity];
			for (int i = 0; i < xValues.length; i++) {
				try {
					String text = "" + this.tableLevelThree.getValueAt(0, i+1);
					text = text.replace(',', '.');
					xValues[i] = Double.parseDouble(text);
				} catch (NumberFormatException | NullPointerException e) {
					return mistake;

				}
			}
			return xValues;
		case 4:
			xValues = new double[quantity];
			for (int i = 0; i < xValues.length; i++) {
				try {
					String text = "" + this.tableLevelFour.getValueAt(0, i+1);
					text = text.replace(',', '.');
					xValues[i] = Double.parseDouble(text);
				} catch (NumberFormatException | NullPointerException e) {
					return mistake;
				}
			}
			return xValues;
		default:
			return null;
		}
	}

	/**
	 * 
	 * @return tableLevelFive die Tabelle von Level 5
	 */
	public TableLevelFive getTableLevelFive() {
		return tableLevelFive;
	}

	// TODO Henning: Bitte JavaDoc machen.
	public double[] getYValues() {
		double[] yValues;
		switch (this.getLevel()) {
		case 1:
			yValues = new double[4];
			for (int i = 0; i < yValues.length; i++) {
				try { 
					String text = "" + this.tableLevelOne.getValueAt(1, i + 1);
					text = text.replace(',', '.');
					yValues[i] = Double.parseDouble(text);
				} catch (NumberFormatException | NullPointerException e) {
					return mistake;
				}

			}
			return yValues;
		case 2:
			yValues = new double[quantity];
			for (int i = 0; i < yValues.length; i++) {
				try {
					String text = "" + this.tableLevelTwo.getValueAt(1, i + 1);
					text = text.replace(',', '.');
					yValues[i] = Double.parseDouble(text);
				} catch (NumberFormatException | NullPointerException e) {
					return mistake;
				}

			}
			return yValues;
		case 4:
			yValues = new double[quantity];
			for (int i = 0; i < yValues.length; i++) {
				try {
					String text = "" + this.tableLevelFour.getValueAt(1, i + 1);
					text = text.replace(',', '.');
					yValues[i] = Double.parseDouble(text);
				} catch (NumberFormatException | NullPointerException e) {
					return mistake;
				}

			}
			return yValues;
		default:
			return null;
		}
	}

	// TODO Henning: Bitte JavaDoc machen.
	public char[] getType() {
		char[] exit = new char[200];
		if (this.getLevel() == 2) {
			char[] type = new char[quantity];
			try {
				for (int i = 0; i < type.length; i++) {
					if(((String) this.tableLevelTwo.getValueAt(2, i + 1)).length() == 1) {
					if (this.tableLevelTwo.getValueAt(2, i + 1) != null) {
					type[i] = ((String) this.tableLevelTwo.getValueAt(2, i + 1)).toUpperCase().charAt(0);
					}else {
						JOptionPane.showMessageDialog(null, "Bitte Extremstelle eingeben!");
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "Bitte nur einen Buchstaben eingeben");
				}}
			} catch (NullPointerException e) {
				return exit;
			}
			return type;
		}
		return null;
	}

	// TODO Henning: Bitte JavaDoc machen.
	public double[] getCoefficients() {
		double[] coeff = new double[quantity];
		if (quantity == 3) {
			coeff[0] = Double.parseDouble(koefficientTwo.getText());
			coeff[1] = Double.parseDouble(koefficientThree.getText());
			coeff[2] = Double.parseDouble(koefficientFour.getText());
		}
		if (quantity == 4) {
			coeff[0] = Double.parseDouble(koefficientOne.getText());
			coeff[1] = Double.parseDouble(koefficientTwo.getText());
			coeff[2] = Double.parseDouble(koefficientThree.getText());
			coeff[3] = Double.parseDouble(koefficientFour.getText());
		}
		for (int i = 0; i < coeff.length / 2; i++) {
			double temp = coeff[i];
			coeff[i] = coeff[coeff.length - i - 1];
			coeff[coeff.length - i - 1] = temp;
		}
		return coeff;
	}

	/**
	 *
	 * @param score der aktuelle Punktestand
	 */
	public void setScoreOutputLabel(Integer score) {
		this.scoreOutputLabel.setText(Integer.toString(score));
	}

	/**
	 *
	 * @param steps die Anzahl der Spielz�ge werden gesetzt
	 */
	public void setTurnOutputLabel(Integer steps) {
		this.turnOutputLabel.setText(Integer.toString(steps));
	}

	/**
	 * 
	 * @param level das aktuelle Level
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * 
	 * @param quantity die Anzahl der Spalten, die benoetigt werden
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
