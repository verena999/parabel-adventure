
package view;

import java.awt.Color;

import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * Oberflaeche fuer die Leveleinleitungen
 * 
 * @author Verena Stech, Marcel Lievre
 *
 */
public class GUILevelIntroductions extends JPanel {

	private static final long serialVersionUID = 1L;
	private JButton continueButton = new JButton("Weiter");
	private JTextArea introductionTextArea = new JTextArea();
	@SuppressWarnings("unused")
	private int level;
	private JLabel levelIntroductionsLabel = new JLabel();

	/**
	 * 
	 * Konstruktor.
	 */
	public GUILevelIntroductions() {
		initialize();
	}

	/**
	 * setzt das Layout
	 * 
	 * fuegt alle Komponenten in die Oberflaeche ein
	 */
	public void initialize() {
		introductionTextArea.setOpaque(false);
		continueButton.setOpaque(false);
		levelIntroductionsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		continueButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		setLayout(new GridBagLayout());
		styleComponents();
		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.PAGE_AXIS));
		addButtonsToPanelComponents(panelButtons);
		panelButtons.setOpaque(false);
		add(panelButtons, new GridBagConstraints());
		
	}

	/**
	 * fuegt die Komponenten in das Panel ein
	 * 
	 * @param panelComponents das Hauptpanel
	 */
	public void addButtonsToPanelComponents(JPanel panelComponents) {
		panelComponents.add(getLevelIntroductionsLabel());
		panelComponents.add(getIntroductionTextArea());
		panelComponents.add(getContinueButton());
	}

	/**
	 * stylt alle Komponenten
	 */
	public void styleComponents() {
		getLevelIntroductionsLabel().setBackground(Color.BLACK);
		getLevelIntroductionsLabel().setForeground(Color.WHITE);
		getLevelIntroductionsLabel().setFont(new Font("Colonna MT", Font.BOLD, 70));
		getIntroductionTextArea().setEditable(false);
		getIntroductionTextArea().setBackground(Color.BLACK);
		getIntroductionTextArea().setForeground(Color.WHITE);
		getIntroductionTextArea().setFont(new Font("Colonna MT", Font.BOLD, 30));
		getContinueButton().setBackground(Color.BLACK);
		getContinueButton().setForeground(Color.WHITE);
		getContinueButton().setFont(new Font("Colonna MT", Font.BOLD, 40));
	}

	/**
	 * fuegt das Dschungelbild im Hintergrund ein
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.DSCHUNGEL, 0, 0, null);
	}


	/**
	 * 
	 * @return continueButton der Fortfahren-Button
	 */
	public JButton getContinueButton() {
		return continueButton;
	}

	/**
	 * 
	 * @return introductionTextArea TextArea mit Einleitungstext des Levels
	 */
	public JTextArea getIntroductionTextArea() {
		return introductionTextArea;
	}



	/**
	 * 
	 * @return levelIntroductionsLabel die Ueberschrift der Seite
	 */
	public JLabel getLevelIntroductionsLabel() {
		return levelIntroductionsLabel;
	}

	/**
	 * setzt das Level und je nach Level die entsprechende Einleitung pro Level
	 * 
	 * @param level das aktuelle Level
	 */
	public void setLevel(int level) {
		this.level = level;
		if (level == 1) {
			levelIntroductionsLabel.setText("Einleitung Level 1");
			introductionTextArea.setText("\n\nIhr teilt euch in zwei Teams auf und durchforstet den Dschungel.\n"
					+ "Das erste Team findet eine saubere Wasserquelle und das zweite\n"
					+ "Team findet einen kleinen Unterschlupf mit einer Feuerstelle.\r\n\n"
					+ "Nach einer Weile trefft ihr euch am Absturzpunkt wieder und \n"
					+ "besprecht euch. Ihr entscheidet euch dazu, zwei Karten auf das\n"
					+ "Papier zu zeichnen: Eine, um zur Wasserquelle zu gelangen und \n"
					+ "eine, um zum Unterschlupf zu gelangen. \r\n\n"
					+ "Trage hierzu die Y-Koordinaten in die Wertetablle ein \n"
					+ "oder markiere die Punkte auf dem Koordinatensystem \n"
					+ "und \u00FCbertrage sie in die Wertetabelle.\n\n");
		} else if (level == 2) {
			levelIntroductionsLabel.setText("Einleitung Level 2");
			introductionTextArea.setText("\n\nNachdem alle sicher und mit Wasser bepackt beim Unterschlupf\n"
					+ "angekommen sind, kommst du auf die Idee, die Berge und T\u00E4ler \n"
					+ "zu besuchen, um dir einen besseren \u00DCberblick \u00fcber die Insel zu\n"
					+ "verschaffen oder weitere n\u00FCtzliche Gegenst\u00E4nde zu finden.\r\n\n"
					+ "Um herauszufinden, wo auf der Insel die f\u00fcnf h\u00F6chsten Berge \n"
					+ "oder tiefsten T\u00E4ler sind, berechnest du die Extrempunkte und \n"
					+ "tr\u00E4gst sie entweder in die Wertetabelle ein oder markierst sie im \n"
					+ "Koordinatensystem und \u00FCbertr\u00E4gst sie in die Wertetabelle. \n"
					+ "Desweiteren musst du entscheiden, ob es sich um einen Hochpunkt\n"
					+ "oder einen Tiefpunkt handelt. \n\n"
					+ "Die letzten drei Extrempunkte m\u00fcssen in Folge richtig \nberechnet werden.\n\n");
		} else if (level == 3) {
			levelIntroductionsLabel.setText("Einleitung Level 3");
			introductionTextArea.setText("\n\nDu kommst nach erfolgreicher Erkundung wieder zum Unterschlupf. \n"
					+ "Auf deinem Weg durch den Dschungel hast du durch Zufall einen \n"
					+ "Fluss entdeckt, der sich \u00FCber den gesamten Dschungel von West \n" + "nach Ost streckt. \n\n"
					+ "An manchen Stellen des Ufers stehen Bananen- und Kokusnussb\u00E4ume. \r\n"
					+ "Du m\u00F6chtest den anderen Passagieren die Nahrungsquellen zeigen \n"
					+ "und so geht ihr einen Trampelpfad entlang, der den Fluss an den \n"
					+ "Stellen kreuzt, an denen die B\u00E4ume wachsen.\n\n"
					+ "Daf\u00fcr musst du die Anzahl der Nullstellen der Funktion bestimmen \n"
					+ "und diese in der Wertetabelle eintragen oder im Koordinatensystem \n"
					+ "markieren und in die Wertetabelle \u00FCbertragen.\r\n"
					+ "Du musst drei Mal hintereinander richtig gerechnet haben, \n"
					+ "um deine Aufgabe erfolgreich zu beenden.\n\n");
		} else if (level == 4) {
			levelIntroductionsLabel.setText("Einleitung Level 4");
			introductionTextArea.setText("\n\nDa euch beim gehen aufgefallen ist, dass sich eine tiefe Schlucht \n"
					+ "zwischen euch und und den B\u00E4umen befindet, suchst du nach einem \n"
					+ "anderen Weg, der den Trampelpfad, auf dem ihr gerade lauft, \n"
					+ "an einer sp\u00E4teren Stelle schneidet.\r\n\n"
					+ "Du nimmst dir eine Karte, die du unterwegs zuf\u00E4llig gefunden hast,\n"
					+ "zur Hilfe und siehst einen m\u00F6glichen Pfad.\r\n\n"
					+ "Finde heraus, wie oft und wo sich die beiden Wege schneiden und \n"
					+ "trage die Punkte in die Wertetabelle ein oder markiere sie im \n"
					+ "Koordinatensystem und \u00FCbertrage sie in die Wertetabelle.\n\n");
		} else if (level == 5) {
			levelIntroductionsLabel.setText("Einleitung Level 5");
			introductionTextArea.setText("\n\nNach mehreren verzweilfelten Hilferufen durch das Walkie Talkie,\n"
					+ "meldet sich jemand auf der anderen Leitung.\r\n\n"
					+ "Der Mann auf der anderen Seite des Walkie Talkies schickt die \n"
					+ "Hilferufe weiter an ein Rettungsdienst, der Rettungshelikopter " + "schickt.\r\n"
					+ "Doch leider kann der Helikopter nur an bestimmten Stellen landen.\n\n"
					+ "Berechne daf\u00FCr den Flugweg des Helikopters, damit er alle\n "
					+ "gegebenen Punkte schneidet.\r\n\n"
					+ "Dazu erg\u00E4nze den Term um die richtigen Koeffizienten, \n"
					+ "um alle aus deiner Truppe zu retten musst du drei mal den \n"
					+ "richtigen Term rausfinden.\n\n");
		}
	}


}
