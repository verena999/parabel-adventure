package view;

import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageUtils {

	public static final Image DSCHUNGEL = ImageUtils.loadImage("Dschungel.png");
	public static final Image BAEUME = ImageUtils.loadImage("Baeume.png");
	public static final Image KARTE = ImageUtils.loadImage("Karte.png");

	public static Image loadImage(String name) {
		try {
			ImageIO.setUseCache(false);
			return ImageIO.read(Thread.currentThread().getContextClassLoader().getResourceAsStream(name));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
		}
	}
}
