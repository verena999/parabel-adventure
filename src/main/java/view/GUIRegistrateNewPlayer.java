package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Oberflaeche fuer die Registrierung neuer Spieler
 * @author Verena Stech
 *
 */
public class GUIRegistrateNewPlayer extends JPanel {
	private static final long serialVersionUID = 1L;
	private JLabel placeholderLabel2 = new JLabel(" ");
	private JLabel placeholderLabel3 = new JLabel(" ");
	private JLabel placeholderLabel4 = new JLabel(" ");
	private JLabel newPlayerLabel = new JLabel("Neuer Spieler\n");
	private JLabel userNameLabel = new JLabel("Username: ");
	private JTextField usernameTextfield = new JTextField();
	private JButton backButton = new JButton("Zur\u00fcck");
	private JButton startButton = new JButton("Start");


	/**
	 * 
	 * Konstruktor.
	 */
	public GUIRegistrateNewPlayer() {
		initialize();
	}

	/**
	 * setzt das Layout
	 * 
	 * fuegt alle Komponenten in die Oberflaeche ein
	 */
	public void initialize() {
		setLayout(new GridBagLayout());
		styleComponents();
		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.PAGE_AXIS));
		addButtonsToPanelComponents(panelButtons);
		panelButtons.setOpaque(false);
		add(panelButtons, new GridBagConstraints());
	}

	/**
	 * fuegt die Komponenten in das Panel ein
	 * @param panelComponents das Hauptpanel
	 */
	public void addButtonsToPanelComponents(JPanel panelComponents) {
		panelComponents.add(getNewPlayerLabel());
		panelComponents.add(getUserNameLabel());
		panelComponents.add(placeholderLabel2);
		panelComponents.add(getUsernameTextfield());
		panelComponents.add(placeholderLabel3);
		panelComponents.add(getBackButton());
		panelComponents.add(placeholderLabel4);
		panelComponents.add(getStartButton());
	}


	/**
	 * stylt alle Komponenten
	 */
	public void styleComponents() {
		getNewPlayerLabel().setBackground(Color.BLACK);
		getNewPlayerLabel().setForeground(Color.WHITE);
		getNewPlayerLabel().setOpaque(false);
		getNewPlayerLabel().setAlignmentX(Component.CENTER_ALIGNMENT);
		getUserNameLabel().setBackground(Color.BLACK);
		getUserNameLabel().setForeground(Color.WHITE);
		getUserNameLabel().setOpaque(false);
		getUserNameLabel().setAlignmentX(Component.CENTER_ALIGNMENT);
		getUsernameTextfield().setBackground(Color.BLACK);
		getUsernameTextfield().setForeground(Color.WHITE);
		getUsernameTextfield().setOpaque(false);
		getUsernameTextfield().setAlignmentX(Component.CENTER_ALIGNMENT);
		getBackButton().setBackground(Color.BLACK);
		getBackButton().setForeground(Color.WHITE);
		getBackButton().setOpaque(false);
		getBackButton().setAlignmentX(Component.CENTER_ALIGNMENT);
		getStartButton().setBackground(Color.BLACK);
		getStartButton().setForeground(Color.WHITE);
		getStartButton().setOpaque(false);
		getStartButton().setAlignmentX(Component.CENTER_ALIGNMENT);

		setComponentsFont();
	}

	/**
	 * setzt die Schrift der Komponenten
	 */
	public void setComponentsFont() {
		getNewPlayerLabel().setFont(new Font("Colonna MT", Font.BOLD, 90));
		getUserNameLabel().setFont(new Font("Colonna MT", Font.BOLD, 40));
		getUsernameTextfield().setFont(new Font("Colonna MT", Font.BOLD, 40));
		getBackButton().setFont(new Font("Colonna MT", Font.BOLD, 40));
		getStartButton().setFont(new Font("Colonna MT", Font.BOLD, 40));
		placeholderLabel2.setFont(new Font("Colonna MT", Font.BOLD, 20));
		placeholderLabel3.setFont(new Font("Colonna MT", Font.BOLD, 20));
		placeholderLabel4.setFont(new Font("Colonna MT", Font.BOLD, 20));
	}

	/**
	 * fuegt das Dschungelbild im Hintergrund ein
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.DSCHUNGEL, 0, 0, null);
	}

	/**
	 * 
	 * @return newPlayerLabel die Ueberschrift der Seite
	 */
	public JLabel getNewPlayerLabel() {
		return newPlayerLabel;
	}

	/**
	 * 
	 * @return userNameLabel Label mit der Eingabeaufforderung eines Usernames
	 */
	public JLabel getUserNameLabel() {
		return userNameLabel;
	}

	/**
	 * 
	 * @return usernameTextfield Textfield zur Eingabe des Usernames
	 */
	public JTextField getUsernameTextfield() {
		return usernameTextfield;
	}

	/**
	 * 
	 * @return backButton der Zurueck-Button
	 */
	public JButton getBackButton() {
		return backButton;
	}

	/**
	 * 
	 * @return startButton der Button zur Weiterleitung ins Hauptmenue
	 */
	public JButton getStartButton() {
		return startButton;
	}
}
