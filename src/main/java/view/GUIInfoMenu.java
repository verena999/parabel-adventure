package view;

import java.awt.Color;

import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * Oberflaeche fuer das Infomenue
 * 
 * @author Verena Stech
 *
 */
public class GUIInfoMenu extends JPanel {

	private static final long serialVersionUID = 1L;
	private JButton backButton = new JButton("Zur\u00FCck");
	private JLabel headlineLabel = new JLabel("Spielinformationen");
	private JTextArea infoTextArea = new JTextArea("\nDieses Spiel wurde erstellt von Henning Bergmann, \n"
			+ "Marcel Lievre, Kevin Burchert, Thorsten Schiffer,\nFabian Urban "
			+ "und Verena Stech.\n\nBei Problemen bitte bei Frau Rollins melden.");

	/**
	 * 
	 * Konstruktor.
	 */
	public GUIInfoMenu() {
		initialize();
	}

	/**
	 * setzt das Layout
	 * 
	 * fuegt alle Komponenten in die Oberflaeche ein
	 */
	public void initialize() {
		infoTextArea.setOpaque(false);
		backButton.setOpaque(false);
		headlineLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		setLayout(new GridBagLayout());
		styleComponents();
		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.PAGE_AXIS));
		addButtonsToPanelComponents(panelButtons);
		panelButtons.setOpaque(false);
		add(panelButtons, new GridBagConstraints());

	}

	/**
	 * fuegt die Komponenten in das Hauptpanel ein
	 * 
	 * @param panelComponents das Hauptpanel
	 */
	public void addButtonsToPanelComponents(JPanel panelComponents) {
		panelComponents.add(getHeadlineLabel());
		panelComponents.add(getInfoTextArea());
		panelComponents.add(getBackButton());
	}

	/**
	 * stylt alle Komponenten
	 */
	public void styleComponents() {
		getHeadlineLabel().setBackground(Color.BLACK);
		getHeadlineLabel().setForeground(Color.WHITE);
		getHeadlineLabel().setFont(new Font("Colonna MT", Font.BOLD, 70));
		getInfoTextArea().setEditable(false);
		getInfoTextArea().setBackground(Color.BLACK);
		getInfoTextArea().setForeground(Color.WHITE);
		getInfoTextArea().setFont(new Font("Colonna MT", Font.BOLD, 30));
		getBackButton().setBackground(Color.BLACK);
		getBackButton().setForeground(Color.WHITE);
		getBackButton().setFont(new Font("Colonna MT", Font.BOLD, 30));
	}

	/**
	 * fuegt das Dschungelbild im Hintergrund ein
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.DSCHUNGEL, 0, 0, null);
	}

	/**
	 * 
	 * @return backButton der Zurueck-Button
	 */
	public JButton getBackButton() {
		return backButton;
	}

	/**
	 * 
	 * @return headlineLabel die Ueberschrift der Seite
	 */
	public JLabel getHeadlineLabel() {
		return headlineLabel;
	}

	/**
	 * 
	 * @return infoTextArea TextArea mit Informationen ueber die Entwickler
	 */
	public JTextArea getInfoTextArea() {
		return infoTextArea;
	}
}
