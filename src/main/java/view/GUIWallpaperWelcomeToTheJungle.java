package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Oberflaeche fuer die Willkommensseite des Spiels
 * @author Verena Stech
 *
 */
public class GUIWallpaperWelcomeToTheJungle extends JPanel {

	private static final long serialVersionUID = 1L;

	private JLabel headlineLabel = new JLabel("Welcome to the Jungle!");

	/**
	 * 
	 * Konstruktor.
	 */
	public GUIWallpaperWelcomeToTheJungle() {
		initialize();
	}

	/**
	 * setzt das Layout
	 * 
	 * fuegt alle Komponenten in die Oberflaeche ein
	 */
	public void initialize() {
		headlineLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		setLayout(new GridBagLayout());
		styleComponents();
		JPanel panelComponent = new JPanel();
		panelComponent.setLayout(new BoxLayout(panelComponent, BoxLayout.PAGE_AXIS));
		panelComponent.setOpaque(false);
		panelComponent.add(getLevelLabel());
		add(panelComponent, new GridBagConstraints());

	}


	/**
	 * stylt alle Komponenten
	 */
	public void styleComponents() {
		getLevelLabel().setBackground(Color.BLACK);
		getLevelLabel().setForeground(Color.WHITE);
		getLevelLabel().setFont(new Font("Colonna MT", Font.BOLD, 80));
	}

	/**
	 * fuegt das Dschungebild im Hintergrund ein
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.DSCHUNGEL, 0, 0, null);
	}

	/**
	 * 
	 * @return headlineLabel die Ueberschrift der Seite
	 */
	public JLabel getLevelLabel() {
		return headlineLabel;
	}

}
