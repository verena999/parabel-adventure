package view;

import java.awt.Color;

import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * Oberflaeche fuer die Gesamtbewertung des Spielers
 * 
 * @author Verena Stech
 *
 */
public class GUISummaryStatistics extends JPanel {

	private static final long serialVersionUID = 1L;
	private JButton backButton = new JButton("Zur�ck");
	private DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	private JLabel headlineLabel = new JLabel("Gesamtbewertung");
	private ChartPanel panel;

	/**
	 * 
	 * Konstruktor.
	 */
	public GUISummaryStatistics(DefaultCategoryDataset dataset) {
		this.dataset = dataset;
		//initialize();
	}

	/**
	 * setzt das Layout
	 * 
	 * fuegt alle Kompoenten in die Oberflaeche ein
	 */
	public void initialize(DefaultCategoryDataset dataset) {
		JFreeChart lineChart = ChartFactory.createLineChart("", "", "Punkte", dataset, PlotOrientation.VERTICAL, true, true, false);
		panel = new ChartPanel(lineChart);
		backButton.setOpaque(false);
		headlineLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		setLayout(new GridBagLayout());
		styleComponents();
		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.PAGE_AXIS));
		addButtonsToPanelComponents(panelButtons);
		panelButtons.setOpaque(false);
		add(panelButtons, new GridBagConstraints());

	}

	/**
	 * fuegt die Komponenten in das Panel ein
	 * 
	 * @param panelComponents das Hauptpanel
	 */
	public void addButtonsToPanelComponents(JPanel panelComponents) {
		panelComponents.add(getHeadlineLabel());
		panelComponents.add(panel);
		panelComponents.add(getBackButton());
	}

	/**
	 * stylt alle Komponenten
	 */
	public void styleComponents() {
		getHeadlineLabel().setBackground(Color.BLACK);
		getHeadlineLabel().setForeground(Color.WHITE);
		getHeadlineLabel().setFont(new Font("Colonna MT", Font.BOLD, 70));
		panel.setBackground(Color.BLACK);
		panel.setForeground(Color.WHITE);
		panel.setFont(new Font("Colonna MT", Font.BOLD, 30));
		getBackButton().setBackground(Color.BLACK);
		getBackButton().setForeground(Color.WHITE);
		getBackButton().setFont(new Font("Colonna MT", Font.BOLD, 30));
	}

	/**
	 * die Punkte werden dem Satz an Daten hinzugefuegt
	 * 
	 * @return dataset der Datensatz mit den Punkten des Spielers
	 */
	public DefaultCategoryDataset createDataset() {
		dataset.addValue(15, "Punkte", "1970");
		dataset.addValue(30, "Punkte", "1980");
		dataset.addValue(120, "Punkte", "2000");
		dataset.addValue(240, "Punkte", "2010");
		dataset.addValue(500, "Punkte", "2014");
		return dataset;
	}

	/**
	 * fuegt das Dschungelbild im Hintergrund ein
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.DSCHUNGEL, 0, 0, null);
	}

	/**
	 * 
	 * @return backButton der Zurueck-Button
	 */
	public JButton getBackButton() {
		return backButton;
	}

	/**
	 * 
	 * @return headlineLabel die Ueberschrift der Seite
	 */
	public JLabel getHeadlineLabel() {
		return headlineLabel;
	}

	public void setDataset(DefaultCategoryDataset dataset) {
		this.dataset = dataset;
	}

}
