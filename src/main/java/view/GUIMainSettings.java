package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;

/**
 * Grundeinstellungen der gesamten Anwendung
 * 
 * @author Verena Stech
 *
 */
public class GUIMainSettings extends JFrame {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * Konstruktor.
	 */
	public GUIMainSettings() {
		start();
	}

	/**
	 * Grundeinstellungen des Fensters
	 */
	public void start() {
		setTitle("Parabelabenteuerland");
		setSize(1400, 850);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		setVisible(true);
	}

}
