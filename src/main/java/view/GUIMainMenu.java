package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Oberflaeche fuer das Hauptmenue
 * 
 * @author Verena Stech
 *
 */
public class GUIMainMenu extends JPanel {

	private static final long serialVersionUID = 1L;
	private JButton buttonNewGame = new JButton("Neues Spiel");
	private JButton buttonContinue = new JButton("Spielstand laden");
	private JButton buttonStatis = new JButton("Statistik");
	private JButton buttonHelp = new JButton("Hilfe");
	private JButton buttonInfo = new JButton("Info");
	private JButton buttonLogOut = new JButton("Abmelden");

	/**
	 * 
	 * Konstruktor.
	 */
	public GUIMainMenu() {
		initialize();
	}

	/**
	 * setzt das Layout
	 * 
	 * fuegt alle Komponenten in die Oberflaeche ein
	 */
	public void initialize() {
		buttonNewGame.setVerticalTextPosition(SwingConstants.CENTER);
		buttonNewGame.setHorizontalTextPosition(SwingConstants.CENTER);
		setLayout(new GridBagLayout());
		setButtonFont();
		setButtonBackgroundAndForeground();
		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new GridLayout(6, 1));
		addButtonsToPanelComponents(panelButtons);
		panelButtons.setOpaque(false);
		add(panelButtons, new GridBagConstraints());
	}

	/**
	 * fuegt Komponenten in Panel ein
	 * @param panelComponents das Hauptpanel
	 */
	public void addButtonsToPanelComponents(JPanel panelComponents) {
		getButtonNewGame().setOpaque(false);
		getButtonContinue().setOpaque(false);
		getButtonHelp().setOpaque(false);
		getButtonInfo().setOpaque(false);
		getButtonLogOut().setOpaque(false);
		getButtonStatis().setOpaque(false);
		panelComponents.add(getButtonNewGame());
		panelComponents.add(getButtonContinue());
		panelComponents.add(getButtonStatis());
		panelComponents.add(getButtonHelp());
		panelComponents.add(getButtonInfo());
		panelComponents.add(getButtonLogOut());
	}

	/**
	 * setzt Farben der Komponenten
	 */
	public void setButtonBackgroundAndForeground() {
		getButtonContinue().setBackground(Color.BLACK);
		getButtonContinue().setForeground(Color.WHITE);
		getButtonNewGame().setBackground(Color.BLACK);
		getButtonNewGame().setForeground(Color.WHITE);
		getButtonHelp().setBackground(Color.BLACK);
		getButtonHelp().setForeground(Color.WHITE);
		getButtonInfo().setBackground(Color.BLACK);
		getButtonInfo().setForeground(Color.WHITE);
		getButtonLogOut().setBackground(Color.BLACK);
		getButtonLogOut().setForeground(Color.WHITE);
		getButtonStatis().setBackground(Color.BLACK);
		getButtonStatis().setForeground(Color.white);
	}

	/**
	 * setzt Schrift der Komponenten
	 */
	public void setButtonFont() {
		getButtonNewGame().setFont(new Font("Colonna MT", Font.BOLD, 40));
		getButtonContinue().setFont(new Font("Colonna MT", Font.BOLD, 40));
		getButtonHelp().setFont(new Font("Colonna MT", Font.BOLD, 40));
		getButtonInfo().setFont(new Font("Colonna MT", Font.BOLD, 40));
		getButtonLogOut().setFont(new Font("Colonna MT", Font.BOLD, 40));
		getButtonStatis().setFont(new Font("Colonna MT", Font.BOLD, 40));
	}

	/**
	 * fuegt das Dschungelbild im Hintergrund ein 
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.DSCHUNGEL, 0, 0, null);
	}

	/**
	 * 
	 * @return buttonContinue Button zum Laden des Spielstands
	 */
	public JButton getButtonContinue() {
		return buttonContinue;
	}

	/**
	 * 
	 * @return buttonNewGame Button zum Laden eines neuen Spiels
	 */
	public JButton getButtonNewGame() {
		return buttonNewGame;
	}

	/**
	 * 
	 * @return buttonHelp Button zum Oeffnen des Hilfsmenues
	 */
	public JButton getButtonHelp() {
		return buttonHelp;
	}

	/**
	 * 
	 * @return buttonInfo Button zum Oeffnen des Infomenues
	 */
	public JButton getButtonInfo() {
		return buttonInfo;
	}

	/**
	 * 
	 * @return buttonLogOut Button zum Abmelden des Spielers
	 */
	public JButton getButtonLogOut() {
		return buttonLogOut;
	}

	/**
	 * 
	 * @return buttonStatis Button zum Oeffnen der Gesamtbewertung des Spielers
	 */
	public JButton getButtonStatis() {
		return buttonStatis;
	}
}
