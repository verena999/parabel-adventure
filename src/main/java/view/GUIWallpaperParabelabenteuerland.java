package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * Oberflaeche f�r Ausgangsszenario der Spielerstory
 * @author Verena Stech
 *
 */
public class GUIWallpaperParabelabenteuerland extends JPanel {
	private static final long serialVersionUID = 1L;

	private JLabel headlineLabel = new JLabel("Parabelabenteuerland");
	private JTextArea beginnScenarioTextArea = new JTextArea();

	/**
	 * 
	 * Konstruktor.
	 */
	public GUIWallpaperParabelabenteuerland() {
		initialize();
	}

	/**
	 * setzt das Layout
	 * 
	 * fuegt alle Komponenten in die Oberflaeche ein
	 */
	public void initialize() {
		beginnScenarioTextArea
				.setText(
						"\n\n\nNach einem Flugzeugabsturz strandest du zusammen mit anderen \nPassagieren mitten im Dschungel auf einer einsamen Insel.\r\n\n" +
								"Das Einzige, das ihr aus dem Flugzeugwrack retten konntet \nist etwas Papier, ein Stift und ein Walkie Talkie.");
		beginnScenarioTextArea.setOpaque(false);
		headlineLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		setLayout(new GridBagLayout());
		styleComponents();
		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.PAGE_AXIS));
		addButtonsToPanelComponents(panelButtons);
		panelButtons.setOpaque(false);
		add(panelButtons, new GridBagConstraints());

	}

	/**
	 * fuegt die Komponenten in das Panel ein
	 * @param panelComponents das Hauptpanel
	 */
	public void addButtonsToPanelComponents(JPanel panelComponents) {
		panelComponents.add(getLevelLabel());
		panelComponents.add(getBeginnScenarioTextArea());
	}

	/**
	 * stylt alle Komponenten
	 */
	public void styleComponents() {
		getLevelLabel().setBackground(Color.BLACK);
		getLevelLabel().setForeground(Color.WHITE);
		getLevelLabel().setFont(new Font("Colonna MT", Font.BOLD, 100));
		getBeginnScenarioTextArea().setEditable(false);
		getBeginnScenarioTextArea().setBackground(Color.BLACK);
		getBeginnScenarioTextArea().setForeground(Color.WHITE);
		getBeginnScenarioTextArea().setFont(new Font("Colonna MT", Font.BOLD, 40));
	}

	/**
	 * fuegt das Dschungelbild im Hintergrund ein 
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.DSCHUNGEL, 0, 0, null);
	}

	/**
	 * 
	 * @return headlineLabel die Ueberschrift der Seite
	 */
	public JLabel getLevelLabel() {
		return headlineLabel;
	}

	/**
	 * 
	 * @return beginnScenarioTextArea TextArea mit Ausgangsszenario
	 */
	public JTextArea getBeginnScenarioTextArea() {
		return beginnScenarioTextArea;
	}
}
