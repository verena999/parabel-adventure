package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.text.View;

//TODO Verena: JavaDoc
/**
 * Klasse fuer Tabelle fuer Level 1
 * @author Verena Stech
 *
 */
public class TablesLevelOne extends JTable {

	private static final long serialVersionUID = 1L;
	private List<DefaultCellEditor> columnEditors = new ArrayList<>();

	/**
	 * 
	 * Konstruktor.
	 */
	public TablesLevelOne(int rows, int column, GUILevels view) {
		super(rows, column);
		((DefaultTableCellRenderer) this.getDefaultRenderer(Object.class)).setHorizontalAlignment(SwingConstants.CENTER);
		this.getColumnModel().getColumn(1).setCellEditor(createCellEditor());
		this.getColumnModel().getColumn(2).setCellEditor(createCellEditor());
		this.getColumnModel().getColumn(3).setCellEditor(createCellEditor());
		this.getColumnModel().getColumn(4).setCellEditor(createCellEditor());
		this.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if(e.getOppositeComponent() != null && !"TableCellTextField".equals(e.getOppositeComponent().getName())) {
					columnEditors.forEach(editor -> editor.stopCellEditing());
				}
			}
		});
		
		this.setSelectionForeground(Color.WHITE);
		this.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					columnEditors.forEach(editor -> editor.stopCellEditing());
					view.getCheckButton().doClick();
				}
			}

		});
	};

	public DefaultCellEditor createCellEditor() {
		JTextField textField = new JTextField();
		textField.setName("TableCellTextField");
		textField.setOpaque(false);
		textField.setForeground(Color.WHITE);
		textField.setFont(new Font("Colonna MT", Font.BOLD, 25));
		DefaultCellEditor defaultCellEditor = new DefaultCellEditor(textField);
		columnEditors.add(defaultCellEditor);
		return defaultCellEditor;
	}

	/**
	 * Tabelle kann erst ab der 2. Zeile und ab der 2. Spalte beschriftet werden
	 */
	@Override
	public boolean isCellEditable(int row, int column) {
		if (row == 0 || column == 0) {

			return false;
		}
		return super.isCellEditable(row, column);
	}

}
