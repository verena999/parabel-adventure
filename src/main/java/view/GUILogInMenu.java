package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Oberflaeche fur das Log In Menue
 * @author Verena Stech
 *
 */
public class GUILogInMenu extends JPanel {

	private static final long serialVersionUID = 1L;
	private JLabel logInMenuLabel = new JLabel("Spielermen\u00fc\n\n\n");
	private JLabel nameInputTextArea = new JLabel("\nBitte Namen eingeben:");
	private JTextField nameInputTextfield = new JTextField();
	private JButton registrateButton = new JButton("Noch nicht registriert? Hier registrieren!");
	private JButton startButton = new JButton("Start");
	private JLabel placeholderLabel1 = new JLabel(" ");
	private JLabel placeholderLabel2 = new JLabel(" ");
	private JLabel placeholderLabel3 = new JLabel(" ");
	private JLabel placeholderLabel4 = new JLabel(" ");

	/**
	 * 
	 * Konstruktor.
	 */
	public GUILogInMenu() {
		initialize();
	}

	/**
	 * setzt das Layout
	 * 
	 * fuegt alle Komponenten in die Oberflaeche ein
	 */
	public void initialize() {
		setLayout(new GridBagLayout());
		styleComponents();
		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.PAGE_AXIS));
		addButtonsToPanelComponents(panelButtons);
		panelButtons.setOpaque(false);
		add(panelButtons, new GridBagConstraints());
		
	}

	/**
	 * fuegt die Komponenten in das Panel ein
	 * @param panelComponents das Hauptpanel
	 */
	public void addButtonsToPanelComponents(JPanel panelComponents) {
		panelComponents.add(getLogInMenuLabel());
		panelComponents.add(placeholderLabel1);
		panelComponents.add(getNameInputTextArea());
		panelComponents.add(placeholderLabel2);
		panelComponents.add(getNameInputTextfield());
		panelComponents.add(placeholderLabel3);
		panelComponents.add(getStartButton());
		panelComponents.add(placeholderLabel4);
		panelComponents.add(getRegistrateButton());
	}


	/**
	 * stylt alle Komponenten
	 */
	public void styleComponents() {
		getLogInMenuLabel().setBackground(Color.BLACK);
		getLogInMenuLabel().setForeground(Color.WHITE);
		getLogInMenuLabel().setOpaque(false);
		getLogInMenuLabel().setAlignmentX(Component.CENTER_ALIGNMENT);
		getNameInputTextArea().setBackground(Color.BLACK);
		getNameInputTextArea().setForeground(Color.WHITE);
		getNameInputTextArea().setOpaque(false);
		getNameInputTextArea().setAlignmentX(Component.CENTER_ALIGNMENT);
		getNameInputTextfield().setBackground(Color.BLACK);
		getNameInputTextfield().setForeground(Color.WHITE);
		getNameInputTextfield().setOpaque(false);
		getNameInputTextfield().setAlignmentX(Component.CENTER_ALIGNMENT);
		getRegistrateButton().setBackground(Color.BLACK);
		getRegistrateButton().setForeground(Color.WHITE);
		getRegistrateButton().setOpaque(false);
		getRegistrateButton().setAlignmentX(Component.CENTER_ALIGNMENT);
		getStartButton().setBackground(Color.BLACK);
		getStartButton().setForeground(Color.WHITE);
		getStartButton().setOpaque(false);
		getStartButton().setAlignmentX(Component.CENTER_ALIGNMENT);

		setComponentsFont();
	}

	/**
	 * setzt die Schrift der Komponenten
	 */
	public void setComponentsFont() {
		placeholderLabel1.setFont(new Font("Colonna MT", Font.BOLD, 80));
		placeholderLabel2.setFont(new Font("Colonna MT", Font.BOLD, 20));
		placeholderLabel3.setFont(new Font("Colonna MT", Font.BOLD, 20));
		placeholderLabel4.setFont(new Font("Colonna MT", Font.BOLD, 20));
		getLogInMenuLabel().setFont(new Font("Colonna MT", Font.BOLD, 90));
		getNameInputTextArea().setFont(new Font("Colonna MT", Font.BOLD, 20));
		getNameInputTextfield().setFont(new Font("Colonna MT", Font.BOLD, 40));
		getRegistrateButton().setFont(new Font("Colonna MT", Font.BOLD, 20));
		getStartButton().setFont(new Font("Colonna MT", Font.BOLD, 40));
	}

	/**
	 * fuegt das Dschungelbild im Hintergrund ein
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.DSCHUNGEL, 0, 0, null);
	}

	/**
	 * 
	 * @return logInMenuLabel die Ueberschrift der Seite
	 */
	public JLabel getLogInMenuLabel() {
		return logInMenuLabel;
	}

	/**
	 * 
	 * @return nameInputTextArea TextArea mit Eingabeaufforderung des Spielernamens
	 */
	public JLabel getNameInputTextArea() {
		return nameInputTextArea;
	}

	/**
	 * 
	 * @return nameInputTextfield das Textfeld zur Eingabe des Spielernamens
	 */
	public JTextField getNameInputTextfield() {
		return nameInputTextfield;
	}

	/**
	 * 
	 * @return registrateButton der Button zur Weiterleitung ins Registrierungsmenue
	 */
	public JButton getRegistrateButton() {
		return registrateButton;
	}

	/**
	 * 
	 * @return startButton der Button zur Weiterleitung ins Hauptmenue
	 */
	public JButton getStartButton() {
		return startButton;
	}
	
}
