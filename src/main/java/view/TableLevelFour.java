package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.DefaultCellEditor;
import javax.swing.InputMap;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

//TODO Verena: JavaDoc
/**
 * Klasse fuer Tabelle fuer Level 2,3,4
 * @author Verena Stech
 *
 */
public class TableLevelFour extends JTable {

	private static final long serialVersionUID = 1L;
	private List<DefaultCellEditor> columnEditors = new ArrayList<>();

	/**
	 * 
	 * Konstruktor.
	 */
	public TableLevelFour(int rows, int column, GUILevels view) {
		super(rows, column);
		((DefaultTableCellRenderer) this.getDefaultRenderer(Object.class)).setHorizontalAlignment(SwingConstants.CENTER);
			this.getColumnModel().getColumn(1).setCellEditor(createCellEditor());
			this.getColumnModel().getColumn(2).setCellEditor(createCellEditor());
			this.getColumnModel().getColumn(3).setCellEditor(createCellEditor());
			this.getColumnModel().getColumn(4).setCellEditor(createCellEditor());
			this.getColumnModel().getColumn(5).setCellEditor(createCellEditor());

		this.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if(e.getOppositeComponent() != null && !"TableCellTextField".equals(e.getOppositeComponent().getName())) {
					columnEditors.forEach(editor -> editor.stopCellEditing());
				}
			}
		});
		this.setSelectionForeground(Color.WHITE);
		tabController(this, 0, rows, 1, column);
		
		this.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					columnEditors.forEach(editor -> editor.stopCellEditing());
					view.getCheckButton().doClick();
				}
			}
		});
	}

	public DefaultCellEditor createCellEditor() {
		JTextField textField = new JTextField();
		textField.setName("TableCellTextField");
		textField.setOpaque(false);
		textField.setForeground(Color.WHITE);
		textField.setFont(new Font("Colonna MT", Font.BOLD, 25));
		DefaultCellEditor defaultCellEditor = new DefaultCellEditor(textField);
		columnEditors.add(defaultCellEditor);
		return defaultCellEditor;
	}
	
	/**
	 * Tabelle kann erst ab der 2. Spalte beschriftet werden
	 */
	@Override
	public boolean isCellEditable(int row, int column) {
		if (column == 0) {
			return false;
		}
		return super.isCellEditable(row, column);
	}
	
	/**
	 * 
	 * man kann in der Tabelle f�r Level 4 von Eingabefeld zu Eingabefeld zuspringen
	 * 
	 *@author Marcel Lievre
	 */
		@SuppressWarnings("serial")
		public void tabController(JTable theTable, int startRow, int numberRows, int startColum, int numberColum) {
			if(theTable == null) {
				throw new IllegalArgumentException("Die Tabelle ist null");
			}
			
			int endRow = startRow + (numberRows - 1);
			int endColum = startColum + (numberColum -1);
			
			if ((startRow > endRow) || (startColum > endColum)) {
		        throw new IllegalArgumentException("Tabellenabmessung Falsch");            
		    }
			 InputMap im = theTable.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
			 ActionMap am = theTable.getActionMap();
			 
			 KeyStroke tab = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0);
			 am.put(im.get(tab), new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					int row = theTable.getSelectedRow();
					int col = theTable.getSelectedColumn();
					
					
					row++;

					if (row > endRow) {
						row = startRow;
						col++;
					}

					if  (col > endColum) {
						col = startColum;
					}
					
					theTable.changeSelection(row, col, false, false);
					theTable.editCellAt(row, col);
					
				}
				 
			 });
	}
}
