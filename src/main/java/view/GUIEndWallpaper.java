package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * Oberflaeche fuer die Abschlussseite des Spiels
 * 
 * @author Verena Stech
 *
 */
public class GUIEndWallpaper extends JPanel {

	private static final long serialVersionUID = 1L;
	private JLabel headlineLabel = new JLabel("Herzlichen Gl\u00FCckwunsch");
	private JTextArea contentTextArea = new JTextArea();
	private JButton continueButton = new JButton("Beenden");

	/**
	 * Konstruktor.
	 */
	public GUIEndWallpaper() {
		initialize();
	}

	/**
	 * setzt das Layout
	 * 
	 * fuegt alle Komponenten in die Oberflaeche ein
	 */
	public void initialize() {

		contentTextArea
				.setText("\nDu hast alle Passagiere aus dem  Dschungel gerettet\nund bist ein wahrer Matheprofi! \n\n"
						+ "Leider sind wir hier schon am Ende, aber eine \nFortsetzung folgt ;)\n ");
		contentTextArea.setOpaque(false);
		continueButton.setOpaque(false);
		headlineLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		continueButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		setLayout(new GridBagLayout());
		styleComponents();
		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.PAGE_AXIS));
		addButtonsToPanelComponents(panelButtons);
		panelButtons.setOpaque(false);
		add(panelButtons, new GridBagConstraints());

	}

	/**
	 * fuegt die Komponenten in das Hauptpanel ein
	 * 
	 * @param panelComponents das Hauptpanel
	 */
	public void addButtonsToPanelComponents(JPanel panelComponents) {
		panelComponents.add(getHeadlineLabel());
		panelComponents.add(getContentTextArea());
		panelComponents.add(getContinueButton());
	}

	/**
	 * stylt alle Komponenten
	 */
	public void styleComponents() {
		getHeadlineLabel().setBackground(Color.BLACK);
		getHeadlineLabel().setForeground(Color.WHITE);
		getHeadlineLabel().setFont(new Font("Colonna MT", Font.BOLD, 80));
		getContentTextArea().setEditable(false);
		getContentTextArea().setBackground(Color.BLACK);
		getContentTextArea().setForeground(Color.WHITE);
		getContentTextArea().setFont(new Font("Colonna MT", Font.BOLD, 50));
		getContinueButton().setBackground(Color.BLACK);
		getContinueButton().setForeground(Color.WHITE);
		getContinueButton().setFont(new Font("Colonna MT", Font.BOLD, 50));
	}

	/**
	 * fuegt das Dschungelbild im Hintergrund ein
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.DSCHUNGEL, 0, 0, null);
	}
	
	/**
	 * 
	 * @return contentTextArea TextArea mit Endszenario der Spielerstory
	 */
	public JTextArea getContentTextArea() {
		return contentTextArea;
	}

	/**
	 * 
	 * @return continueButton der Button zur Weiterleitung ins Hauptmenue
	 */
	public JButton getContinueButton() {
		return continueButton;
	}

	/**
	 * 
	 * @return headlineLabel die Ueberschrift der Abschlussseite
	 */
	public JLabel getHeadlineLabel() {
		return headlineLabel;
	}



}
