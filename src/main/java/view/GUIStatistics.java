package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

//TODO Verena: JavaDoc
public class GUIStatistics extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	private JLabel infoLabel = new JLabel("Spielinformationen");
	private ChartPanel panel;
	private JButton backButton = new JButton("Zur\u00FCck");

	public GUIStatistics() {
		initialize();
	}

	public void initialize() {
		JFreeChart lineChart = ChartFactory.createLineChart("", "Datum", "Punkte", createDataset(), PlotOrientation.VERTICAL, true, true, false);
		panel = new ChartPanel(lineChart);
		backButton.setOpaque(false);
		infoLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		setLayout(new GridBagLayout());
		styleComponents();
		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.PAGE_AXIS));
		addButtonsToPanelButtons(panelButtons);
		panelButtons.setOpaque(false);
		add(panelButtons, new GridBagConstraints());
	
	}

	public void addButtonsToPanelButtons(JPanel panelButtons) {
		panelButtons.add(getInfoLabel());
		panelButtons.add(panel);
		panelButtons.add(getBackButton());
	}

	public void styleComponents() {
		getInfoLabel().setBackground(Color.BLACK);
		getInfoLabel().setForeground(Color.WHITE);
		getInfoLabel().setFont(new Font("Colonna MT", Font.BOLD, 70));
		panel.setBackground(Color.BLACK);
		panel.setForeground(Color.WHITE);
		panel.setFont(new Font("Colonna MT", Font.BOLD, 30));
		getBackButton().setBackground(Color.BLACK);
		getBackButton().setForeground(Color.WHITE);
		getBackButton().setFont(new Font("Colonna MT", Font.BOLD, 30));
	}

	public DefaultCategoryDataset createDataset() {
		dataset.addValue(15, "Punkte", "1970");
		dataset.addValue(30, "Punkte", "1980");
		dataset.addValue(120, "Punkte", "2000");
		dataset.addValue(240, "Punkte", "2010");
		dataset.addValue(500, "Punkte", "2014");
		return dataset;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.DSCHUNGEL, 0, 0, null);
	}

	public JLabel getInfoLabel() {
		return infoLabel;
	}

	public JButton getBackButton() {
		return backButton;
	}


}
