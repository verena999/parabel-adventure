package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
/**
 * Oberflaeche fuer das Hilfemenue in den Levels 
 * 
 * @author Verena Stech, Marcel Lievre
 *
 */
public class GUIHelpMenuLevel extends JPanel {

	private static final long serialVersionUID = 1L;
	private JLabel headlineLabel = new JLabel("Hilfsinformationen");
	private JTextArea contentTextArea = new JTextArea(
			"\nBei diesem Spiel handelt es sich um das\nParabelabenteuerland.\nDer Sinn des Spiels liegt darin, spielerisch den Umgang mit Parabeln zu lernen.\n");
	private JButton backButton = new JButton("Zur\u00fcck");
	private JTable helpLevelTable = new JTable(6, 4);

	/**
	 * 
	 * Konstruktor.
	 */
	public GUIHelpMenuLevel() {
		initialize();
	}

	/**
	 * setzt das Layout
	 * 
	 * fuegt alle Komponenten in die Oberflaeche ein
	 */
	public void initialize() {
		headlineLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		contentTextArea.setOpaque(false);
		backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		setLayout(new GridBagLayout());

		styleComponents();
		addTextToTable();

		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.PAGE_AXIS));
		addButtonsToPanelComponents(panelButtons);
		panelButtons.setOpaque(false);
		add(panelButtons, new GridBagConstraints());

	}

	/**
	 * fuegt die Komponenten in das Panel ein
	 * @param panelComponents das Hauptpanel
	 */
	public void addButtonsToPanelComponents(JPanel panelComponents) {
		panelComponents.add(getHeadlineLabel());
		panelComponents.add(getContent());
		panelComponents.add(getHelpLevelTable());
		panelComponents.add(getBackButton());
	}

	/**
	 * stylt alle Komponenten
	 */
	public void styleComponents() {
		getHeadlineLabel().setBackground(Color.BLACK);
		getHeadlineLabel().setForeground(Color.WHITE);
		getHeadlineLabel().setFont(new Font("Colonna MT", Font.BOLD, 70));

		getContent().setEditable(false);
		getContent().setBackground(Color.BLACK);
		getContent().setForeground(Color.WHITE);
		getContent().setFont(new Font("Colonna MT", Font.BOLD, 30));

		getBackButton().setBackground(Color.BLACK);
		getBackButton().setForeground(Color.WHITE);
		getBackButton().setFont(new Font("Colonna MT", Font.BOLD, 30));
		getBackButton().setOpaque(false);

		Color transparet = new Color(0, 0, 0, 0);
		getHelpLevelTable().setBackground(transparet);
		getHelpLevelTable().setForeground(Color.WHITE);
		getHelpLevelTable().setGridColor(transparet);
		getHelpLevelTable().setFont(new Font("Colonna MT", Font.BOLD, 25));
		getHelpLevelTable().setRowHeight(80);
		getHelpLevelTable().setAutoResizeMode(0);
		getHelpLevelTable().setEnabled(false);
		getHelpLevelTable().setSize(new Dimension(900, 700));
		getHelpLevelTable().getColumnModel().getColumn(0).setPreferredWidth(50);
		getHelpLevelTable().getColumnModel().getColumn(1).setPreferredWidth(250);
		getHelpLevelTable().getColumnModel().getColumn(2).setPreferredWidth(150);
		getHelpLevelTable().getColumnModel().getColumn(3).setPreferredWidth(450);

	}

	/**
	 * fuegt den Text in die Tabelle ein
	 */
	public void addTextToTable() {
		getHelpLevelTable().setValueAt("Level", 0, 0);
		getHelpLevelTable().setValueAt("Mathematik", 0, 1);
		getHelpLevelTable().setValueAt("Vorgabe vom Spiel", 0, 2);
		getHelpLevelTable().setValueAt("Engabe vom Spieler", 0, 3);
		getHelpLevelTable().setValueAt("Level 1", 1, 0);
		getHelpLevelTable().setValueAt("Funktionswert ausrechnen", 1, 1);
		getHelpLevelTable().setValueAt("Term & x-Werte", 1, 2);
		getHelpLevelTable().setValueAt("y-Werte", 1, 3);
		getHelpLevelTable().setValueAt("Level 2", 2, 0);
		getHelpLevelTable().setValueAt("Scheitelpunkt bestimmen", 2, 1);
		getHelpLevelTable().setValueAt("Term & x-Wert", 2, 2);
		getHelpLevelTable().setValueAt("y-Wert & Markierung im Koodrinatensystem", 2, 3);
		getHelpLevelTable().setValueAt("Level 3", 3, 0);
		getHelpLevelTable().setValueAt("Nullstellen bestimmen", 3, 1);
		getHelpLevelTable().setValueAt("Term", 3, 2);
		getHelpLevelTable().setValueAt("<html>Anzahl Punkte, Punkte (x/y) & Markierung im Koordinatensystem</html>", 3, 3);
		getHelpLevelTable().setValueAt("Level 4", 4, 0);
		getHelpLevelTable().setValueAt("Schnittpunkte bestimmen", 4, 1);
		getHelpLevelTable().setValueAt("Terme", 4, 2);
		getHelpLevelTable().setValueAt("<html>Anzahl Punkte, Punkte (x/y) \n& Markierung im Koordinatensystem</html>", 4, 3);
		getHelpLevelTable().setValueAt("Level 5", 5, 0);
		getHelpLevelTable().setValueAt("Gau\u00df-Algorithmus", 5, 1);
		getHelpLevelTable().setValueAt("4 Punkte", 5, 2);
		getHelpLevelTable().setValueAt("Term mit Koeffizienten", 5, 3);
	}

	/**
	 * fuegt das Dschungelbild im Hintergrund ein
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(ImageUtils.DSCHUNGEL, 0, 0, null);
	}

	/**
	 * 
	 * @return headlineLabel die Ueberschrift der Seite
	 */
	public JLabel getHeadlineLabel() {
		return headlineLabel;
	}

	/**
	 * 
	 * @return contentTextArea TextArea mit Erklaerung des Sinns des Spiels
	 */
	public JTextArea getContent() {
		return contentTextArea;
	}

	/**
	 * 
	 * @return backButton der Zurueck-Button
	 */
	public JButton getBackButton() {
		return backButton;
	}

	/**
	 * 
	 * @return helpLevelTable die Tabelle mit den thematischen Levelinhalten
	 */
	public JTable getHelpLevelTable() {
		return helpLevelTable;
	}
}
