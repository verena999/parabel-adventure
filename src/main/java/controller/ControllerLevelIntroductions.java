package controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;

import view.GUILevelIntroductions;
import view.GUILevels;

/**
 *
 * Controller f�r die GUILevelIntroductions
 *
 * @author Verena Stech
 */
public class ControllerLevelIntroductions implements Controller {
	private ControllerMain mainController;
	private GUILevelIntroductions view;

	/**
	 * Konstruktor.
	 *
	 * @param mainController der Hauptcontroller der Anwendung
	 */
	public ControllerLevelIntroductions(ControllerMain mainController) {
		this.mainController = mainController;
		initialize();
	}

	/**
	 * GUILevelIntroductions wird initialisiert
	 * 
	 * setzt in der View das Level aus der LevelEvaluation-Klasse
	 * 
	 * ActionListener wird dem ContinueButton hinzugef�gt
	 */
	public void initialize() {
		view = new GUILevelIntroductions();
		view.setLevel(mainController.getLevelEvaluations().getLevel());
		view.getContinueButton().addActionListener(this::doStart);
		view.getContinueButton().addKeyListener(keyListener());
	}

	private KeyAdapter keyListener() {
		return new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					view.getContinueButton().doClick();
				}
			}
		};
	}

	/**
	 * setzt in der View das Level aus der LevelEvaluation-Klasse
	 * 
	 * startet mit der init-Methode der View das entsprechende Level
	 * 
	 * Canvas wird aus der View entfernt
	 * 
	 * je nach Level die Funktion der Berechnungen neu erstellt
	 * 
	 * Canvas wird neu erstellt und wieder der View hinzugef�gt
	 * 
	 * ruft beim mainController die Methode zu Starten der GUILevels auf
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doStart(ActionEvent e) {
		view.setLevel(mainController.getLevelEvaluations().getLevel());
		((GUILevels) mainController.getControllerLevels().getView())
				.initLevel(mainController.getLevelEvaluations().getLevel());
		mainController.getControllerLevels().excludeCanvas();
		if (mainController.getLevelEvaluations().getLevel() == 1) {
			mainController.getControllerLevels().activateLevel1Function();
		} else if (mainController.getLevelEvaluations().getLevel() == 2) {
			mainController.getControllerLevels().activateLevel2Function();
		} else if (mainController.getLevelEvaluations().getLevel() == 3) {
			mainController.getControllerLevels().activateLevel3Function();
		} else if (mainController.getLevelEvaluations().getLevel() == 4) {
			mainController.getControllerLevels().activateLevel4Function();
		} else if (mainController.getLevelEvaluations().getLevel() == 5) {
			mainController.getControllerLevels().activateLevel5Function();

		}
		mainController.getControllerLevels().includeCanvas();
		mainController.startLevels();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {
		return view;
	}

}
