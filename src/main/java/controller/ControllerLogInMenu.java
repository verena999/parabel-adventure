package controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.KeyStroke;

import model.JAXBParser;
import model.User;
import model.UserJAXB;
import view.GUILogInMenu;

/**
 * Controller der GUILogInMenu
 *
 * @author Verena Stech, Fabian Urban
 */
public class ControllerLogInMenu implements Controller {
	private ControllerMain mainController;
	private GUILogInMenu view;
	private User user;

	/**
	 * Konstruktor.
	 *
	 * @param mainController Hauptcontroller der Anwendung
	 * @param user           Der angemeldete Benutzer
	 */
	public ControllerLogInMenu(ControllerMain mainController, User user) {
		this.mainController = mainController;
		this.user = user;
		initialize();
	}

	/**
	 * GUILogInMenu wird initialisiert
	 * 
	 * ActionListener werden den Buttons hinzugefuegt
	 */
	public void initialize() {
		view = new GUILogInMenu();
		view.getRegistrateButton().addActionListener(this::doRegistrate);
		view.getStartButton().addActionListener(this::doReady);
		view.getNameInputTextfield().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					view.getStartButton().doClick();
				}
			}

		});
	};

	/**
	 * Liest den Benutzernamen aus dem Textfeld ein und gibt ihn weiter an eine
	 * Methode, die diesen Namen in der XML-Datei sucht und anschlie�end ein Objekt
	 * des Users zur�ckgibt. Ruft anschlie�end beim MainController die Methode zum
	 * Starten der GUIMainMenu auf
	 * 
	 * Wird der Name nicht gefunden ersteint eine Info zum Erstellen eines neuen
	 * Benutzers.
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	public void doReady(ActionEvent e) {
		JAXBParser jaxb = new JAXBParser();
		UserJAXB userjaxb = jaxb.getUser(view.getNameInputTextfield().getText());
		if (userjaxb != null) {
			user.setUsername(userjaxb.getUsername());
			mainController.startMainMenu();
		}
		view.getNameInputTextfield().setText("");
	}

	/**
	 * ruft beim MainController die Methode zum Starten der GUIRegistrateNewPlayer
	 * auf
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	public void doRegistrate(ActionEvent e) {
		mainController.startRegistrateNewPlayer();
		view.getNameInputTextfield().setText("");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {
		return view;
	}

	public void enterControler(KeyEvent e) {

		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			view.getStartButton().doClick();
		}
	}
}
