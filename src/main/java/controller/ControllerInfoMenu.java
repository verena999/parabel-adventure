package controller;

import java.awt.event.ActionEvent;

import javax.swing.JPanel;

import view.GUIInfoMenu;

/**
 * Controller der GUIInfoMenu
 *
 * @author Verena Stech
 */
public class ControllerInfoMenu implements Controller {

	private ControllerMain mainController;
	private GUIInfoMenu view;

	/**
	 * Konstruktor.
	 *
	 * @param mainController der Hauptcontroller der Anwendung
	 */
	public ControllerInfoMenu(ControllerMain mainController) {
		this.mainController = mainController;
		initialize();
	}

	/**
	 * GUIInfoMenu wird initialisiert und
	 * 
	 * dem BackButton wird ein ActionListener hinzugefuegt
	 */
	public void initialize() {
		view = new GUIInfoMenu();
		view.getBackButton().addActionListener(this::doBack);
	}

	/**
	 * ruft beim mainController die Methode zum Starten der GUIGameMenu auf
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doBack(ActionEvent e) {
		mainController.startBackToMainMenu();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {
		return view;
	}

}
