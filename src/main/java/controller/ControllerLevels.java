package controller;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.CheckResults;
import model.Function;
import model.Game;
import model.GameJAXB;
import model.JAXBParser;
import model.PlayerRating;
import model.User;
import view.GUILevels;

/**
 *
 * Controller der GUILevels
 *
 * @author Verena Stech, Henning Bergmann, Kevin Burchert, Fabian Urban
 */
public class ControllerLevels implements Controller {
	private static final int ORDER = 2;
	private static final double CANVAS_TOLERANCE = 0.15;
	private ControllerMain mainController;
	private GUILevels view;
	private FunctionCanvas canvas;
	private ArrayList<Function> functions = new ArrayList<Function>();
	private PlayerRating rating;
	private Function function = new Function(ORDER, false);
	private Function function2 = new Function(ORDER, false);
	private User user;
	private int spielzuege = 0;

	/**
	 * Konstruktor.
	 *
	 * @param mainController der Hauptcontroller der Anwendung
	 * @param user           Der angemeldete Benutzer
	 */
	public ControllerLevels(ControllerMain mainController, User user) {
		this.user = user;
		this.mainController = mainController;
		switch (mainController.getLevelEvaluations().getLevel()) {
		case 1:
			activateLevel1Function();
			break;

		case 2:
			activateLevel2Function();
			break;

		case 3:
			activateLevel3Function();
			break;

		case 4:
			activateLevel4Function();
			break;

		case 5:
			activateLevel5Function();
			break;
		}
		initialize();
	}

	/**
	 * GUILevels wird initialisiert
	 * 
	 * Canvas wird eingebunden
	 * 
	 * CheckButton wird ActionListener hinzugefuegt
	 * 
	 * Term wird berechnet
	 * 
	 * Methode zum Starten der Level wird aufgerufen
	 */
	public void initialize() {
		rating = new PlayerRating();
		view = new GUILevels();
		view.initLevel(mainController.getLevelEvaluations().getLevel());
		includeCanvas();
		view.getCheckButton().addActionListener(this::doCheck);
		view.getReadyButton().addActionListener(this::doReady);
		view.getMainMenuButton().addActionListener(this::doMainMenu);
		view.getHelpButton().addActionListener(this::doHelp);
		view.getStatisticsButton().addActionListener(this::doStatis);
		view.getChangeQuantityButton().addActionListener(this::doChangeQuantity);
		view.getEquation().setText(function.createFunctionString());
		view.getSecondTermLabel().setText(function2.createFunctionString());
		view.getGoOnButton().addActionListener(this::doGoOn);
		view.getGoAheadButton().addActionListener(this::doGoAhead);
		view.setScoreOutputLabel(rating.getPointsSummary());
		view.setTurnOutputLabel(spielzuege);
	}

	/**
	 * ruft beim mainController die Methode zum Starten der GUIMainMenu auf
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doMainMenu(ActionEvent e) {
		JAXBParser jaxb = new JAXBParser();
		GameJAXB gamejaxb = jaxb.getCurrentGame(user.getUsername());
		jaxb.saveGame(user.getUsername(), fillGameJaxbWithModelValues(gamejaxb));
		mainController.resetAndRestart();
		mainController.startBackToMainMenu();
	}

	/**
	 * ruft beim mainController die Methode zum Starten der GUIHelpMenu
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doHelp(ActionEvent e) {
		mainController.startHelpMenuLevel();
	}

	/**
	 * ruft beim mainController die Methode zum Starten der GUILevelStatistics auf
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doStatis(ActionEvent e) {
		mainController.getControllerStatistics().getDatafromXML();
		mainController.startLevelStatictics();
	}

	/**
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doChangeQuantity(ActionEvent e) {
		view.changeQuantityLevelThreeFour();
	}

	/**
	 * Ermittlung, ob das eingegebene Ergebnis korrekt ist
	 * 
	 * wenn ja werden die Level ausgewertet und die Tabellen der jeweiligen Level
	 * geleert
	 * 
	 * bei erfolgreichem Abschliessen von Level 5 wird im mainController die Methode
	 * zum Starten der GUIEndWallpaper aufgerufen;
	 * 
	 * wenn das Ergebnis falsch war, wird die Zahl der richtigen Ergebnisse in
	 * Reihenfolge zurueckgesetzt
	 *
	 * @param e das ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doCheck(ActionEvent e) {
		spielzuege++;
		view.setTurnOutputLabel(spielzuege);
		JAXBParser jaxb = new JAXBParser();
		GameJAXB gamejaxb = jaxb.getCurrentGame(user.getUsername());
		boolean success = false;
		CheckResults check = new CheckResults(mainController.getLevelEvaluations().getLevel(),CANVAS_TOLERANCE);
		check.setArrayXValues(view.getXValues());
		check.setArrayYValues(view.getYValues());
		check.setPoints(canvas.getPoints());
		if (view.getLevel() != 3 && view.getLevel() != 5) {
			if (view.getYValues().length != 200 && view.getXValues().length != 200) {
				evaluate(jaxb, gamejaxb, check);
			} else {
				JOptionPane.showMessageDialog(null, "Entweder eine falsche Eingabe oder nichts eingegeben.");
			}
		}
		if (view.getLevel() == 3) {
			if (view.getXValues().length != 200) {
				evaluate(jaxb, gamejaxb, check);
			} else {
				JOptionPane.showMessageDialog(null, "Entweder eine falsche Eingabe oder nichts eingegeben.");
				this.getCanvas().clearPoints();
			}
		}
		if (view.getLevel() == 5) {
			evaluate(jaxb, gamejaxb, check);
		}
	}

	private void evaluate(JAXBParser jaxb, GameJAXB gamejaxb, CheckResults check) {
		boolean success;
		check.setCoeff1(function.getCoefficients());
		if (mainController.getLevelEvaluations().getLevel() == 4) {
			check.setCoeff2(function2.getCoefficients());
		}
		if (mainController.getLevelEvaluations().getLevel() == 5) {
			check.setCoeff2(view.getCoefficients());
		}
		check.setType(view.getType());
		success = check.evaluate();
		boolean[] result = check.getResult();
		// Reduziert die Wertung, wenn falsche Werte eingetragen wurden
		for (boolean entry : result) {
			if (!entry) {
				rating.decreasePoints();
			}
		}

		view.displayResults(success, result);

		user.getGame().getCurrentLevel().setTotalMoves(user.getGame().getCurrentLevel().getTotalMoves() + 1);
		if (success) {
			successTrue(jaxb, gamejaxb);
		} else {
			mainController.getLevelEvaluations().setLevelFourInRow(0);
			mainController.getLevelEvaluations().setLevelThreeInRow(0);
			mainController.getLevelEvaluations().setLevelTwoInRow(0);
			this.getCanvas().clearPoints();
		}
	}

	public void successTrue(JAXBParser jaxb, GameJAXB gamejaxb) {
		if (mainController.getLevelEvaluations().getLevel() == 1) {
			user.getGame().getCurrentLevel().setRightMoves((user.getGame().getCurrentLevel().getRightMoves() + 1));
			mainController.getLevelEvaluations().evaluateLevelOne();
			view.deleteLevelOneTableContent();
		} else if (mainController.getLevelEvaluations().getLevel() == 2) {
			user.getGame().getCurrentLevel().setRightMoves((user.getGame().getCurrentLevel().getRightMoves() + 1));
			user.getGame().getCurrentLevel()
					.setRightMovesInRow(mainController.getLevelEvaluations().getLevelTwoInRow());
			mainController.getLevelEvaluations().evaluateLevelTwo();
			view.deleteLevelTwoTableContent();
		} else if (mainController.getLevelEvaluations().getLevel() == 3) {
			user.getGame().getCurrentLevel()
					.setRightMovesInRow(mainController.getLevelEvaluations().getLevelThreeInRow());
			mainController.getLevelEvaluations().evaluateLevelThree();
			view.deleteLevelThreeTableContent();
		} else if (mainController.getLevelEvaluations().getLevel() == 4) {
			user.getGame().getCurrentLevel()
					.setRightMovesInRow(mainController.getLevelEvaluations().getLevelFourInRow());
			mainController.getLevelEvaluations().evaluateLevelFour();
			view.deleteLevelFourTableContent();
		} else if (mainController.getLevelEvaluations().getLevel() == 5) {
			user.getGame().getCurrentLevel().setRightMoves((user.getGame().getCurrentLevel().getRightMoves() + 1));
			mainController.getLevelEvaluations().evaluateLevelFive();
			view.deleteLevelFiveKoefficientContent();
		} else if (mainController.getLevelEvaluations().getLevel() == 6) {
			mainController.getLevelEvaluations().zuruecksetzen();
			mainController.resetAndRestart();
			mainController.startEndWallpaper();
			gamejaxb.setCurrentLevel(null);
			jaxb.saveGame(user.getUsername(), gamejaxb);
		}
		if (mainController.getLevelEvaluations().getStartNewLevel()
				&& mainController.getLevelEvaluations().getLevel() != 6) {
			if (mainController.getLevelEvaluations().getLevel() - 1 == 1) {
				user.getGame().setScoreLevelOne(rating.getPoints());
			} else if (mainController.getLevelEvaluations().getLevel() - 1 == 2) {
				user.getGame().setScoreLevelTwo(rating.getPoints());
			} else if (mainController.getLevelEvaluations().getLevel() - 1 == 3) {
				user.getGame().setScoreLevelThree(rating.getPoints());
			} else if (mainController.getLevelEvaluations().getLevel() - 1 == 4) {
				user.getGame().setScoreLevelFour(rating.getPoints());
			} else if (mainController.getLevelEvaluations().getLevel() - 1 == 5) {
				user.getGame().setScoreLevelFive(rating.getPoints());
			}

			if (gamejaxb != null) {
				fillGameJaxbWithModelValues(gamejaxb);
				jaxb.saveGame(user.getUsername(), gamejaxb);
				resetCurrentGame();
			}

			int currentLevel = user.getGame().getCurrentLevel().getId();
			Game game = mainController.getUser().getGame();
			if (currentLevel++ == 1) {
				game.setScoreLevelOne(rating.getPoints());
			}
			gamejaxb.setScoreLevelOne(rating.getPoints());
			rating.setPointsSummary(rating.getPoints());
			view.setScoreOutputLabel(rating.getPointsSummary());
			view.goToNextLevel();
			this.getCanvas().setFunctionVisible();

		} else {
			view.goToNextCheck();
			this.getCanvas().setFunctionVisible();

		}
	}

	/**
	 * Aktiviert die naechste Funktion fuer einen Level
	 */
	private void nextFunction(int level) {
		switch (level) {
		case 1:
			excludeCanvas();
			activateLevel1Function();
			includeCanvas();
			break;
		case 2:
			excludeCanvas();
			activateLevel2Function();
			includeCanvas();
			break;
		case 3:
			excludeCanvas();
			activateLevel3Function();
			includeCanvas();
			break;
		case 4:
			excludeCanvas();
			activateLevel4Function();
			includeCanvas();
			break;
		case 5:
			excludeCanvas();
			activateLevel5Function();
			includeCanvas();
			break;
		default:
			return;
		}
	}

	/**
	 * Zaehlt die ID um 1 hoch und setzt alle anderen Werte auf 0, immer wenn ein
	 * Level abgeschlossen ist.
	 */
	private void resetCurrentGame() {
		user.getGame().getCurrentLevel().setId(user.getGame().getCurrentLevel().getId() + 1);
		user.getGame().getCurrentLevel().setTotalMoves(0);
		user.getGame().getCurrentLevel().setRightMoves(0);
		user.getGame().getCurrentLevel().setRightMovesInRow(0);
		user.getGame().getCurrentLevel().setParameterOfFunction(0);
	}

	/**
	 * Gibt die Werte aus dem Game in das GameJAXB weiter.
	 * 
	 * @param gamejaxb Das GameJAXB dem die Werte hinzugefuegt werden sollen.
	 * @return gamejaxb Das GameJAXB mit allen Werten.
	 */
	private GameJAXB fillGameJaxbWithModelValues(GameJAXB gamejaxb) {
		gamejaxb.setScoreLevelOne(user.getGame().getScoreLevelOne());
		gamejaxb.setScoreLevelTwo(user.getGame().getScoreLevelTwo());
		gamejaxb.setScoreLevelThree(user.getGame().getScoreLevelThree());
		gamejaxb.setScoreLevelFour(user.getGame().getScoreLevelFour());
		gamejaxb.setScoreLevelFive(user.getGame().getScoreLevelFive());
		gamejaxb.setTimestamp(user.getGame().getTimestamp());
		gamejaxb.getCurrentLevel().setID(user.getGame().getCurrentLevel().getId());
		gamejaxb.getCurrentLevel().setTotalMoves(user.getGame().getCurrentLevel().getTotalMoves());
		gamejaxb.getCurrentLevel().setRightMoves(user.getGame().getCurrentLevel().getRightMoves());
		gamejaxb.getCurrentLevel().setRightMovesInRow(user.getGame().getCurrentLevel().getRightMovesInRow());
		gamejaxb.getCurrentLevel().setParameterOfFunction(user.getGame().getCurrentLevel().getParameterOfFunction());
		return gamejaxb;
	}

	/**
	 * je nach Level und Anzahl der Eingabefelder wird die richtige Tabelle in die
	 * Oberflaeche gesetzt
	 * 
	 * ruft beim mainController die Methode zum Starten der GUILevelIntroductions
	 * auf
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doGoOn(ActionEvent e) {
		getCanvas().clearPoints();
		mainController.startLevelIntroductions();
	}

	public void doGoAhead(ActionEvent e) {

		nextFunction(mainController.getLevelEvaluations().getLevel());
		this.getCanvas().clearPoints();
		mainController.resetAndRestart();
		mainController.startLevels();
	}

	/**
	 * je nach Level und Anzahl der Eingabefelder wird die richtige Tabelle in die
	 * Oberflaeche gesetzt
	 *
	 * @param e das ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doReady(ActionEvent e) {
		if (mainController.getLevelEvaluations().getLevel() == 3) {
			view.setQuantity(Integer.parseInt(view.getQuantityXValueTextField().getText()));
			view.showTableLevelThree();
		} else if (mainController.getLevelEvaluations().getLevel() == 4) {
			view.setQuantity(Integer.parseInt(view.getQuantityXValueTextField().getText()));
			view.showTableLevelFour();
		}
	}

	/**
	 * Methode zum Einbinden von Canvas
	 */
	public void includeCanvas() {

		canvas = new FunctionCanvas((int) view.getPreferredSize().getWidth(), (int) view.getPreferredSize().getHeight(),
				functions, -10, 10, -10, 30);
		view.add(canvas, BorderLayout.CENTER);
		view.getEquation().setText(function.createFunctionString());
		view.getSecondTermLabel().setText(function2.createFunctionString());
	}

	/**
	 * loescht das Canvas-Objekt
	 */
	public void excludeCanvas() {
		view.remove(canvas);
	}

	public void activateLevel1Function() {
		function = new Function(ORDER, false);
		functions.removeAll(functions);
		functions.add(function);
	}

	public void activateLevel2Function() {
		function = new Function(ORDER, false);
		functions.removeAll(functions);
		functions.add(function);
	}

	/**
	 * berechnet die Funktion fuer Level 3
	 */
	public void activateLevel3Function() {
		function = new Function(ORDER, true);
		functions.removeAll(functions);
		functions.add(function);
	}

	/**
	 * berechnet die Funktionen fuer Level 4
	 */
	public void activateLevel4Function() {
		function = new Function(ORDER, false);
		function2 = new Function(ORDER, false);
		functions.removeAll(functions);
		functions.add(function);
		functions.add(function2);
	}

	/**
	 * berechnet die Funktion fuer Level 5
	 */
	public void activateLevel5Function() {
		function = new Function(ORDER, false);
		functions.removeAll(functions);
		functions.add(function);
	}

	/**
	 *
	 * @return _canvas das aktuelle Canvas-Objekt
	 */
	public FunctionCanvas getCanvas() {
		return canvas;
	}

	public ArrayList<Function> getFunctions() {
		return functions;
	}

	/**
	 *
	 * @return function die aktuelle Funktion zum Rechnen und Zeichnen des Canvas
	 */
	public Function getFunction() {
		return function;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {

		setTurn();
		return view;
	}

	public PlayerRating getPlayerRating() {
		return rating;
	}

	public void setPoints() {
		view.setScoreOutputLabel(rating.getPointsSummary());
	}

	public void setTurn() {
		view.setTurnOutputLabel(user.getGame().getCurrentLevel().getTotalMoves());
	}

}
