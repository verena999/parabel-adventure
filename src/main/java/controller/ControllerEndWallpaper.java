package controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;

import view.GUIEndWallpaper;

/**
 * Controller der GUIEndWallpaper
 *
 * @author Verena Stech
 */
public class ControllerEndWallpaper implements Controller {
	private ControllerMain mainController;
	private GUIEndWallpaper view;

	/**
	 * Konstruktor.
	 *
	 * @param mainController der Hauptcontroller der Anwendung
	 */
	public ControllerEndWallpaper(ControllerMain mainController) {
		this.mainController = mainController;
		initialize();
	}

	/**
	 * GUIEndWallpaper wird initialisiert
	 * 
	 * ActionListener wird dem ContinueButton hinzugefügt
	 */
	public void initialize() {
		view = new GUIEndWallpaper();
		view.getContinueButton().addActionListener(this::doEnd);
		view.getContinueButton().requestFocus();
		view.getContinueButton().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					
					view.getContinueButton().doClick();
				}
			}
		});

	}

	/**
	 * ruft beim mainController die Methode zum Starten der GUIMainMenu auf
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doEnd(ActionEvent e) {
		mainController.startBackToMainMenu();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {
		return view;
	}
}
