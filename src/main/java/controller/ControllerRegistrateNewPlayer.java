package controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.JAXBParser;
import model.User;
import model.UserJAXB;
import view.GUIRegistrateNewPlayer;

/**
 * Controller der GUINewPlayer
 *
 * @author Verena Stech, Fabian Urban
 */
public class ControllerRegistrateNewPlayer implements Controller {
	private ControllerMain mainController;
	private GUIRegistrateNewPlayer view;
	private User user;

	/**
	 *
	 * Konstruktor.
	 *
	 * @param mainController der Hauptcontroller der Anwendung
	 */
	public ControllerRegistrateNewPlayer(ControllerMain mainController, User user) {
		this.mainController = mainController;
		this.user = user;
		initialize();
	}

	/**
	 * GUINewPlayer wird initialisiert
	 * 
	 * den Buttons werden ActionListener hinzugefuegt
	 */
	public void initialize() {
		view = new GUIRegistrateNewPlayer();
		view.getStartButton().addActionListener(this::doReady);
		view.getBackButton().addActionListener(this::doBack);
		view.getUsernameTextfield().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					view.getStartButton().doClick();
				}
			}

		});
	};
	

	/**
	 * Liest den Benutzernamen aus dem Textfeld ein und gibt ihn weiter an eine Methode, 
	 * die pr�ft ob dieser Name in der XML-Datei steht. 
	 * Wenn dieser nicht existiert, wird ein neuer Benutzer angelegt und gespeichert.
	 * Ruft anschlie�end beim MainController die Methode zum Starten der GUIMainMenu auf.
	 * 
	 * Wenn dieser bereits existiert, wird eine Info zum Laden des Benutzers angezeigt.
	 * 
	 * Pr�ft au�erdem ob ein Name eingegeben wurde.
	 *
	 * @param e das ActionEvent beim Klick auf den Button
	 */
	public void doReady(ActionEvent e) {
		if (!view.getUsernameTextfield().getText().isEmpty()) {
			JAXBParser jaxb = new JAXBParser();
			UserJAXB userjaxb = jaxb.fillListWithUsersAndMarshall(view.getUsernameTextfield().getText());
			if (userjaxb != null) {
				user.setUsername(userjaxb.getUsername());
				mainController.startMainMenu();
				view.getUsernameTextfield().setText("");
			}
			
		} else {
			JOptionPane.showMessageDialog(null, "Bitte einen Benutzernamen eingeben.");
		}
	}

	/**
	 * ruft beim mainController die Methode zum Starten der GUILogInMenu auf
	 *
	 * @param e das ActionEvent beim Klick auf den Button
	 */
	public void doBack(ActionEvent e) {
		mainController.startLogInMenu();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {
		return view;
	}
}
