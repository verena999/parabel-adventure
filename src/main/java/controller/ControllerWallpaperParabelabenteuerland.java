package controller;

import javax.swing.JPanel;

import view.GUIWallpaperParabelabenteuerland;

/**
 * Controller der GUIStartApplication
 *
 * @author Verena Stech
 */
public class ControllerWallpaperParabelabenteuerland implements Controller {
	@SuppressWarnings("unused")
	private ControllerMain mainController;
	private GUIWallpaperParabelabenteuerland view;

	/**
	 *
	 * Konstruktor.
	 *
	 * @param mainController der Hauptcontroller der Anwendung
	 */
	public ControllerWallpaperParabelabenteuerland(ControllerMain mainController) {
		this.mainController = mainController;
		initialize();
	}

	/**
	 * GUIStartApplication wird initialisiert
	 */
	public void initialize() {
		view = new GUIWallpaperParabelabenteuerland();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {
		return view;
	}

}
