package controller;

import javax.swing.JPanel;

import view.GUIWallpaperWelcomeToTheJungle;

/**
 * Controller der GUIWallpaperWelcomeToTheJungle
 * 
 * @author Verena Stech
 */
public class ControllerWallpaperWelcomeToTheJungle implements Controller {
	@SuppressWarnings("unused")
	private ControllerMain mainController;
	private GUIWallpaperWelcomeToTheJungle view;
	@SuppressWarnings("unused")
	private String wallpaper;

	/**
	 * Konstruktor.
	 *
	 * @param mainController der Hauptcontroller der Anwendung
	 */
	public ControllerWallpaperWelcomeToTheJungle(ControllerMain mainController) {
		this.mainController = mainController;
		initialize();
	}

	/**
	 * GUIStartApplication wird initialisiert
	 */
	public void initialize() {
		view = new GUIWallpaperWelcomeToTheJungle();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {
		return view;
	}
}
