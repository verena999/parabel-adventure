package controller;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import model.Function;
import model.Functions;
import view.ImageUtils;

//TODO Kevin: Bitte JavaDoc machen und kannst du bitte allen �berfl�ssigen Code l�schen? Der wird von Sonar angemotzt
public class FunctionCanvas extends Canvas implements MouseListener, MouseMotionListener {

	private static final long serialVersionUID = 1L;
	private static final int CANVAS_WIDTH = 20;
	private static final int CANVAS_HEIGHT = 20;
	private ArrayList<Function> _functions;
	private ArrayList<Point> _points = new ArrayList<>();
	private int _minX, _minY, _maxX, _maxY, _stepX, _stepY;
	private Point _zeroPoint;
	private Color[] _colors;
	private int _canvasBorderLeft, _canvasBorderRight, _canvasBorderTop, _canvasBorderBottom;
	private boolean _drawFunctions;

	public int getCanvasBorderLeft() {
		return _canvasBorderLeft;
	}

	public int getCanvasBroderRight() {
		return _canvasBorderRight;
	}

	public int getCanvasBorderTop() {
		return _canvasBorderTop;
	}

	public int getCanvasBorderBottom() {
		return _canvasBorderBottom;
	}

	private void setCanvasBorders(int canvasBorder) {

		if (canvasBorder < 20) {
			canvasBorder = 20;
		}

		_canvasBorderLeft = 0 + canvasBorder;
		_canvasBorderRight = this.getWidth() - canvasBorder;
		_canvasBorderTop = 0 + canvasBorder;
		_canvasBorderBottom = this.getHeight() - canvasBorder;
	}

	public void setFunctionVisible(){
		_drawFunctions = true;
		invalidate();
		repaint();
	}

	public void setFunctionInvisible(){
		_drawFunctions = false;
		repaint();
	}

	public Function getFunc(int i)
	{
		if(!(i > _functions.size()) || !(i < 0)){
			return _functions.get(i);
		}
		else{
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	public ArrayList<Function> getFunctions() {

		return _functions;
	}

	public int getMinX() {
		return _minX;
	}

	public int getMaxX() {
		return _maxX;
	}

	public int getMinY() {
		return _minY;
	}

	public int getMaxY() {
		return _minY;
	}

	public int getStepX() {
		return _stepX;
	}

	public int getStepY() {
		return _stepY;
	}

	public Point getZeroPoint() {
		return _zeroPoint;
	}

	public ArrayList<Point2D.Double> getPoints() {
		ArrayList<Point2D.Double> result = new ArrayList<Point2D.Double>();
		for(int i = 0;i<_points.size();i++) {
			result.add(getFunctionValuesFromPoint(_points.get(i)));
		}
		return result;
	}

	// constructor

	public FunctionCanvas(int width, int height, ArrayList<Function> functions, int minX, int maxX, int minY, int maxY) {
		_functions = functions;
		_drawFunctions = false;
		setMinMax();

		setBackground(Color.WHITE);
		setSize(width, height);
		setMarkColors();
		addMouseListener(this);
		addMouseMotionListener(this);
	}

	private void setMinMax() {
		_functions.get(0).setMinMax(CANVAS_WIDTH, CANVAS_HEIGHT);
		_minX = _functions.get(0).getCanvasMinX();
		_minY = _functions.get(0).getCanvasMinY();
		_maxX = _functions.get(0).getCanvasMaxX();
		_maxY = _functions.get(0).getCanvasMaxY();
	}

	// calculates steps, not possible in constructor
	private void calculateSteps() {

		_stepX = (_canvasBorderRight - _canvasBorderLeft) / (_maxX - _minX);
		_stepY = (_canvasBorderBottom - _canvasBorderTop) / (_maxY - _minY);
	}

	// sets ZeroPoint
	private void setZeroPoint() {
		_zeroPoint = this.getZeroForCoordinateSystem(_minX, _maxX, _minY, _maxY);
	}

	// creates array with colors for marks at canvas
	private void setMarkColors() {
		// defines colors for marks at canvas
		Color[] colors = new Color[7];
		colors[0] = Color.RED;
		colors[1] = Color.BLUE;
		colors[2] = Color.GREEN;
		colors[3] = Color.ORANGE;
		colors[4] = Color.CYAN;
		colors[5] = Color.MAGENTA;
		colors[6] = Color.YELLOW;

		_colors = colors;
	}

	// ToDo: Ignore unnecessary repaint; call setZeroPoints & calculateSteps from somewhere else??;
	@Override
	public void paint(Graphics g) {


		// set CanvasBorders
		setCanvasBorders(50);

		// calculate steps
		calculateSteps();

		// get ZeroPoint
		setZeroPoint();

		g.drawImage(ImageUtils.KARTE, 0,0,null);

		g.setColor(Color.BLACK);

		this.drawCoordinateSystem(_minX, _maxX, _minY, _maxY);

		if(_drawFunctions){
			for(int i = 0; i < _functions.size(); i++){
				Functions.draw(this, _functions.get(i), _minX, _maxX, _minY, _maxY, 1000);
			}
		}

		if (_points != null) {
			Graphics g2 = this.getGraphics();
			int count = 0;
			for (Point p : _points) {
				if (count < _colors.length) {
					g2.setColor(_colors[count]);
				} else {
					g2.setColor(Color.BLACK);
				}
				g2.drawLine(p.x, p.y, p.x - 5, p.y - 5);
				g2.drawLine(p.x, p.y, p.x + 5, p.y + 5);
				g2.drawLine(p.x, p.y, p.x + 5, p.y - 5);
				g2.drawLine(p.x, p.y, p.x - 5, p.y + 5);
				count++;
			}

		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			_points.add(e.getPoint());
		}
		else if(e.getButton() == MouseEvent.BUTTON2){
			_drawFunctions = !_drawFunctions;
			repaint();
		}
		else if (e.getButton() == MouseEvent.BUTTON3) {
			_points.removeAll(_points);
		}
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	// returns a Point2D with mapped function coordinates from a point - object
	public Point2D.Double getFunctionValuesFromPoint(Point point) {
		Point2D.Double result = new Point2D.Double((point.getX() - _zeroPoint.getX()) / _stepX, (point.getY() - _zeroPoint.getY()) / _stepY * -1);

		return result;
	}

	// Returns Point - object with values for (0/0) - position in coordinate-system
	public Point getZeroForCoordinateSystem(int minX, int maxX, int minY, int maxY) {
		Point zeroPoint = new Point();

		int positionX = _canvasBorderLeft;
		int positionY = _canvasBorderBottom;
		if(minX <= 0 && maxX >= 0) {
			for (int i = minX; i < maxX; i++) {

				if (i == 0) { break; }

				positionX += _stepX;
			}
		}

		else if(minX > 0) {
			for(int i = minX; i > 0; i--) {
				positionX -= _stepX;
			}
		}

		else if(maxX < 0) {
			positionX = _canvasBorderRight;
			for(int i = maxX; i < 0; i++) {
				positionX += _stepX;
			}
		}

		else {
			throw new IllegalArgumentException("Lower border must be below upper border!");
		}

		if(minY <= 0 && maxY >= 0) {
			for (int i = minY; i < maxY; i++) {

				 if (i == 0) { break; }

				positionY -= _stepY;
			}
		}

		else if(minY > 0) {
			for(int i = minY; i > 0; i--) {
				positionY += _stepY;
			}
		}

		else if(maxY < 0) {
			positionY = _canvasBorderTop;
			for(int i = maxY; i < 0; i++) {
				positionY -= _stepY;
			}
		}

		else {
			throw new IllegalArgumentException("Lower border must be below upper border!");
		}

		zeroPoint.setLocation(positionX, positionY);

		return zeroPoint;
	}

	public void clearPoints(){
	    _points.removeAll(_points);
	    repaint();
    }

	// draws a coordinate - system at the canvas
	private void drawCoordinateSystem(int minX, int maxX, int minY, int maxY) {

		Graphics g = this.getGraphics();
		Graphics g2 = g.create();

		g.setColor(Color.BLACK);
		g2.setColor(Color.GRAY);

		// draws x-axis
		g.drawLine(_canvasBorderLeft, (int) _zeroPoint.getY(), _canvasBorderRight, (int) _zeroPoint.getY());

		// draws y-axis
		g.drawLine((int) _zeroPoint.getX(), _canvasBorderTop, (int) _zeroPoint.getX(), _canvasBorderBottom);

		// draws marks and values on x - axis, g2 draws grid
		int positionX = 0;
		int valueX = 0;

		while (positionX <= _canvasBorderRight) {
			if ((int) _zeroPoint.getX() - positionX >= _canvasBorderLeft) {
				if (positionX > 0) {
					g2.drawLine((int) _zeroPoint.getX() - positionX, _canvasBorderTop, (int) _zeroPoint.getX() - positionX, _canvasBorderBottom);
				}
				g.drawLine((int) _zeroPoint.getX() - positionX, (int) _zeroPoint.getY() + 2, (int) _zeroPoint.getX() - positionX, (int) _zeroPoint.getY() - 2);
				// g.drawString(Integer.toString(valueX * -1), (int) _zeroPoint.getX() - positionX, (int) _zeroPoint.getY() - 5);
				g.drawString(Integer.toString(valueX * -1), (int) _zeroPoint.getX() - positionX, _canvasBorderBottom + 20);
			}
			if ((int) _zeroPoint.getX() + positionX <= _canvasBorderRight) {
				if (positionX > 0) {
					g2.drawLine((int) _zeroPoint.getX() + positionX, _canvasBorderTop, (int) _zeroPoint.getX() + positionX, _canvasBorderBottom);
				}
				g.drawLine((int) _zeroPoint.getX() + positionX, (int) _zeroPoint.getY() + 2, (int) _zeroPoint.getX() + positionX, (int) _zeroPoint.getY() - 2);
				// g.drawString(Integer.toString(valueX), (int) _zeroPoint.getX() + positionX, (int) _zeroPoint.getY() - 5);
				g.drawString(Integer.toString(valueX), (int) _zeroPoint.getX() + positionX, _canvasBorderBottom + 20);
			}
			positionX += _stepX;
			valueX++;
		}

		// draws marks and values on y-axis
		int positionY = 0;
		int valueY = 0;

		while (positionY <= _canvasBorderBottom) {
			if ((int) _zeroPoint.getY() - positionY >= _canvasBorderTop) {
				if (positionY > 0) {
					g2.drawLine(_canvasBorderLeft, (int) _zeroPoint.getY() - positionY, _canvasBorderRight, (int) _zeroPoint.getY() - positionY);
				}
				g.drawLine((int) _zeroPoint.getX() + 2, (int) _zeroPoint.getY() - positionY, (int) _zeroPoint.getX() - 2, (int) _zeroPoint.getY() - positionY);
				// g.drawString(Integer.toString(valueY), (int) _zeroPoint.getX() + 10, (int) _zeroPoint.getY() - positionY);
				g.drawString(Integer.toString(valueY), _canvasBorderLeft - 20, (int) _zeroPoint.getY() - positionY);

			}
			if ((int) _zeroPoint.getY() + positionY <= _canvasBorderBottom) {
				if (positionY > 0) {
					g2.drawLine(_canvasBorderLeft, (int) _zeroPoint.getY() + positionY, _canvasBorderRight, (int) _zeroPoint.getY() + positionY);
				}
				g.drawLine((int) _zeroPoint.getX() + 2, (int) _zeroPoint.getY() + positionY, (int) _zeroPoint.getX() - 2, (int) _zeroPoint.getY() + positionY);
				// g.drawString(Integer.toString(valueY * -1), (int) _zeroPoint.getX() + 10, (int) _zeroPoint.getY() + positionY);
				g.drawString(Integer.toString(valueY * -1), _canvasBorderLeft - 20, (int) _zeroPoint.getY() + positionY);
			}
			positionY += _stepY;
			valueY++;
		}

		//draws Borders
		g.drawLine(_canvasBorderLeft, _canvasBorderTop, _canvasBorderLeft, _canvasBorderBottom);
		g.drawLine(_canvasBorderRight, _canvasBorderTop, _canvasBorderRight, _canvasBorderBottom);
		g.drawLine(_canvasBorderLeft, _canvasBorderTop, _canvasBorderRight, _canvasBorderTop);
		g.drawLine(_canvasBorderLeft, _canvasBorderBottom, _canvasBorderRight, _canvasBorderBottom);
	}

	private static final class ImageLoader{

		static BufferedImage loadImage(String fileName)
		{
			BufferedImage bi = null;
			//System.err.println("....setimg...." + fileName);

			try {
				bi = ImageIO.read(new File(fileName));

			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Image could not be read");
				System.exit(1);
			}

			return bi;
		}

		static BufferedImage resizeImage(BufferedImage image, int scaleWidth, int scaleHeight, boolean preserveAlpha)
		{
			int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;

			BufferedImage scaledImage = new BufferedImage(scaleWidth, scaleHeight, imageType);
			Graphics2D g = scaledImage.createGraphics();

			if (preserveAlpha) {
				g.setComposite(AlphaComposite.Src);
			}

			g.drawImage(image, 0, 0, scaleWidth, scaleHeight, null);
			g.dispose();

			return scaledImage;
		}
	}
}
