package controller;

import java.awt.event.ActionEvent;

import javax.swing.JPanel;

import model.CurrentLevel;
import model.Game;
import model.GameJAXB;
import model.JAXBParser;
import model.User;
import model.UserJAXB;
import view.GUIMainMenu;

/**
 * Controller der GUIGameMenu
 *
 * @author Verena Stech, Fabian Urban
 */
public class ControllerMainMenu implements Controller {
	private ControllerMain mainController;
	private GUIMainMenu view;
	private User user;

	/**
	 * Konstruktor.
	 *
	 * @param mainController
	 *            der Hauptcontroller der Anwendung
	 * @param user
	 *            Der angemeldete Benutzer
	 */
	public ControllerMainMenu(ControllerMain mainController, User user) {
		this.mainController = mainController;
		this.user = user;
		initialize();
	}

	/**
	 * GUIGameMenu wird initialisiert
	 * 
	 * den Buttons werden ActionListener hinzugefuegt
	 */
	public void initialize() {
		view = new GUIMainMenu();
		view.getButtonContinue().addActionListener(this::doContinueGame);
		view.getButtonNewGame().addActionListener(this::doNewGame);
		view.getButtonHelp().addActionListener(this::doHelp);
		view.getButtonInfo().addActionListener(this::doInfo);
		view.getButtonLogOut().addActionListener(this::doLogOut);
		view.getButtonStatis().addActionListener(this::doStatis);
	}

	/**
	 * Liest mit dem Benutzernamen die Benutzerinformationen zum aktuellen
	 * Spielstand aus der XML-Datei aus. Dieser Spielstand wird in ein Objekt
	 * �bertragen und anschlie�end dem Model als aktueller Spielstand gesetzt.
	 * 
	 * ruft beim mainController die Methode zum Starten der GUILevelIntroductions
	 * auf
	 *
	 * @param e
	 *            ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doContinueGame(ActionEvent e) {
		JAXBParser jaxb = new JAXBParser();
		GameJAXB gamejaxb = jaxb.getCurrentGame(user.getUsername());
		if (gamejaxb != null) {
			CurrentLevel currentLevel = new CurrentLevel(gamejaxb.getCurrentLevel().getID(), gamejaxb.getCurrentLevel().getTotalMoves(), gamejaxb.getCurrentLevel().getRightMoves(),
					gamejaxb.getCurrentLevel().getRightMovesInRow(), gamejaxb.getCurrentLevel().getParameterOfFunction());
			Game game = new Game(gamejaxb.getId(), gamejaxb.getScoreLevelOne(), gamejaxb.getScoreLevelTwo(), gamejaxb.getScoreLevelThree(), gamejaxb.getScoreLevelFour(), gamejaxb.getScoreLevelFive(),
					gamejaxb.getTimestamp(), currentLevel);
			user.setGame(game);
			mainController.startLevelIntroductions();
		}
	}

	/**
	 * Holt sich zu einem Benutzernamen den Benutzer aus der XML-Datei und erstellt zu diesem ein neues Spiel.
	 * 
	 * Setzt das Level zurueck auf 1
	 * 
	 * ruft beim mainController die Methode zum Starten der GUILevelIntroductions
	 * auf
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doNewGame(ActionEvent e) {
		JAXBParser jaxb = new JAXBParser();
		GameJAXB gamejaxb = jaxb.createNewGame(jaxb.getUser(user.getUsername()));
		CurrentLevel currentLevel = new CurrentLevel(gamejaxb.getCurrentLevel().getID(), gamejaxb.getCurrentLevel().getTotalMoves(),
				gamejaxb.getCurrentLevel().getRightMoves(), gamejaxb.getCurrentLevel().getRightMovesInRow(), gamejaxb.getCurrentLevel().getParameterOfFunction());
		Game game = new Game(gamejaxb.getId(), gamejaxb.getScoreLevelOne(), gamejaxb.getScoreLevelTwo(), gamejaxb.getScoreLevelThree(),
				gamejaxb.getScoreLevelFour(), gamejaxb.getScoreLevelFive(), gamejaxb.getTimestamp(), currentLevel);
		user.setGame(game);
		mainController.getLevelEvaluations().setLevel(1);
		mainController.getLevelEvaluations().zuruecksetzen();
		mainController.startLevelIntroductions();
	}

	/**
	 * ruft beim mainController die Methode zum Starten der GUILevelStatistics auf
	 *
	 * @param e
	 *            ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doStatis(ActionEvent e) {
		mainController.getControllerSummaryStatistics().getDatafromXML();
		mainController.startSummaryStatistics();
	}

	/**
	 * ruft beim mainController die Methode zum Starten der GUIHelpMenu auf
	 *
	 * @param e
	 *            ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doHelp(ActionEvent e) {
		mainController.startHelpMenu();
	}

	/**
	 * ruft beim mainController die Methode zum Starten der GUIInfoMenu auf
	 *
	 * @param e
	 *            ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doInfo(ActionEvent e) {
		mainController.startInfoMenu();
	}

	/**
	 * setzt den User zurueck
	 * 
	 * setzt das Spiel zurueck
	 * 
	 * ruft beim mainController die Methode zum Starten der GUILogInMenu auf
	 *
	 * @param e
	 */
	@SuppressWarnings("unused")
	public void doLogOut(ActionEvent e) {
		user.setUsername("");
		user.setGame(null);
		mainController.startLogInMenu();
	}

	/**
	 * Pr�ft ob der aktuelle Spieler bereits einen gespeicherten Spielstand hat oder
	 * ob das aktuelle Level vorhanden ist und deaktiviert den Button zum Laden des
	 * Spiels, wenn dies nicht zutrifft. Wenn es einen Spielstand oder ein aktuelles
	 * Level gibt wird der Button aktiviert.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {
		if (user.getUsername() != null) {
			JAXBParser jaxb = new JAXBParser();
			UserJAXB userjaxb = jaxb.getUser(user.getUsername());
			if (userjaxb.getGames().isEmpty() || userjaxb.getGames().get(userjaxb.getGames().size() - 1).getCurrentLevel() == null) {
				view.getButtonContinue().setEnabled(false);
			} else {
				view.getButtonContinue().setEnabled(true);
			}
		}
		return view;
	}

}
