package controller;

import java.awt.event.ActionEvent;

import javax.swing.JPanel;

import view.GUIHelpMenu;

/**
 * Controller der GUIHelpMenu
 *
 * @author Verena Stech
 */
public class ControllerHelpMenu implements Controller {

	private ControllerMain mainController;
	private GUIHelpMenu view;

	/**
	 * Konstruktor.
	 *
	 * @param mainController der Hauptcontroller der Anwendung
	 */
	public ControllerHelpMenu(ControllerMain mainController) {
		this.mainController = mainController;
		initialize();
	}

	/**
	 * GUIHelpMenu wird initialisiert
	 * 
	 * dem BackButton wird ein ActionListener hinzugefuegt
	 */
	public void initialize() {
		view = new GUIHelpMenu();
		view.getBackButton().addActionListener(this::doBack);
	}

	/**
	 * ruft beim mainController die Methode zum Starten der GUIGameMenu auf
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doBack(ActionEvent e) {
		mainController.startBackToMainMenu();
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {
		return view;
	}

}
