package controller;

import javax.swing.SwingUtilities;

import lombok.NonNull;
import model.Functions;
import model.GameJAXB;
import model.JAXBParser;
import model.LevelEvaluations;
import model.User;
import view.GUILevelIntroductions;
import view.GUILevels;
import view.GUIMainSettings;

/**
 * HauptController der Anwendung mit Methoden zum Starten der Controller der
 * einzelnen Oberflaechenelemente
 *
 * @author Verena Stech, Fabian Urban
 */
public class ControllerMain {
	private User user;
	private GUIMainSettings view;
	private ControllerLogInMenu controllerLogInMenu;
	private ControllerRegistrateNewPlayer controllerRegistrateNewPlayer;
	private ControllerHelpMenu controllerHelpMenu;
	private ControllerInfoMenu controllerInfoMenu;
	private ControllerMainMenu controllerMainMenu;
	private ControllerWallpaperParabelabenteuerland controllerWallpaperParabelabenteuerland;
	private ControllerWallpaperWelcomeToTheJungle controllerWallpaperWelcomeToTheJungle;
	private ControllerLevelStatistics controllerLevelStatistics;
	private ControllerLevelIntroductions controllerLevelIntroductions;
	private ControllerLevels controllerLevels;
	private ControllerEndWallpaper controllerEndWallpaper;
	private ControllerSummaryStatistics controllerSummaryStatistics;
	private ControllerHelpMenuLevel controllerHelpMenuLevel;
	private LevelEvaluations levelEvaluations = new LevelEvaluations();

	/**
	 * Main-Methode, die die gesamte Anwendung startet
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		ControllerMain controller = new ControllerMain();
		controller.start();
	}

	/**
	 * Konstruktor.
	 *
	 * ruft die initialize-Methode auf
	 */
	public ControllerMain() {
		initialize();
	}

	/**
	 * die GUIMainSettings, der User und alle Controller werden initialisiert
	 */
	public void initialize() {
		view = new GUIMainSettings();
		user = new User();
		controllerLogInMenu = new ControllerLogInMenu(this, user);
		controllerRegistrateNewPlayer = new ControllerRegistrateNewPlayer(this, user);
		controllerMainMenu = new ControllerMainMenu(this, user);
		controllerInfoMenu = new ControllerInfoMenu(this);
		controllerHelpMenu = new ControllerHelpMenu(this);
		controllerLevelStatistics = new ControllerLevelStatistics(this, user);
		controllerWallpaperParabelabenteuerland = new ControllerWallpaperParabelabenteuerland(this);
		controllerWallpaperWelcomeToTheJungle = new ControllerWallpaperWelcomeToTheJungle(this);
		controllerLevelIntroductions = new ControllerLevelIntroductions(this);
		controllerLevels = new ControllerLevels(this, user);
		controllerEndWallpaper = new ControllerEndWallpaper(this);
		controllerSummaryStatistics = new ControllerSummaryStatistics(this, user);
		controllerHelpMenuLevel = new ControllerHelpMenuLevel(this);
	}

	/**
	 * wird bei jedem Controlleraufruf gestartet; ueberprueft, ob Controller null
	 * ist und aktualisiert die Oberflaeche
	 *
	 * @param controller
	 */
	public void start(@NonNull Controller controller) {
		view.setContentPane(controller.getView());
		view.revalidate();
		view.repaint();
	}

	/**
	 * startet den Controller der GUIWallpaperParabelabenteuerland
	 * 
	 * nach einer Verzoegerung von 10 Sekunden wird der Controller der GUILoginMenu
	 * gestartet
	 */
	public void start() {
		startWallpaperParabelabenteuerland();
		SwingUtilities.invokeLater(() -> {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.getMessage();
			}
			startLogInMenu();
		});
		view.setVisible(true);
	}

	/**
	 * startet den Controller der GUIRegistrateNewPlayer
	 */
	public void startRegistrateNewPlayer() {
		start(controllerRegistrateNewPlayer);
	}

	/**
	 * startet den Controller der GUIWallpaperParabelabenteuerland
	 */
	public void startWallpaperParabelabenteuerland() {
		start(controllerWallpaperParabelabenteuerland);
	}

	/**
	 * startet den Controller der GUIWallpaperWelcomeToTheJungle
	 */
	public void startWallpaperWelcomeToTheJungle() {
		start(controllerWallpaperWelcomeToTheJungle);
	}

	/**
	 * startet den Controller der GUIHelpMenu
	 */
	public void startHelpMenu() {
		start(controllerHelpMenu);
	}
	
	/**
	 * startet den Controller der GUIHelpMenuLevel
	 */
	public void startHelpMenuLevel() {
		start(controllerHelpMenuLevel);
	}

	/**
	 * startet den Controller der GUIInfoMenu
	 */
	public void startInfoMenu() {
		start(controllerInfoMenu);
	}

	/**
	 * startet den Controller der GUILevelStatistics
	 */
	public void startLevelStatictics() {
		start(controllerLevelStatistics);
	}

	/**
	 * startet den Controller der GUISummaryStatistics
	 */
	public void startSummaryStatistics() {
		start(controllerSummaryStatistics);
	}

	/**
	 * startet den Controller der GUIEndWallpaper
	 */
	public void startEndWallpaper() {
		start(controllerEndWallpaper);
	}

	/**
	 * startet den Controller der GUILogInMenu
	 */
	public void startLogInMenu() {
		start(controllerLogInMenu);
	}

	/**
	 * startet den Controller der GUIMainMenu
	 */
	public void startBackToMainMenu() {
		start(controllerMainMenu);
	}

	/**
	 * startet den Controller der GUIWallpaperWelcomeToTheJungle,
	 * 
	 * nach einer Verzoegerung von 1,5 Sekunden wird der Controller der GUIMainMenu
	 * gestartet
	 */
	public void startMainMenu() {
		startWallpaperWelcomeToTheJungle();
		SwingUtilities.invokeLater(() -> {
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				e.getMessage();
			}
			start(controllerMainMenu);
		});
	}

	/**
	 * setzt in der GUILevelIntroductions das Level
	 * 
	 * startet den Controller der GUILevelIntroductions
	 */
	public void startLevelIntroductions() {
		levelEvaluations.setLevel(user.getGame().getCurrentLevel().getId());
		GUILevelIntroductions gui = (GUILevelIntroductions) controllerLevelIntroductions.getView();
		gui.setLevel(levelEvaluations.getLevel());
		start(controllerLevelIntroductions);
		gui.getContinueButton().requestFocus();
	}

	/**
	 * in der GUILevels wird das Level gesetzt
	 * 
	 * der Controller der GUILevels wird gestartet
	 * 
	 * wenn das Level 5 ist, werden hier die Werte berechnet
	 */
	public void startLevels() {
		((GUILevels) getControllerLevels().getView()).setLevel(levelEvaluations.getLevel());
		if (levelEvaluations.getLevel() == 5) {
			((GUILevels) getControllerLevels().getView()).setQuantity(3);
			int[] xValues = { 3, 5, 7, 9 };
			int[] yValues = new int[xValues.length];
			for (int i = 0; i < yValues.length; i++) {
				yValues[i] = (int) Functions.calculate(controllerLevels.getFunction(), xValues[i]);
			}
			for (int i = 0; i < 3; i++) {
				((GUILevels) getControllerLevels().getView()).getTableLevelFive().setValueAt(xValues[i], 0, i + 1);
				((GUILevels) getControllerLevels().getView()).getTableLevelFive().setValueAt(yValues[i], 1, i + 1);
			}
		}
		JAXBParser jaxb  = new JAXBParser();
		GameJAXB game = jaxb.getCurrentGame(user.getUsername());
//		if(this.getLevelEvaluations().getLevel() == 2) {
//		game.getScoreLevelTwo();
//		}else if(this.getLevelEvaluations().getLevel() == 3) {
//			controllerLevels.setPoints(game.getScoreLevelThree());
//		}else if(this.getLevelEvaluations().getLevel() == 4) {
//			game.getScoreLevelFour();
//		}else if(this.getLevelEvaluations().getLevel() == 5) {
//			game.getScoreLevelFive();
//		}
		controllerLevels.setPoints();
		controllerLevels.setTurn();
		start(controllerLevels);
	}

	/**
	 * controllerLevels wird auf null gesetzt und neu initialisiert
	 */
	public void resetAndRestart() {
		controllerLevels = null;
		controllerLevels = new ControllerLevels(this, user);
	}

	/**
	 * @return controllerLevels der Controller der GUILevels
	 */
	public ControllerLevels getControllerLevels() {
		return controllerLevels;
	}

	/**
	 * @return levelEvaluations die Klasse zur Auswertung der Level
	 */
	public LevelEvaluations getLevelEvaluations() {
		return levelEvaluations;
	}

	public ControllerLevelStatistics getControllerStatistics() {
		return controllerLevelStatistics;
	}

	public ControllerSummaryStatistics getControllerSummaryStatistics() {
		return controllerSummaryStatistics;
	}
	


	public User getUser() {
		return user;
	}

}
