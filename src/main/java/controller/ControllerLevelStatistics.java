package controller;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JPanel;

import org.jfree.data.category.DefaultCategoryDataset;

import model.GameJAXB;
import model.JAXBParser;
import model.User;
import view.GUILevelStatistics;

/**
 * Controller f�r die GUILevelStatistics
 *
 * @author Verena Stech, Fabian Urban
 */
public class ControllerLevelStatistics implements Controller {

	private DefaultCategoryDataset dataset;
	private ControllerMain controllerMain;
	private GUILevelStatistics view;
	private User user;
	private List<GameJAXB> xmlData;

	/**
	 * Konstruktor.
	 *
	 * @param controllerMain der Hauptcontroller der Anwendung
	 * @param user Der Benutzer der aktuell angemeldet ist
	 */
	public ControllerLevelStatistics(ControllerMain controllerMain, User user) {
		this.controllerMain = controllerMain;
		this.user = user;
		initialize();
	}

	public void getDatafromXML() {
		initialize();
		JAXBParser jaxb = new JAXBParser();
		xmlData = jaxb.getUsername(user.getUsername()).getGames();
		setDataset(new DefaultCategoryDataset());
	}

	public void setDataset(DefaultCategoryDataset dataset) {
		int currentLevel = user.getGame().getCurrentLevel().getId();
		int count = 0;
		dataset.addValue(0, "Punkte", "");
		count++;
		for (GameJAXB xmlEntry : xmlData) {
			if (currentLevel == 1 && xmlEntry.getScoreLevelOne() > 0) {
				dataset.addValue(xmlEntry.getScoreLevelOne(), "Punkte", "" + count++);
			} else if (currentLevel == 2 && xmlEntry.getScoreLevelTwo() > 0) {
				dataset.addValue(xmlEntry.getScoreLevelTwo(), "Punkte", "" + count++);
			} else if (currentLevel == 3 && xmlEntry.getScoreLevelThree() > 0) {
				dataset.addValue(xmlEntry.getScoreLevelThree(), "Punkte", "" + count++);
			} else if (currentLevel == 4 && xmlEntry.getScoreLevelFour() > 0) {
				dataset.addValue(xmlEntry.getScoreLevelFour(), "Punkte", "" + count++);
			} else if (currentLevel == 5 && xmlEntry.getScoreLevelFive() > 0) {
				dataset.addValue(xmlEntry.getScoreLevelFive(), "Punkte", "" + count++);
			}
		}
		view.initialize(dataset);
	
	}

	

	/**
	 * GUILevelStatistics wird initialisiert
	 * 
	 * dem backButton wird ein ActionListener hinzugefuegt
	 */
	public void initialize() {
		view = new GUILevelStatistics(dataset);
		view.getBackButton().addActionListener(this::doBack);
	}

	/**
	 * ruft beim mainController die Methode zum Zurueckgehen in die GUIMainMenu auf
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doBack(ActionEvent e) {
		controllerMain.getControllerLevels().setPoints();
		controllerMain.startLevels();
		view = null;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {
		return view;
	}

}