package controller;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.JPanel;
import org.jfree.data.category.DefaultCategoryDataset;
import javax.swing.JPanel;
import model.GameJAXB;
import model.JAXBParser;
import model.User;
import view.GUISummaryStatistics;

/**
 * Controller der GUISummaryStatistics
 *
 * @author Verena Stech, Fabian Urban
 */
public class ControllerSummaryStatistics implements Controller {

	private DefaultCategoryDataset dataset;
	private ControllerMain controllerMain;
	private GUISummaryStatistics view;
	private User user;
	private List<GameJAXB> xmlData;

	/**
	 *
	 * Konstruktor.
	 *
	 * @param controllerMain der Hauptcontroller der Anwendung
	 * @param user Der angemeldete Benutzer
	 */
	public ControllerSummaryStatistics(ControllerMain controllerMain, User user) {
		this.controllerMain = controllerMain;
		this.user = user;
		initialize();
	}


	public void getDatafromXML() {
		initialize();
		JAXBParser jaxb = new JAXBParser();
		xmlData = jaxb.getUsername(user.getUsername()).getGames();
		setDataset(new DefaultCategoryDataset());
	}


	public void setDataset(DefaultCategoryDataset dataset) {
		//int currentLevel = user.getGame().getCurrentLevel().getId();
		int count = 0;
		dataset.addValue(0, "Punkte", "");
		count++;
		for (GameJAXB xmlEntry : xmlData) {
			if (xmlEntry.getScoreLevelFive() > 0) {
				int scoreSum =
					xmlEntry.getScoreLevelOne()
					+ xmlEntry.getScoreLevelTwo()
					+ xmlEntry.getScoreLevelThree()
					+ xmlEntry.getScoreLevelFour()
					+ xmlEntry.getScoreLevelFive();
				dataset.addValue(scoreSum, "Punkte", "" + count++);
			}
		}
		view.initialize(dataset);

	}



	/**
	 * GUISummaryStatistics wird initialisiert
	 * 
	 * BackButton wird ActionListener hinzugefuegt
	 */
	public void initialize() {
		view = new GUISummaryStatistics(dataset);
		view.getBackButton().addActionListener(this::doBack);
	}

	/**
	 * ruft beim mainController die Methode zum Zur�ckgehen in die GUIMainMenu auf
	 *
	 * @param e ein ActionEvent beim Klick auf den Button
	 */
	@SuppressWarnings("unused")
	public void doBack(ActionEvent e) {
		controllerMain.startBackToMainMenu();
		view = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JPanel getView() {
		return view;
	}
}
