package controller;

import javax.swing.JPanel;

/**
 * @author Verena Stech
 *
 *         Controller Interface mit Methode zum Holen des aktuellen Panels
 */
public interface Controller {

	/**
	 * holt die aktuelle View
	 * 
	 * @return
	 */
	JPanel getView();
}
