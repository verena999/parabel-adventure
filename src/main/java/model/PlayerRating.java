package model;

public class PlayerRating {

    private static final int  INITIAL_POINTS = 50;
    private static final int POINT_LOSS = 5;

    private int points = INITIAL_POINTS;
    private int pointsSummary = 0;

    public int getPoints() {
        return points;
    }

    public void setPoints(int startingPoints) {
        this.points = startingPoints;
    }

    public int getPointsSummary() {
        return pointsSummary;
    }

    public void setPointsSummary(int points) {
        this.pointsSummary += points;
    }

    public void decreasePoints() {
        this.points -= POINT_LOSS;
        if (this.points <= 0) {
            setPoints(1);
        }
    }

}
