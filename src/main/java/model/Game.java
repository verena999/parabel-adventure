package model;

/**
 * Modellklasse des Spielstandes
 * 
 * @author Fabian Urban
 */
public class Game {

	private int id;
	private int scoreLevelOne;
	private int scoreLevelTwo;
	private int scoreLevelThree;
	private int scoreLevelFour;
	private int scoreLevelFive;
	private String timestamp;
	private CurrentLevel currentLevel;

	/**
	 * Konstruktor des Games
	 * 
	 * @param id Die ID des Spielstandes
	 * @param scoreLevelOne Der Punktestand des 1. Levels
	 * @param scoreLevelTwo Der Punktestand des 2. Levels
	 * @param scoreLevelThree Der Punktestand des 3. Levels
	 * @param scoreLevelFour Der Punktestand des 4. Levels
	 * @param scoreLevelFive Der Punktestand des 5. Levels
	 * @param timestamp Der Zeitpunkt an dem das Spiel gestartet wurde
	 * @param currentLevel Das aktuelle Level
	 */
	public Game(int id, int scoreLevelOne, int scoreLevelTwo, int scoreLevelThree, int scoreLevelFour, int scoreLevelFive, String timestamp,
			CurrentLevel currentLevel) {
		this.id = id;
		this.scoreLevelOne = scoreLevelOne;
		this.scoreLevelTwo = scoreLevelTwo;
		this.scoreLevelThree = scoreLevelThree;
		this.scoreLevelFour = scoreLevelFour;
		this.scoreLevelFive = scoreLevelFive;
		this.timestamp = timestamp;
		this.currentLevel = currentLevel;
	}

	/**
	 * Getter des Felds id
	 *
	 * @return id Die ID des Spielstandes
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter des Felds id 
	 *
	 * @param id Die ID die gesetzt wird
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter des Felds scoreLevelOne
	 *
	 * @return scoreLevelOne Der Punktestand des 1. Levels
	 */
	public int getScoreLevelOne() {
		return scoreLevelOne;
	}

	/**
	 * Setter des Felds scoreLevelOne 
	 *
	 * @param scoreLevelOne Der Punktestand des 1. Levels der gesetzt wird
	 */
	public void setScoreLevelOne(int scoreLevelOne) {
		this.scoreLevelOne = scoreLevelOne;
	}

	/**
	 * Getter des Felds scoreLevelTwo
	 *
	 * @return scoreLevelTwo Der Punktestand des 2. Levels
	 */
	public int getScoreLevelTwo() {
		return scoreLevelTwo;
	}

	/**
	 * Setter des Felds scoreLevelTwo 
	 *
	 * @param scoreLevelTwo Der Punktestand des 2. Levels der gesetzt wird
	 */
	public void setScoreLevelTwo(int scoreLevelTwo) {
		this.scoreLevelTwo = scoreLevelTwo;
	}

	/**
	 * Getter des Felds scoreLevelThree
	 *
	 * @return scoreLevelThree Der Punktestand des 3. Levels
	 */
	public int getScoreLevelThree() {
		return scoreLevelThree;
	}

	/**
	 * Setter des Felds scoreLevelThree 
	 *
	 * @param scoreLevelThree Der Punktestand des 3. Levels der gesetzt wird
	 */
	public void setScoreLevelThree(int scoreLevelThree) {
		this.scoreLevelThree = scoreLevelThree;
	}

	/**
	 * Getter des Felds scoreLevelFour
	 *
	 * @return scoreLevelFour Der Punktestand des 4. Levels
	 */
	public int getScoreLevelFour() {
		return scoreLevelFour;
	}

	/**
	 * Setter des Felds scoreLevelFour 
	 *
	 * @param scoreLevelFour Der Punktestand des 4. Levels der gesetzt wird
	 */
	public void setScoreLevelFour(int scoreLevelFour) {
		this.scoreLevelFour = scoreLevelFour;
	}

	/**
	 * Getter des Felds Der Punktestand des 5. Levels
	 *
	 * @return scoreLevelFive
	 */
	public int getScoreLevelFive() {
		return scoreLevelFive;
	}

	/**
	 * Setter des Felds scoreLevelFive 
	 *
	 * @param scoreLevelFive Der Punktestand des 5. Levels der gesetzt wird
	 */
	public void setScoreLevelFive(int scoreLevelFive) {
		this.scoreLevelFive = scoreLevelFive;
	}

	/**
	 * Getter des Felds timestamp
	 *
	 * @return timestamp Der Zeitpunkt an dem das Spiel gestartet wurde
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * Setter des Felds timestamp 
	 *
	 * @param timestamp Der timestamp der gesetzt wird
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Getter des Felds currentLevel
	 *
	 * @return currentLevel Das aktuelle Level
	 */
	public CurrentLevel getCurrentLevel() {
		return currentLevel;
	}

	/**
	 * Setter des Felds currentLevel 
	 *
	 * @param currentLevel Das aktuelle Level das gesetzt wird
	 */
	public void setCurrentLevel(CurrentLevel currentLevel) {
		this.currentLevel = currentLevel;
	}

	/**
	 * toString-Methode des Games
	 * 
	 * @return String der Felder des Games
	 */
	@Override
	public String toString() {
		return "Game [id=" + id + ", scoreLevelOne=" + scoreLevelOne + ", scoreLevelTwo=" + scoreLevelTwo + ", scoreLevelThree=" + scoreLevelThree
				+ ", scoreLevelFour=" + scoreLevelFour
				+ ", scoreLevelFive=" + scoreLevelFive + ", timestamp=" + timestamp + ", currentLevel=" + currentLevel + "]";
	}

}
