package model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//TODO Fabian: Bitte ganze Klasse mit JavaDoc kommentieren und auch hier Sinn der Klasse in den obersten Kommentar
/**
 *
 * @author Fabian Urban
 *
 */
@XmlRootElement(name = "user")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class UserJAXB {

	@XmlAttribute
	private int id;

	@XmlAttribute
	private String username;

	@XmlElement
	private List<GameJAXB> game = new ArrayList<>();

	public UserJAXB() {
	}

	public UserJAXB(int id, String username) {
		this.id = id;
		this.username = username;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<GameJAXB> getGames() {
		return game;
	}

	public void setGames(List<GameJAXB> game) {
		this.game = game;
	}

	@Override
	public String toString() {
		return "UserJAXB [id=" + id + ", username=" + username + ", game=" + game + "]";
	}

}
