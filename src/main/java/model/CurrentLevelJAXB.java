package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

//TODO Fabian: Bitte ganze Klasse mit JavaDoc kommentieren
/**
 *
 * @author Fabian Urban
 *
 */
@XmlRootElement(name = "currentPlayer")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class CurrentLevelJAXB {

	@XmlAttribute
	private int id;

	@XmlAttribute
	private int totalMoves;

	@XmlAttribute
	private int rightMoves;

	@XmlAttribute
	private int rightMovesInRow;

	@XmlAttribute
	private double parameterOfFunction;

	public CurrentLevelJAXB() {

	}

	public CurrentLevelJAXB(int id, int totalMoves, int rightMoves,
			int rightMovesInRow, double parameterOfFunction) {
		this.id = id;
		this.totalMoves = totalMoves;
		this.rightMoves = rightMoves;
		this.rightMovesInRow = rightMovesInRow;
		this.parameterOfFunction = parameterOfFunction;
	}

	public int getTotalMoves() {
		return totalMoves;
	}

	public void setTotalMoves(int totalMoves) {
		this.totalMoves = totalMoves;
	}

	public int getRightMoves() {
		return rightMoves;
	}

	public void setRightMoves(int rightMoves) {
		this.rightMoves = rightMoves;
	}

	public int getRightMovesInRow() {
		return rightMovesInRow;
	}

	public void setRightMovesInRow(int rightMovesInRow) {
		this.rightMovesInRow = rightMovesInRow;
	}

	public double getParameterOfFunction() {
		return parameterOfFunction;
	}

	public void setParameterOfFunction(double parameterOfFunction) {
		this.parameterOfFunction = parameterOfFunction;
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "CurrentLevelJAXB [id=" + id + ", totalMoves=" + totalMoves + ", rightMoves=" + rightMoves + ", rightMovesInRow=" + rightMovesInRow
				+ ", parameterOfFunction=" + parameterOfFunction
				+ "]";
	}

}
