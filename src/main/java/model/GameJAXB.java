package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//TODO Fabian: Bitte ganze Klasse mit JavaDoc kommentieren und auch hier Sinn der Klasse mit in obersten Kommentar
/**
 *
 * @author Fabian Urban
 *
 */
@XmlRootElement(name = "game")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class GameJAXB {

	@XmlAttribute
	private int id;

	@XmlAttribute
	private int scoreLevelOne;

	@XmlAttribute
	private int scoreLevelTwo;

	@XmlAttribute
	private int scoreLevelThree;

	@XmlAttribute
	private int scoreLevelFour;

	@XmlAttribute
	private int scoreLevelFive;

	@XmlAttribute
	private String timestamp;

	@XmlElement
	private CurrentLevelJAXB currentLevel;

	public GameJAXB() {
	}

	public GameJAXB(int id) {
		setId(id);
	}

	public GameJAXB(int id, int scoreLevelOne, int scoreLevelTwo, int scoreLevelThree, int scoreLevelFour, int scoreLevelFive, String timestamp) {
		this.scoreLevelOne = scoreLevelOne;
		this.scoreLevelTwo = scoreLevelTwo;
		this.scoreLevelThree = scoreLevelThree;
		this.scoreLevelFour = scoreLevelFour;
		this.scoreLevelFive = scoreLevelFive;
		setId(id);
		setTimestamp(timestamp);
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");
			sdf.parse(timestamp);
			this.timestamp = timestamp;
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CurrentLevelJAXB getCurrentLevel() {
		return currentLevel;
	}

	public void setCurrentLevel(CurrentLevelJAXB currentLevel) {
		this.currentLevel = currentLevel;
	}

	public int getScoreLevelOne() {
		return scoreLevelOne;
	}

	public void setScoreLevelOne(int scoreLevelOne) {
		this.scoreLevelOne = scoreLevelOne;
	}

	public int getScoreLevelTwo() {
		return scoreLevelTwo;
	}

	public void setScoreLevelTwo(int scoreLevelTwo) {
		this.scoreLevelTwo = scoreLevelTwo;
	}

	public int getScoreLevelThree() {
		return scoreLevelThree;
	}

	public void setScoreLevelThree(int scoreLevelThree) {
		this.scoreLevelThree = scoreLevelThree;
	}

	public int getScoreLevelFour() {
		return scoreLevelFour;
	}

	public void setScoreLevelFour(int scoreLevelFour) {
		this.scoreLevelFour = scoreLevelFour;
	}

	public int getScoreLevelFive() {
		return scoreLevelFive;
	}

	public void setScoreLevelFive(int scoreLevelFive) {
		this.scoreLevelFive = scoreLevelFive;
	}

	@Override
	public String toString() {
		return "GameJAXB [id=" + id + ", scoreLevelOne=" + scoreLevelOne + ", scoreLevelTwo=" + scoreLevelTwo + ", scoreLevelThree=" + scoreLevelThree
				+ ", scoreLevelFour=" + scoreLevelFour
				+ ", scoreLevelFive=" + scoreLevelFive + ", timestamp=" + timestamp + ", currentLevel=" + currentLevel + "]";
	}

}
