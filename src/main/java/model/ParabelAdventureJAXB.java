package model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//TODO Fabian: Bitte ganze Klasse mit JavaDoc kommentieren und auch hier Sinn der Klasse in den obersten Kommentar
/**
 *
 * @author Fabian Urban
 *
 */
@XmlRootElement(name = "parabelAdventure")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class ParabelAdventureJAXB {

	@XmlElement
	private List<UserJAXB> user = new ArrayList<>();

	public List<UserJAXB> getUsers() {
		return user;
	}

	public void setUsers(List<UserJAXB> user) {
		this.user = user;
	}

}
