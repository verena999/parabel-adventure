package model;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

/**
 * Klasse die Methoden zum Speichern und Laden der Benutzerdaten in XML-Dateien
 * enth�lt.
 *
 * @author Fabian Urban
 *
 */
// TODO Fabian: Refactoring!
public class JAXBParser {

	private ParabelAdventureJAXB parabelAdventure = new ParabelAdventureJAXB();
	private List<UserJAXB> users = new ArrayList<>();
	private Calendar timestamp = Calendar.getInstance();
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");
	private String fileUrl = "playerDataOutput.xml";

	/**
	 * Konstruktor.
	 */
	public JAXBParser() {
		// Default-Konstruktor
	}

	/**
	 * Ruft die Methode unmarshall() auf, holt die bisherigen User und legt sie in
	 * einer Liste ab. Pr�ft ob der User bereits existiert, sonst wird ein neuer
	 * erstellt und zur Liste hinzugef�gt. Anschlie�end werden die Daten wieder in
	 * einer XML-Datei gespeichert.
	 *
	 * @param username Benutzername der von dem Benutzer eingegeben wurde
	 * @return currentUser Der aktuelle Benutzer
	 */
	public UserJAXB fillListWithUsersAndMarshall(String username) {
		ParabelAdventureJAXB paOv = unmarshall();
		users.clear();
		List<UserJAXB> jaxbUsers = paOv.getUsers().stream().distinct().collect(Collectors.toList());
		users.addAll(jaxbUsers);
		UserJAXB currentUser = checkIfUserExistsNewPlayer(paOv, username);
		marshall(currentUser);
		return currentUser;
	}

	/**
	 * Speicher den Spielstand
	 * 
	 * @param username Der User dessen Spielstand gespeichert werden soll
	 * @param game     Der Spielstand der gespeichert werden soll
	 */
	public void saveGame(String username, GameJAXB game) {
		ParabelAdventureJAXB paOv = unmarshall();
		users.clear();
		List<UserJAXB> jaxbUsers = paOv.getUsers().stream().distinct().collect(Collectors.toList());
		users.addAll(jaxbUsers);
		UserJAXB user = findExistingUser(username);
		user.getGames().set(user.getGames().size() - 1, game);
		marshall(null);
	}

	/**
	 * Pr�ft ob der Benutzer null ist. F�gt den Benutzer zu der XML-Datei hinzu,
	 * wenn er nicht null ist.
	 * 
	 * @param currentUser Der aktuelle Benutzer
	 */
	private void marshall(UserJAXB currentUser) {
		File output = new File(fileUrl);
		try (FileOutputStream fos = new FileOutputStream(output)) {
			if (currentUser != null && findExistingUser(currentUser.getUsername()) == null) {
				users.add(currentUser);
			}
			ParabelAdventureJAXB paNv = new ParabelAdventureJAXB();
			paNv.getUsers().addAll(users);
			JAXBContext context = JAXBContext.newInstance(ParabelAdventureJAXB.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			m.marshal(paNv, fos);
		} catch (JAXBException | IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Pr�ft ob die Datei bereits vorhanden ist. Wenn diese nicht existiert wird
	 * eine neue Datei erstellt und mit einem leeren Parabeladventure gef�llt.
	 */
	public void createFileIfItDoesNotExists() {
		File file = new File(fileUrl);
		try {
			if (file.createNewFile()) {
				try (FileOutputStream fos = new FileOutputStream(file)) {
					ParabelAdventureJAXB pa = new ParabelAdventureJAXB();
					JAXBContext context = JAXBContext.newInstance(ParabelAdventureJAXB.class);
					Marshaller m = context.createMarshaller();
					m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
					m.marshal(pa, fos);
				} catch (IOException | JAXBException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Liest die XML-Datei ein und legt die Daten in Objekten ab.
	 *
	 * @return parabelAdventure Das Objekt das alle Daten beinhaltet.
	 */
	public ParabelAdventureJAXB unmarshall() {
		createFileIfItDoesNotExists();
		File file = new File(fileUrl);
		try (InputStream inputStream = new FileInputStream(file)) {
			SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
			Schema schema = sf.newSchema(new StreamSource(
					Thread.currentThread().getContextClassLoader().getResourceAsStream("playerData.xsd")));
			JAXBContext context = JAXBContext.newInstance(ParabelAdventureJAXB.class);
			Unmarshaller unm = context.createUnmarshaller();
			unm.setSchema(schema);
			XMLInputFactory xif = XMLInputFactory.newFactory();
			xif.setProperty(XMLInputFactory.SUPPORT_DTD, false);
			XMLStreamReader xsr = xif.createXMLStreamReader(inputStream);
			parabelAdventure = (ParabelAdventureJAXB) unm.unmarshal(xsr);
			Marshaller ms = context.createMarshaller();
			ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			List<UserJAXB> jaxbUsers = parabelAdventure.getUsers().stream().distinct().collect(Collectors.toList());
			users.addAll(jaxbUsers);
		} catch (SAXException | JAXBException | XMLStreamException | IOException e) {
			e.printStackTrace();
		}
		return parabelAdventure;
	}

	/**
	 * Pr�ft ob ein Benutzer bereits vorhanden ist. Wenn der Benutzer vorhanden ist
	 * wird eine Info angezeigt, wenn nicht wird der neue Benutzer angelegt.
	 *
	 * @param paOv     Die Daten die aus der XML-Datei ausgelesen wurden
	 * @param username Der Benutzername vom Benutzer der angelegt werden soll
	 * @return user Der neu angelegte Benutzer
	 */
	public UserJAXB checkIfUserExistsNewPlayer(ParabelAdventureJAXB paOv, String username) {
		UserJAXB existingUser = findExistingUser(username);
		if (existingUser != null) {
			JOptionPane.showMessageDialog(null,
					"Dieser Nutzer existiert bereits.\nBitte Spieler laden oder neuen Nutzer anlegen.");
			return null;
		} else {
			return createNewUser(paOv, username);
		}
	}

	/**
	 * Pr�ft ob ein Benutzer bereits vorhanden ist. Wenn der Benutzer vorhanden ist
	 * wird dieser Benutzer geladen, wenn nicht wird dies in einer Info angezeigt.
	 *
	 * @param username Der Benutzername vom Benutzer der angelegt werden soll
	 * @return user Der geladene Benutzer
	 */
	public UserJAXB checkIfUserExistsContinue(String username) {
		UserJAXB existingUser = findExistingUser(username);
		if (existingUser != null) {
			return existingUser;
		} else {
			JOptionPane.showMessageDialog(null,
					"Dieser Nutzer existiert noch nicht.\nBitte zuerst neuen Nutzer anlegen.");
			return null;
		}
	}

	/**
	 * Erstellt zu einem Benutzer ein neues Spiel.
	 *
	 * @param existingUser Der Benutzer f�r den ein neues Spiel erzeugt wird
	 * @return existingUser Der �bergebene Benutzer mit neuem Spiel
	 */
	public GameJAXB createNewGame(UserJAXB existingUser) {
		int gameID = existingUser.getGames().size();
		GameJAXB game = new GameJAXB(gameID, 0, 0, 0, 0, 0, sdf.format(timestamp.getTime()));
		CurrentLevelJAXB currentLevel = new CurrentLevelJAXB(1, 0, 0, 0, 0);
		existingUser.getGames().add(game);
		game.setCurrentLevel(currentLevel);
		marshall(existingUser);
		return game;
	}

	/**
	 * Erstellt einen neuen Benutzer.
	 *
	 * @param paOv     Das ParabelAdventure dem der Benutzer hinzugef�gt wird
	 * @param username Der Name des Benutzers
	 * @return user Der Benutzer der erzeugt wurde
	 */
	public UserJAXB createNewUser(ParabelAdventureJAXB paOv, String username) {
		int userID = paOv.getUsers().size();
		UserJAXB user = new UserJAXB(userID, username);
		return user;
	}

	/**
	 * Sucht in der XML-Datei mit dem Benutzernamen nach einem Benutzer.
	 *
	 * @param username Der Benutzername nach dem gesucht wird
	 * @return user Der gesuchte Benutzer
	 */
	public UserJAXB getUser(String username) {
		unmarshall();
		UserJAXB user = checkIfUserExistsContinue(username);
		marshall(user);
		return user;
	}

	public UserJAXB getUsername(String username) {
		unmarshall();
		UserJAXB user = checkIfUserExistsContinue(username);
		return user;
	}

	/**
	 * Sucht zu einem Benutzer den letzten Spielstand und gibt diesen zur�ck.
	 *
	 * @param username Der Benutzername zu dem der aktuelle Sppielstand abgefragt
	 *                 wird
	 * @return currentGame Der aktuelle Spielstand zu einem Benutzer.
	 */
	public GameJAXB getCurrentGame(String username) {
		UserJAXB currentUser = getUser(username);
		if (currentUser.getGames().isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"Dieser Nutzer hat noch keinen Spielstand.\nBitte zuerst neues Spiel anlegen.");
			return null;
		} else {
			return currentUser.getGames().get(currentUser.getGames().size() - 1);
		}
	}

	/**
	 * Sucht in der Liste users mit dem Benutzernamen nach einem Benutzer.
	 *
	 * @param username Der Benutzername der gesucht werden soll
	 * @return user Der gesuchte Benutzer
	 */
	public UserJAXB findExistingUser(String username) {
		for (UserJAXB user : users) {
			if (user.getUsername().equals(username)) {
				return user;
			}
		}
		return null;
	}
}