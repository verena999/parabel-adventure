package model;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

import java.awt.Graphics;
import java.awt.Point;
import java.util.Arrays;

import controller.FunctionCanvas;

public class Functions {

	/**
	 * berechnet den Funktionswert an der Stelle x
	 *
	 * @param function	die Funktion, deren Wert berechnet werden soll
	 * @param x			die Stelle, an der der Funktionswert berechnet werden soll
	 * @return 			Funktionswert an der Stelle x
	 */
	public static double calculate(Function function, double x) {
		double[] coeff = function.getCoefficients();
		int order = function.getOrder();
		double result = 0;

		for (int i = 0; i <= order; i++) {
			result += (coeff[i] * Math.pow(x, i));
		}
		return result;
	}

	/**
	 * berechnet die erste Ableitung der uebergebenen Funktion
	 *
	 * @param function 		die abzuleitende Funktion
	 * @return 				die erste Ableitung der Funktion
	 */
	public static Function derive(Function function) {
		double[] coeff = function.getCoefficients();
		int order = function.getOrder();
		if (order == 0) {
			double[] newCoeff = { 0 };
			return new Function(newCoeff);
		}
		double[] newCoeff = new double[order];
		for (int i = 1; i < order + 1; i++) {
			newCoeff[i - 1] = coeff[i] * i;
		}
		return new Function(newCoeff);
	}

	/**
	 * prueft, ob x eine Nullstelle der Funktion ist
	 *
	 * @param function	die Funktion, die geprüft werden soll
	 * @param x			der x-Wert, der geprüft werden soll
	 * @param tolerance	die Toleranz der Nullstellenbestimmung
	 * @return			<code>true</code> wenn x eine Nullstelle von function ist, sonst <code>false</code>
	 */
	public static boolean isZero(Function function, double x, double tolerance) {
		if (0 <= calculate(function, x) + tolerance || 0 >= calculate(function, x) - tolerance) {
			return true;
		}
		return false;
	}

	/**
	 * Prueft, ob x ein Maximum der Funktion ist
	 *
	 * @param function	die Funktion, die geprüft werden soll
	 * @param x			der x-Wert, der geprüft werden soll
	 * @param tolerance	die Toleranz der Extremwertbestimmung
	 * @return			<code>true</code> wenn x ein Maximum von function ist, sonst <code>false</code>
	 */
	public static boolean isMax(Function function, double x, double tolerance) {
		Function deriv1 = derive(function);
		if (!isZero(deriv1, x, tolerance)) {
			return false;
		}
		Function deriv2 = derive(deriv1);
		if (calculate(deriv2, x) < 0) {
			return true;
		}
		return false;
	}

	/**
	 * Prueft, ob x ein Minimum der Funktion ist
	 *
	 * @param function	die Funktion, die geprüft werden soll
	 * @param x			der x-Wert, der geprüft werden soll
	 * @param tolerance	die Toleranz der Extremwertbestimmung
	 * @return			<code>true</code> wenn x ein Minimum von function ist, sonst <code>false</code>
	 */
	public static boolean isMin(Function function, double x, double tolerance) {
		Function deriv1 = derive(function);
		if (!isZero(deriv1, x, tolerance)) {
			return false;
		}
		Function deriv2 = derive(deriv1);
		if (calculate(deriv2, x) > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Findet die Nullstellen einer Funktion maximal zweiter Ordnung
	 *
	 * @param function	die Funktion, deren Nullstellen gefunden werden sollen
	 * @return			ein Array der Nullstellen von funtion
	 */
	public static double[] findZero(Function function) {
		int order = function.getOrder();
		double[] ret;
		if (order == 0) {
			ret = new double[1];
		} else {
			ret = new double[order];
		}
		switch (function.getOrder()) {
		case 0:
			if (function.getCoefficients()[0] == 0) {
				ret[0] = 0;
			}
			return ret;
		case 1:
			ret[0] = function.getCoefficients()[0] * (-1) / function.getCoefficients()[1];
			return ret;
		case 2:
			double a = (-1) * function.getCoefficients()[1] / function.getCoefficients()[2] / 2;
			double b = sqrt(pow(a, 2) - function.getCoefficients()[0] / function.getCoefficients()[2]);
			if (!Double.isNaN(b)) {
				ret[0] = a + b;
				ret[1] = a - b;
			}
			return ret;
		default:
			throw new IllegalArgumentException("Only second order or lower is supported.");
		}
	}
	
	/**
	 * Findet das Extremum einer Funktion zweiter Ordnung
	 *
	 * @param function	die Funktion, deren Extremwert gefunden werden sollen
	 * @return			der x-Wert des Extremums von function
	 */
	public static double findExtremum(Function function) {
		if(function.getOrder() != 2) {
			throw new IllegalArgumentException("Only second order is supported.");
		}
		Function deriv = derive(function);
		return findZero(deriv)[0];
	}
	
	
	/**
	 * Berechnet die Schnittstellen von zwei Funktionen
	 *
	 * @param function1	die erste Funktion
	 * @param function2	die zweite Funktion
	 * @return			ein Array der x-Werte der Schnittstellen von function1 und function2
	 */
	public static double[] intersect(Function function1, Function function2) {
		if (Arrays.equals(function1.getCoefficients(), function2.getCoefficients())) {
			throw new IllegalArgumentException("The functions must not be identical.");
		}
		double func[] = new double[3];
		double koeff1 = function1.getCoefficients()[0] - function2.getCoefficients()[0];
		double koeff2 = function1.getCoefficients()[1] - function2.getCoefficients()[1];
		double koeff3 = function1.getCoefficients()[2] - function2.getCoefficients()[2];
		func[0] = koeff1;
		func[1] = koeff2;
		func[2] = koeff3;
		Function f = new Function(func);
		double[] ret = new double[f.getOrder()];
		ret = findZero(f);
		if (f.getOrder() == 2) {
			if (ret[0] == ret[1]) {
				double[] newRet = new double[1];
				newRet[0] = round(ret[0],1);
				return newRet;
			}
		}
		for(int i = 0;i < ret.length;i++) {
			ret[i] = round(ret[i],1);		
		}
		return ret;
	}

	/**
	 * Zeichnet eine Funktion auf einen uebergebenen Canvas
	 *
	 * @param c		der FunctionCanvas, auf den gezeichnet werden soll
	 * @param f		die zu zeichnende Funktion
	 * @param minX	der minimale x-Wert des Koordinatensystems
	 * @param maxX	der maximale x-Wert des Koordinatensystems
	 * @param minY	der minimale y-Wert des Koordinatensystems
	 * @param maxY	der maximale y-Wert des Koordinatensystems
	 * @param res	die Anzahl der zu zeichnenden Punkte
	 */
	public static void draw(FunctionCanvas c, Function f, double minX, double maxX, double minY, double maxY, int res) {
		Graphics g = c.getGraphics();

		double[] xValues = new double[res];
		double[] yValues = new double[res];
		double scaleX = (maxX - minX) / res;
		double stepX = c.getStepX();
		double stepY = c.getStepY();
		Point zeroPoint = c.getZeroPoint();

		for (int i = 0; i < res; i++) {
			double xCoord = minX + scaleX * i;
			xValues[i] = xCoord;
			yValues[i] = calculate(f, xCoord) * (-1);
		}
		
		int xCanvasOld = (int) (zeroPoint.getX() + (xValues[0] * stepX));
		int yCanvasOld = (int) (zeroPoint.getY() + (yValues[0] * stepY));

		for (int i = 1; i < res; i++) {
			int xCanvas = (int) (zeroPoint.getX() + (xValues[i] * stepX));
			int yCanvas = (int) (zeroPoint.getY() + (yValues[i] * stepY));

			// break loop if new values exceeds canvasBorders
			if (xCanvas < c.getCanvasBroderRight() && xCanvas > c.getCanvasBorderLeft() && yCanvas > c.getCanvasBorderTop()
					&& yCanvas < c.getCanvasBorderBottom()) {
				g.drawLine(xCanvasOld, yCanvasOld, xCanvas, yCanvas);
			}

			xCanvasOld = xCanvas;
			yCanvasOld = yCanvas;
		}
	}
	
	
	/**
	 * Rundet einen Wert auf eine bestimmte Anzahl Nachkommastellen
	 *
	 * @param value		der zu rundende Wert
	 * @param places	die Anzahl Nachkommastellen, auf die gerundet werden soll
	 * @return			der gerundete Wert
	 */
	public static double round(double value, int places) {
		if (places < 0) {
			throw new IllegalArgumentException();
		}
		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long temp = Math.round(value);
		return (double) temp / factor;
	}
}
