package model;

/**
 * Modellklasse des aktuellen Levels
 * 
 * @author Fabian Urban
 */
public class CurrentLevel {

	private int id;
	private int totalMoves;
	private int rightMoves;
	private int rightMovesInRow;
	private double parameterOfFunction;

	/**
	 * Konstruktor des CurrentLevels
	 * 
	 * @param id Die ID des aktuellen Levels.
	 * @param totalMoves Die Anzahl der gesamten Spielz�ge.
	 * @param rightMoves Die Anzahl der richtigen Spielz�ge.
	 * @param rightMovesInRow Die Anzahl der richtigen Spielz�ge am St�ck.
	 * @param parameterOfFuction Der Parameter der Funktion.
	 */
	public CurrentLevel(int id, int totalMoves, int rightMoves, int rightMovesInRow, double parameterOfFunction) {
		this.id = id;
		this.totalMoves = totalMoves;
		this.rightMoves = rightMoves;
		this.rightMovesInRow = rightMovesInRow;
		this.setParameterOfFunction(parameterOfFunction);
	}

	/**
	 * Getter des Felds id
	 *
	 * @return id Die ID des aktuellen Levels
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter des Felds id 
	 *
	 * @param id Die ID die gesetzt wird
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter des Felds totalMoves
	 *
	 * @return totalMoves Die Anzahl der gesamten Spielz�ge
	 */
	public int getTotalMoves() {
		return totalMoves;
	}

	/**
	 * Setter des Felds totalMoves 
	 *
	 * @param totalMoves Die Anzahl der gesamten Spielz�ge die gesetzt wird
	 */
	public void setTotalMoves(int totalMoves) {
		this.totalMoves = totalMoves;
	}

	/**
	 * Getter des Felds rightMoves
	 *
	 * @return rightMoves Die Anzahl der richtigen Spielz�ge
	 */
	public int getRightMoves() {
		return rightMoves;
	}

	/**
	 * Setter des Felds rightMoves 
	 *
	 * @param rightMoves Die Anzahl der richtigen Spielz�ge die gesetzt wird
	 */
	public void setRightMoves(int rightMoves) {
		this.rightMoves = rightMoves;
	}

	/**
	 * Getter des Felds rightMovesInRow
	 *
	 * @return rightMovesInRow Die Anzahl der richtigen Spielz�ge am St�ck
	 */
	public int getRightMovesInRow() {
		return rightMovesInRow;
	}

	/**
	 * Setter des Felds rightMovesInRow 
	 *
	 * @param rightMovesInRow Die Anzahl der richtigen Spielz�ge am St�ck die gesetzt wird
	 */
	public void setRightMovesInRow(int rightMovesInRow) {
		this.rightMovesInRow = rightMovesInRow;
	}
	
	/**
	 * Setter des Feldes parameterOfFunction
	 * 
	 * @param parameterOfFunction Der Parameter der Funktion
	 */
	public void setParameterOfFunction(double parameterOfFunction) {
		this.parameterOfFunction = parameterOfFunction;
	}
	
	/**
	 * Getter der Feldes parameterOfFunction
	 * 
	 * @return parameterOfFunction Der Parameter der Funktion
	 */
	public double getParameterOfFunction() {
		return parameterOfFunction;
	}

	/**
	 * Die toString-Methode des CurrentLevels
	 * 
	 * @return String der Felder des CurrentLevels
	 */
	@Override
	public String toString() {
		return "CurrentLevel [id=" + id + ", totalMoves=" + totalMoves + ", rightMoves=" + rightMoves + ", rightMovesInRow=" + rightMovesInRow + ", parameterOfFunction=" + parameterOfFunction + "]";
	}

}
