package model;

/**
 * Modellklasse des Benutzers
 *
 * @author Fabian Urban
 *
 */
public class User {

	private String username;
	private Game game;
	
	/**
	 * Konstruktor des Users
	 */
	public User() {
		
	}

	/**
	 * Getter des Felds username
	 *
	 * @return username Der Benutzername
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Setter des Felds username 
	 *
	 * @param username Der Benutzername der gesetzt wird
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Getter des Felds game
	 *
	 * @return game Der Spielstand des Benutzers
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * Setter des Felds game 
	 *
	 * @param game Der Spielstand der gesetzt wird
	 */
	public void setGame(Game game) {
		this.game = game;
	}

	/**
	 * Die toString-Methode des Users
	 * 
	 * @return String der Felder des Users
	 */
	@Override
	public String toString() {
		return "User [username=" + username + ", game=" + game + "]";
	}

}
