package model;

import java.util.ArrayList;

//TODO Kevin: Bitte ganze Klasse mit JavaDoc kommentieren
public class Function {
	private static final int MIN_X = 4;
	private static final int MAX_COEFF = 4;
	
	private double[] _coefficients;
	private int _order;
	private int canvasMinX = -10;
	private int canvasMaxX = 10;
	private int canvasMinY = -10;
	private int canvasMaxY = 10;
	
	// sets coefficients and order
	private void setCoefficients(double[] coefficients) {

		ArrayList<Double> list = new ArrayList<>();

		for (int i = 0; i < coefficients.length; i++) {
			list.add(coefficients[i]);
		}

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) == 0) {
				list.remove(i);
			} else {
				break;
			}
		}

		double[] newCoefficients = new double[list.size()];
		for (int i = 0; i < list.size(); i++) {
			newCoefficients[i] = list.get(i);
		}

		this._coefficients = newCoefficients;
		this._order = newCoefficients.length - 1;
	}
	
	public void setMinMax(int width, int height) {
		if(this._order != 2) {
			return;
		}
		
		int minX, maxX, minY, maxY;
		double extX = Functions.findExtremum(this);
		double extY = Functions.calculate(this, extX);
		
		if(Functions.isMin(this, extX, 0)) {
			if(extY < (-1)*height/2 + height/20 || extY > height/20) {
				minY = (int) extY - height/10;
			}
			else {
				minY = (-1)*height/2;
			}
			maxY = minY + height;
		}
		else {
			if(extY > height/2 - height/20 || extY < (-1)*height/20) {
				maxY = (int) extY + height/10;
			}
			else {
				maxY = height/2;
			}
			minY = maxY - height;
		}
		
		int centre = 0;
		
		if(extX < (-1)*width/10) {
			centre = (int) extX + width/10;
		}
		else if(extX > width/10){
			centre = (int) extX - width/10;
		}
		
		maxX = centre + width/2;
		minX = maxX - width;
		
		if(minX > (-1)*MIN_X) {
			minX = (-1)*MIN_X-1;
		}
		
		if(maxX < MIN_X) {
			maxX = MIN_X+1;
		}
		
		if(minY > Functions.calculate(this, (-1)*MIN_X) || minY > Functions.calculate(this, MIN_X)) {
			minY = (int) Math.min(Functions.calculate(this, (-1)*MIN_X),Functions.calculate(this, MIN_X)) - 1;
		}
		
		if(maxY < Functions.calculate(this, (-1)*MIN_X) || maxY < Functions.calculate(this, MIN_X)) {
			maxY = (int) Math.max(Functions.calculate(this, (-1)*MIN_X),Functions.calculate(this, MIN_X)) + 1;
		}
		
		this.canvasMinX = minX;
		this.canvasMaxX = maxX;
		this.canvasMinY = minY;
		this.canvasMaxY = maxY;
	}

	public double[] getCoefficients() {
		return _coefficients;
	}

	public int getOrder() {
		return _order;
	}

	public int getCanvasMinX() {
		return canvasMinX;
	}

	public int getCanvasMaxX() {
		return canvasMaxX;
	}

	public int getCanvasMinY() {
		return canvasMinY;
	}

	public int getCanvasMaxY() {
		return canvasMaxY;
	}

	// implements constructor, randomized generation of functions
	public Function(int order, boolean prettyZeroes) {
		if (order < 0) {
			throw new IllegalArgumentException();
		}
		else if(prettyZeroes == true) {
			int[] zeroPositions = new int[order];
			
			for (int i = 0; i < order; i++) {
				zeroPositions[i] = (int) getRangedRandom(MAX_COEFF*(-1), MAX_COEFF);
			}
			
			double[] coefficients = calcCoefficients(zeroPositions);
			setCoefficients(coefficients);
		}
		else {
			int min = MAX_COEFF*(-1);
			int max = MAX_COEFF;
			double[] coefficients = new double[order + 1];

			for (int i = 0; i < order + 1; i++) {
				coefficients[i] = getRangedRandom(min, max);
				min = min/2;
				max = max/2;
			}
			
			// It's not a parabola adventure without parabolas
			if(order >= 2) {
				if(coefficients[2] == 0) {
					coefficients[2] = 1;
				}
			}
			
			setCoefficients(coefficients);
		}
	}

	// implements constructor  for randomized generation of functions
	// with specified zeroPositions, coefficients are calculated from zeroPositions
	public Function(int[] zeroPositions) {

		double[] coefficients = calcCoefficients(zeroPositions);

		setCoefficients(coefficients);

		this.printFunction();
	}

	// implements constructor which accepts an array of coefficients
	public Function(double[] coefficients) {
		setCoefficients(coefficients);
	}

	//calculates coefficients from zero positions
	private double[] calcCoefficients(int[] zeroPositions) {
		double[] coefficients = new double[zeroPositions.length + 1];

		int x = 1;
		double[][] calcPairs = new double[zeroPositions.length][2];

		// fills calcPairs
		for (int i = 0; i < zeroPositions.length; i++) {
			calcPairs[i][0] = x;
			calcPairs[i][1] = zeroPositions[i] * -1;
		}

		switch (calcPairs.length) {

		case 1:
			coefficients[1] = calcPairs[0][0];
			coefficients[0] = calcPairs[0][1];
			break;
		case 2:

			// calculate coefficients
			for (int i = 0; i <= 1; i++) {
				for (int j = 0; j <= 1; j++) {

					// x * x
					if (i == 0 && j == 0) {
						coefficients[2] = calcPairs[0][i] * calcPairs[1][j];
					}

					// x * ?
					else if (i == 0 && j == 1 || i == 1 && j == 0) {
						coefficients[1] = coefficients[1] + calcPairs[0][i] * calcPairs[1][j];
					}

					// ? * ?
					else if (i == 1 && j == 1) {
						coefficients[0] = calcPairs[0][i] * calcPairs[1][j];
					}
				}
			}
			break;
		case 3:

			// calculates coefficients
			for (int i = 0; i <= 1; i++) {
				for (int j = 0; j <= 1; j++) {
					for (int k = 0; k <= 1; k++) {
						System.out.println("i: " + i + " j: " + j + " k: " + k);

						// x * x * x
						if (i == 0 && j == 0 && k == 0) {
							// System.out.println("Test1");
							coefficients[3] = coefficients[0] + calcPairs[0][i] * calcPairs[1][j] * calcPairs[2][k];
						}

						// x * x * ?
						else if ((i == 0 && j == 0 && k == 1) || (i == 0 && j == 1 && k == 0) || (i == 1 && j == 0 && k == 0)) {
							coefficients[2] = coefficients[1] + calcPairs[0][i] * calcPairs[1][j] * calcPairs[2][k];
							// System.out.println("Test2 " + coefficients[1]);
						}

						// x * ? * ?
						else if (i == 0 && j == 1 && k == 1 || i == 1 && j == 0 && k == 1 || i == 1 && j == 1 && k == 0) {
							coefficients[1] = coefficients[2] + calcPairs[0][i] * calcPairs[1][j] * calcPairs[2][k];
							// System.out.println("Test3 " + coefficients[2]);
						}

						// ? * ? * ?
						else if (i == 1 && j == i && k == 1) {
							coefficients[0] = coefficients[3] + calcPairs[0][i] * calcPairs[1][j] * calcPairs[2][k];
							// System.out.println("Test4 " + coefficients[3]);
						}
					}
				}
			}
			break;
		default:
			throw new IllegalArgumentException();
		}
		
		return coefficients;
	}
	
	// creates a string to display the function
	public String createFunctionString() {

		StringBuilder functionString = new StringBuilder();
		int i = 0;

		for (i = _coefficients.length - 1; i >= 0; i--) {
			functionString.append(_coefficients[i]);
			if (i != 0) {

				if (i == 2) {
					functionString.append("x\u00B2");
				} else if (i == 1) {
					functionString.append("x");
				} else if (i == 3) {
					functionString.append("x\u00B3");
				} else {
					functionString.append("x^").append(i);
				}
			}
			if (i > 0 && _coefficients[i - 1] > 0) {
				functionString.append("+");
			}
		}

		return functionString.toString();
	}

	// prints function to console
	public void printFunction() {
		String string = createFunctionString();
		System.out.println(string);
	}

	public double getRangedRandom(double min, double max) {
		double random = 0.00;

		random = Functions.round((Math.random() * (max - min) + min), 1);

		return random;
	}


	@SuppressWarnings("unused")
	private boolean arrayContainsOnlyInteger(double[] array) {
		int temp = 0;

		for (int i = 0; i < array.length; i++) {
			try {
				temp = Integer.parseInt(Double.toString(array[i]));
			} catch (Exception e) {
				return false;
			}
		}
		return true;
	}
}
