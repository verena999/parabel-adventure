package model;

/**
 * Levelauswertung des Spiels
 *
 * @author Verena Stech
 */
public class LevelEvaluations {

	private int level = 1;
	private int levelOneEvaluation = 0;
	private int levelTwoEvaluation = 0;
	private int levelTwoInRow = 0;
	private int levelThreeInRow = 0;
	private int levelFourInRow = 0;
	private int levelFiveEvaluation = 0;
	@SuppressWarnings("unused")
	private boolean startNewLevel = false;

	/**
	 * Auswertung Level 1: Zweimal korrektes Ergebnis, dann wird startNewLevel auf true gesetzt
	 */
	public void evaluateLevelOne() {
		startNewLevel = false;
		if (levelOneEvaluation == 0) {
			levelOneEvaluation++;
		} else if (levelOneEvaluation == 1) {
			startNewLevel = true;
			level = 2;
		} else {
			return;
		}
	}

	/**
	 * Auswertung Level 2: F�nfmal korrektes Ergebnis, davon die letzten 3 in Folge, dann wird startNewLevel auf true gesetzt
	 */
	public void evaluateLevelTwo() {
		startNewLevel = false;
		if (levelTwoEvaluation == 0 || levelTwoEvaluation == 1 || levelTwoEvaluation == 2 || levelTwoEvaluation == 3 || levelTwoEvaluation == 4) {
			levelTwoEvaluation++;
		}
		if (levelTwoEvaluation > 1) {
			if (levelTwoInRow == 0 || levelTwoInRow == 1 || levelTwoInRow == 2) {
				levelTwoInRow++;
			}
		}
		if (levelTwoEvaluation == 5 && levelTwoInRow == 3) {
			startNewLevel = true;
			level = 3;
		}

	}

	/**
	 * Auswertung Level 3: Dreimal korrektes Ergebnis in Folge, dann wird startNewLevel auf true gesetzt
	 */
	public void evaluateLevelThree() {
		startNewLevel = false;
		if (levelThreeInRow == 0) {
			levelThreeInRow++;
		} else if (levelThreeInRow == 1) {
			levelThreeInRow++;
		} else if (levelThreeInRow == 2) {
			startNewLevel = true;
			level = 4;
		} else {
			return;
		}
	}

	/**
	 * Auswertung Level 4: Dreimal korrektes Ergebnis in Folge, dann wird startNewLevel auf true gesetzt
	 */
	public void evaluateLevelFour() {
		startNewLevel = false;
		if (levelFourInRow == 0) {
			levelFourInRow++;
		} else if (levelFourInRow == 1) {
			levelFourInRow++;
		} else if (levelFourInRow == 2) {
			startNewLevel = true;
			level = 5;
		} else {
			return;
		}
	}

	/**
	 * Auswertung Level 1: Dreimal korrektes Ergebnis, dann wird startNewLevel auf true gesetzt
	 */
	public void evaluateLevelFive() {
		startNewLevel = false;
		if (levelFiveEvaluation == 0) {
			levelFiveEvaluation++;
		} else if (levelFiveEvaluation == 1) {
			levelFiveEvaluation++;
		} else if (levelFiveEvaluation == 2) {
			startNewLevel = true;
			level = 6;
		} else {
			return;
		}
	}

	/**
	 *
	 * @param level das Level
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 *
	 * @return level das Level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 *
	 * @param levelTwoInRow die Anzahl der in Level 2 in Folge richtigen berechneten Ergebnisse
	 */
	public void setLevelTwoInRow(int levelTwoInRow) {
		this.levelTwoInRow = levelTwoInRow;
	}

	/**
	 *
	 * @param levelThreeInRow die Anzahl der in Level 3 in Folge richtigen berechneten Ergebnisse
	 */
	public void setLevelThreeInRow(int levelThreeInRow) {
		this.levelThreeInRow = levelThreeInRow;
	}

	/**
	 *
	 * @param levelFourInRow die Anzahl der in Level 4 in Folge richtigen berechneten Ergebnisse
	 */
	public void setLevelFourInRow(int levelFourInRow) {
		this.levelFourInRow = levelFourInRow;
	}

	/**
	 *
	 * @return startNewLevel boolean zur Ueberpruefung, ob neues Level gestartet werden soll
	 */
	public boolean getStartNewLevel() {
		return startNewLevel;
	}
	
	public void setStartNewLevel(boolean startNewLevel) {
		this.startNewLevel = startNewLevel;
	}
	
	public int getLevelTwoInRow() {
		return levelTwoInRow;
	}

	public int getLevelThreeInRow() {
		return levelThreeInRow;
	}

	public int getLevelFourInRow() {
		return levelFourInRow;
	}

	/**
	 * setzt alle Werte auf 0 zurueck und das Level zurueck auf 1
	 */
	public void zuruecksetzen() {
		level = 1;
		levelOneEvaluation = 0;
		levelTwoEvaluation = 0;
		levelTwoInRow = 0;
		levelThreeInRow = 0;
		levelFourInRow = 0;
		levelFiveEvaluation = 0;
		startNewLevel = false;
	}

	public int getLevelOneEvaluation() {
		return levelOneEvaluation;
	}

	public void setLevelOneEvaluation(int levelOneEvaluation) {
		this.levelOneEvaluation = levelOneEvaluation;
		
	}

	public void setLevelTwoEvaluation(int levelTwoEvaluation) {
		this.levelTwoEvaluation = levelTwoEvaluation;
	}

	public int getLevelTwoEvaluation() {
		return levelTwoEvaluation;
	}

	public void setLevelFiveEvaluation(int levelFiveEvaluation) {
		this.levelFiveEvaluation = levelFiveEvaluation;
	}

	public int getLevelFiveEvaluation() {
		return levelFiveEvaluation;
	}

}
