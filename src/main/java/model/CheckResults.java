package model;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;

public class CheckResults {

	private int level;
	private double canvasTolerance;
	private double[] arrayXValues;
	private double[] arrayYValues;
	private double[] coeff1;
	private double[] coeff2;
	private char[] type;
	private boolean[] result;
	private ArrayList<Point2D.Double> points;
	private boolean[] canvasResult;

	
	public double getCanvasTolerance() {
		return canvasTolerance;
	}

	public void setCanvasTolerance(double canvasTolerance) {
		this.canvasTolerance = canvasTolerance;
	}
	
	
	public boolean[] getCanvasResult() {
		return canvasResult;
	}

	public ArrayList<Point2D.Double> getPoints() {
		return points;
	}

	public void setPoints(ArrayList<Point2D.Double> points) {
		this.points = points;
	}

	public CheckResults(int level, double canvasTolerance) {
		this.setLevel(level);
		this.setCanvasTolerance(canvasTolerance);
	}

	public double[] getArrayXValues() {
		return arrayXValues;
	}

	public void setArrayXValues(double[] arrayXValues) {
		this.arrayXValues = arrayXValues;
	}

	public double[] getArrayYValues() {
		return arrayYValues;
	}

	public void setArrayYValues(double[] arrayYValues) {
		this.arrayYValues = arrayYValues;
	}

	public double[] getCoeff1() {
		return this.coeff1;
	}

	public void setCoeff1(double[] coeff) {
		this.coeff1 = coeff;
	}

	public double[] getCoeff2() {
		return this.coeff2;
	}

	public void setCoeff2(double[] coeff) {
		this.coeff2 = coeff;
	}

	public char[] getType() {
		return this.type;
	}

	public void setType(char[] type) {
		this.type = type;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public boolean[] getResult() {
		return result;
	}

	public boolean evaluate() {

		switch (level) {
		case 1:
			return verifyLevelOneSolutions();
		case 2:
			return verifyLevelTwoSolutions();
		case 3:
			return verifyLevelThreeSolutions();
		case 4:
			return verifyLevelFourSolutions();
		case 5:
			return verifyLevelFiveSolutions();
		default:
			return false;
		}
	}

	public boolean verifyLevelOneSolutions() {
		if (this.arrayXValues == null || this.arrayYValues == null || this.coeff1 == null || this.points == null) {
			throw new NullPointerException();
		}
		boolean check = true;
		this.result = new boolean[this.arrayXValues.length];
		this.canvasResult = new boolean[this.points.size()];
		Function function = new Function(coeff1);
		for (int i = 0; i < this.arrayXValues.length; i++) {
			if (Functions.round(Functions.calculate(function, this.arrayXValues[i]),1) == Functions.round(this.arrayYValues[i],1)) {
				this.result[i] = true;
			} else {
				this.result[i] = false;
				check = false;
			}
		}
		double[] canvasXArray = new double[this.points.size()];
		for (int i = 0; i < this.points.size(); i++) {
			double x = Functions.round(points.get(i).getX(),1);
			canvasXArray[i] = x;
			double y = Functions.round(points.get(i).getY(),1);
			if((Functions.round(Functions.calculate(function, x), 1) >= y - this.canvasTolerance 
					&& Functions.round(Functions.calculate(function, x), 1) <= y + this.canvasTolerance) 
					&& Arrays.binarySearch(this.arrayXValues, x) >= 0) {
				this.canvasResult[i] = true;
			}else {
				this.canvasResult[i] = false;
				check = false;
			}
		}
		for(int i = 0; i < this.arrayXValues.length; i++) {
			if(Arrays.binarySearch(canvasXArray, this.arrayXValues[i]) < 0) {
				check = false;
			}
		}
		return check;
	}

	public boolean verifyLevelThreeSolutions() {
		if (this.arrayXValues == null || this.coeff1 == null || this.points == null) {
			throw new NullPointerException();
		}
		boolean check = true;
		this.result = new boolean[this.arrayXValues.length];
		this.canvasResult = new boolean[this.points.size()];
		Function function = new Function(coeff1);
		for (int i = 0; i < this.arrayXValues.length; i++) {
			if (Functions.isZero(function, this.arrayXValues[i], 0.2)) {
				this.result[i] = true;
			} else {
				this.result[i] = false;
				check = false;
			}
		}
		double[] canvasXArray = new double[this.points.size()];
		for (int i = 0; i < this.points.size(); i++) {
			double x = Functions.round(points.get(i).getX(),1);
			canvasXArray[i] = x;
			double y = Functions.round(points.get(i).getY(),1);
			if(Functions.isZero(function, x, this.canvasTolerance * 2) && y == 0) {
				this.canvasResult[i] = true;
			}else {
				this.canvasResult[i] = false;
				check = false;
			}
		}
		for(int i = 0; i < this.arrayXValues.length; i++) {
			if(Arrays.binarySearch(canvasXArray, this.arrayXValues[i]) < 0) {
				check = false;
			}
		}
		return check;
	}

	public boolean verifyLevelTwoSolutions() {
		if (this.arrayXValues == null || this.arrayYValues == null || this.coeff1 == null || this.type == null || this.points == null) {
			throw new NullPointerException();
		}
		boolean check = true;
		this.result = new boolean[this.arrayXValues.length];
		this.canvasResult = new boolean[this.points.size()];
		Function function = new Function(coeff1);
		for (int i = 0; i < this.arrayXValues.length; i++) {
			if (this.type[i] == 'T') {
				if (Functions.isMin(function, this.arrayXValues[i], 0.2) && Functions.round(Functions.calculate(function, this.arrayXValues[i]),1) == Functions.round(this.arrayYValues[i],1)) {
					this.result[i] = true;
				} else {
					this.result[i] = false;
					check = false;
				}
			} else if (this.type[i] == 'H') {
				if (Functions.isMax(function, this.arrayXValues[i], 0.2) && Functions.round(Functions.calculate(function, this.arrayXValues[i]),1) == Functions.round(this.arrayYValues[i],1)) {
					this.result[i] = true;
				} else {
					this.result[i] = false;
					check = false;
				}
			} else {
				this.result[i] = false;
				check = false;
			}
		}
		double[] canvasXArray = new double[this.points.size()];
		for (int i = 0; i < this.points.size(); i++) {
			double x = Functions.round(points.get(i).getX(),1);
			canvasXArray[i] = x;
			double y = Functions.round(points.get(i).getY(),1);
			if((Functions.isMax(function, x, this.canvasTolerance * 2) || Functions.isMin(function, x, this.canvasTolerance * 2)) && Functions.round(Functions.calculate(function, x), 1) == y) {
				this.canvasResult[i] = true;
			}else {
				this.canvasResult[i] = false;
				check = false;
			}
		}
		for(int i = 0; i < this.arrayXValues.length; i++) {
			if(Arrays.binarySearch(canvasXArray, this.arrayXValues[i]) < 0) {
				check = false;
			}
		}
		return check;
	}

	public boolean verifyLevelFourSolutions() {
		if (this.arrayXValues == null || this.arrayYValues == null || this.coeff1 == null || this.coeff2 == null || this.points == null) {
			throw new NullPointerException();
		}
		boolean check = true;
		this.result = new boolean[this.arrayXValues.length];
		this.canvasResult = new boolean[this.points.size()];
		Function function1 = new Function(coeff1);
		Function function2 = new Function(coeff2);
		double[] intersection = Functions.intersect(function1, function2);
		for (int i = 0; i < this.arrayXValues.length; i++) {
			if (Arrays.binarySearch(intersection, Functions.round(this.arrayXValues[i],1)) >= 0 && 
					(Functions.round(Functions.calculate(function1, this.arrayXValues[i]),1) >= Functions.round(this.arrayYValues[i],1) - 0.1)
					&& Functions.round(Functions.calculate(function1, this.arrayXValues[i]),1) <= Functions.round(this.arrayYValues[i],1) + 0.1) {
				this.result[i] = true;
			} else {
				this.result[i] = false;
				check = false;
			}
		}
		double[] canvasXArray = new double[this.points.size()];
		for (int i = 0; i < this.points.size(); i++) {
			double x = Functions.round(points.get(i).getX(),1);
			canvasXArray[i] = x;
			double y = Functions.round(points.get(i).getY(),1);
			if(Arrays.binarySearch(intersection, x) >= 0 && Functions.round(Functions.calculate(function1, x),1) >= y - this.canvasTolerance 
					&& Functions.round(Functions.calculate(function1, x),1) <= y + this.canvasTolerance) {
				this.canvasResult[i] = true;
			}else {
				this.canvasResult[i] = false;
				check = false;
			}
		}
		for(int i = 0; i < this.arrayXValues.length; i++) {
			if(Arrays.binarySearch(canvasXArray, this.arrayXValues[i]) < 0) {
				check = false;
			}
		}
		return check;
	}

	public boolean verifyLevelFiveSolutions() {
		if (this.coeff1 == null || this.coeff2 == null) {
			throw new NullPointerException();
		}
		boolean check = true;
		this.result = new boolean[this.coeff1.length];
		for (int i = 0; i < this.coeff1.length; i++) {
			if (Functions.round(this.coeff1[i],1) == Functions.round(this.coeff2[i],1)) {
				this.result[coeff1.length - i - 1] = true;
			} else {
				this.result[coeff1.length - i - 1] = false;
				check = false;
			}
		}
		return check;
	}

}
